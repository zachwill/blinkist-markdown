---
id: 50cf7a86e4b0cc0df4ce964e
slug: you-can-negotiate-anything-en
published_date: 2013-01-13T00:00:00.000+00:00
author: Herb Cohen
title: You Can Negotiate Anything
subtitle: None
main_color: 344F8B
text_color: 344F8B
---

# You Can Negotiate Anything

_None_

**Herb Cohen**

_You Can Negotiate Anything_ (1980) shows that negotiations occur in every walk of life and that it is vital to have the skills and understanding to deal with those situations. The book outlines the key factors affecting negotiation success, as well as ways of negotiating for win-win solutions.

---
### 1. Most things in the world can be negotiated, so you should learn to do it well. 

Life is a constant stream of negotiations. Whether you're trying to get your son to behave at a fancy restaurant or your boss to give you a raise, any situation where you try to affect someone else's behavior is basically a negotiation.

Most things in the world are negotiable. Prices at fancy department stores, mortgages or speeding tickets can all be negotiated, as long as you know how. Even notoriously inflexible entities such as the Internal Revenue Service (IRS) will negotiate with you, because the tax officials, at the end of the day, are just people like the rest of us. Because negotiations are so ubiquitous, don't you think it's time you started improving your skills in that arena?

Sometimes negotiating is just a matter of talking to the right person. Every organization has a hierarchy, and the lowest rungs always have the least power to grant you what you wish. So if the lackeys can't help you, demand to speak to managers, directors, and so on, until you find someone with sufficient authority to make concessions.

Once at a Mexican hotel, the author was told that this hotel had no room for him. He calmly asked for the manager, and inquired:

"Would you have room for the President of Mexico if he shows up?"

"Sí, Señor."

"Well, he's not coming, so I'll take his room."

In general, the only factors that should affect your decision to negotiate for something are whether you are comfortable negotiating for it, whether a negotiation will result in your needs being met and whether it's worth the effort.

**Most things in the world can be negotiated, so you should learn to do it well.**

### 2. Avoid Soviet-style negotiators – they mistake negotiations for battlefields. 

Some people see negotiations as a _win-lose_ battle, where one party's gain is always the other party's loss. This "winning at all costs" strategy is deployed around the globe, but the former Soviet Union is a prime example of its use. Because these negotiation tactics are so widespread, you must be able to identify them and protect yourself against them.

A clear trait of Soviet-style negotiators is that they take ridiculous _initial positions_ and are then unwilling to make _significant concessions_. When the Soviet Union sought to purchase land on Long Island for its embassy's recreational use, it initially offered $125,000 for the property worth $420,000. When the seller dropped the price to $360,000, the Soviets viewed this as a _sign of weakness_.

Eventually, they raised their offer — to $133,000. Such _stingy concessions_, especially at the last minute, are typical of Soviet-style negotiators, as they hope the pressure of the deadline will make the opposition cave.

Other Soviet-style tactics include using negotiators with _limited authority_, meaning people who cannot make concessions on their own. In this case, you will effectively be negotiating against yourself.

_Emotional tactics_ such as bullying, walking out or even crying are also often used as a weapon. Have you ever tried to negotiate with someone who breaks down in tears? You often end up giving the other side whatever they want. This particular tactic may not happen with a vodka-drinking, grim-faced Soviet general, but it is quite common in spousal arguments.

If you encounter a Soviet-style negotiator, you might simply want to walk away and consider other alternatives. If you do enter the fray, however, you should be careful to adhere to limits you have set for yourself in advance, and keep a cool demeanor despite any emotional tactics.

**Avoid Soviet-style negotiators — they mistake negotiations for battlefields.**

### 3. Collaborative win-win negotiations are possible when everyone’s needs are identified and harmonized. 

A win-lose negotiation mindset implies that there is a set amount of "pie" to be divided up, and each party must fight for theirs. If both parties fight ferociously, the common solution is a compromise, which despite its positive connotations actually means both parties are giving up something that they want. But a true _win-win negotiation_ is also possible.

Usually people focus on the _demands_ the other side makes in a negotiation, whereas they should really try to understand their underlying _needs_ and unexpressed desires. Since every person has unique, differing needs, several people can usually be satisfied without anyone losing. This requires their needs to be _harmonized_.

Consider a situation where your family is trying to decide where to go on vacation. This supposedly happy negotiation turns into a deadlock when it turns out your spouse wants to go to Texas, your son to the Rocky Mountains, while you yearn for the Great Lakes. These demands seem irreconcilable.

If you look beyond the demands, you might find that your needs can all be met. Your son actually just wants to see mountains, your spouse admittedly desires a warm place with tennis facilities, and you basically just want to go to a lake where you can swim and snorkel. After you realize this, it turns out a resort in Colorado fulfills the needs of everyone involved, and the impasse is solved.

The universal lubricant that allows such win-win negotiating is _trust_. Without trust, no party will be willing to share their true needs and make concessions.

Therefore, unless you're negotiating with family members like in the above example, you will need to build trust. Before the negotiation event, this can mean working to establish a relationship, and during the negotiation, highlighting your shared goals.

**Collaborative win-win negotiations are possible when everyone's needs are identified and harmonized.**

### 4. Successful negotiation means tapping into the many sources of power at your disposal. 

In negotiations, the opposing sides usually have many sources of power at their disposal.

For example, when negotiating with your boss, she usually has a very concrete form of power at her disposal: the power to _reward or punish_ you through e.g. the tasks she assigns to you.

But less obvious forms of power also affect negotiations.

Say you walk into a department store to buy a new refrigerator, and you start discussing the price with the sales clerk. The _alternatives_ you have can give you negotiating power. If you can easily walk across the street to another store stocking the same fridge, you are in a strong position. But what if your family is waiting in the car and they have told you to buy this exact fridge or else? Whether or not you can _risk_ losing this fridge can also be a source of power to either you or your opponent.

Let's continue the example. You walk up to the sales person and invoke the power of _precedent_ : "Look, my brother bought this same fridge and he got a discount." Or the power of _expertise and credentials_ : "I happen to be a fridge-expert — here's my card — and this fridge is overpriced!"

Most likely, an experienced sales clerk will counter with the power of _legitimacy_, typically pointing at some kind of printed sign that says, "No discounts." Most people are in awe of such written documents, though usually they are easily circumvented if you find someone with enough authority.

No matter how much power you have in a negotiation, what really matters is how much power you think you have and how much power the other person thinks you have. All power is based on perception.

**Successful negotiation means tapping into the many sources of power at your disposal.**

### 5. Make it hard for the other side to say no, but guard yourself against this same tactic. 

Imagine you're still trying to buy a fridge at a department store. What kind of powers could you use to improve your chances of getting a discount?

Instead of going straight for the model you want, you could ask the sales clerk to show you every single fridge in the store. You ask lots of technical questions, forcing him to dig up manuals for each model, and then you say that you'll have to think about it. The next day, you come back with your brother and ask the sales clerk to again demonstrate every model in the shop since your brother "knows about fridges".

After this rigmarole, do you think the sales clerk will be more likely to give you a discount? He sure will, because he's already invested several hours of time into you, and a discount will seem like a small cost to make the sale.

Getting the other side to _invest_ in a negotiation will make them more willing to grant concessions.

If, on the other hand, you came to the store with your family, a wily sales clerk may instead try to sell the fridge to them. This is called _getting commitment_. Once your wife, kids and dog are in love with a particular fridge, the sales clerk knows you pretty much have to buy it. In this case, you will be the one making concessions, not him.

Thus, in any situation where there are several people on your side of the table, you need to ensure every member is committed to the common goal so that you present a united front. In the fridge-example, this could mean discussing your family's shared goal before walking into the store.

**Make it hard for the other side to say no, but guard yourself against this same tactic.**

### 6. The distribution and sharing of information can greatly affect your negotiation success. 

If you know the needs, deadlines and constraints of your counterpart in a negotiation, you are always in a strong position compared to your opponent.

You gather this information by starting early. Most people think of negotiations as a clearly defined event that happens at a given moment in time, like when you meet with your boss to discuss a raise. In fact, negotiations are a process and this process begins far before you actually sit down at a table.

Before meeting your boss, you could consult next year's salary budget to see if there is leeway for another raise, and ask colleagues about your boss's usual demeanor in salary negotiations.

In addition to thorough preparation, you can glean information by asking lots of questions in the meeting itself. Some people even play a little dumb in negotiations to get the other side to reveal more facts. Especially when faced with an acknowledged expert. You can negate their advantage by forcing them to explain everything multiple times in "layman's terms."

Revealing information is a game of give and take: your opponent may reveal some of their constraints but expect you to do the same in return. Therefore, you should consider how you divulge information.

For example, when you make concessions to your opponent, you simultaneously share information on what your true negotiation limits are. Say you make the first offer of $100 for the fridge, to which the seller angrily shakes his head. If after that you immediately jump to $500, he will know that you are probably willing to go higher still, compared with if you had just gone up to $110.

**The distribution and sharing of information can greatly affect your negotiation success.**

### 7. Deadlines can influence negotiations, but don’t consider them absolute. 

Time plays a key role in negotiations. Just as students tend to hand in their term papers at the last minute, so too do negotiations tend to make the most progress as the deadline approaches.

If your opponent's deadline for concluding negotiations is at noon, whereas you have no fixed deadline and could go on negotiating at a leisurely pace for weeks, who do you think has the advantage? You do because as your opponent's deadline approaches, she will need to start making concessions to ensure a deal is reached.

Once upon a time the author tried to negotiate a deal in Japan on behalf of his employer. His hosts insisted he first fully experience Japanese hospitality and culture, and serious discussions did not begin until the last day of his two-week stay. The author, desperate to not go home empty-handed, was forced to negotiate on the way to the airport. His weak position resulted in a deal his superiors called, "The first great Japanese victory since Pearl Harbor."

Whenever you feel pressured by a deadline in a negotiation, coolly consider what the consequences of not adhering to it will be. Then make an informed decision as to whether the deadline is worth exceeding or not.

Also, keep in mind that your opponents always have a deadline too, and they will be more likely to make concessions as that deadline approaches.

**Deadlines can influence negotiations, but don't consider them absolute.**

### 8. When you negotiate, make it personal – but in a good way. 

In any negotiation, you will increase your chances of success if you can make things personal by getting the opposing side to identify with you as a human being. This could be as simple as behaving decently and generally being likeable, but some skilled negotiators also try to win sympathy from their opponents by playing weak.

For instance, if a cop pulls you over on the highway, you will be much better off playing the lost tourist on the brink of tears than a know-it-all big shot in a hurry.

Being likeable is a powerful force in negotiations, and can even overwhelm logic. In some American courtrooms, juries will sometimes absolve someone of guilt in the face of overwhelming evidence simply because the prosecutor seems unpleasant. Similarly, if you like the sales person at a certain store, you are likely to shop there again, even if it is more expensive than the store next door.

A less successful way of getting personal is to be obnoxious and offend the other person. If this happens in private, it will likely cost you a concession as you try to make up for your crude behavior. If it happens in public, you will cause the other person _to lose face_ and you will almost certainly make a _visceral enemy_, meaning someone who is not only opposed to the ideas you are advocating but to you as a person. Once you make a visceral enemy, they are likely to stay that way, trying to trip you up at every opportunity.

The best way to stay personal in a good way is to maintain a relaxed, cool demeanor. Try to imagine you're negotiating on someone else's behalf, as this will help you to not get emotionally involved.

**When you negotiate, make it personal — but in a good way.**

### 9. Final summary 

The key message in this book:

**You negotiate more often than you think, and hence should learn to do it well. The outcome of any negotiation primarily depends on the power, time and information each side has at their disposal. Though some negotiators aim to win at all costs, it is entirely possible to negotiate for mutual satisfaction.**

The questions this book answered:

**Why are negotiation skills important?**

  * Most things in the world can be negotiated, so you should learn to do it well.

**What kinds of negotiation strategies exist?**

  * Avoid Soviet-style negotiators — they mistake negotiations for battlefields.

  * Collaborative win-win negotiations are possible when everyone's needs are identified and harmonized.

**What factors affect the outcome of a negotiation?**

  * Successful negotiation means tapping into the many sources of power at your disposal.

  * Make it hard for the other side to say no, but guard yourself against this same tactic.

  * The distribution and sharing of information can greatly affect your negotiation success.

  * Deadlines can influence negotiations, but don't consider them absolute.

  * When you negotiate, make it personal — but in a good way.
---

### Herb Cohen

Herb Cohen has been a negotiator for over four decades, covering a wide range of situations from corporate mergers to international disarmament contracts and hostage negotiations. He has taught negotiation skills at many prestigious institutions, including the FBI, CIA and Harvard University. His analyses and insights have made him a subject of articles in diverse publications such as _The New Yorker, The Economist, TIME_ magazine and _Rolling Stone_.

