---
id: 5800b971230ff80003a1b7b9
slug: you-cant-read-this-book-en
published_date: 2016-10-18T00:00:00.000+00:00
author: Nick Cohen
title: You Can't Read This Book
subtitle: Censorship in an Age of Freedom
main_color: FDDA33
text_color: 87720E
---

# You Can't Read This Book

_Censorship in an Age of Freedom_

**Nick Cohen**

_You Can't Read This Book_ (2012) asks a pointed question: Does free speech exist or not? Today society has unlimited access to information online, but people still struggle to freely express opinions, fearing a backlash from governments, religious leaders or other powerful organizations.

---
### 1. What’s in it for me? Learn why freedom of speech is under attack worldwide. 

Do you want to read these blinks? What if you weren't allowed to do so? What if the government prohibited Blinkist from writing them in the first place?

We live in a world where freedom of speech appears to thrive. We can access the internet or social media sites, and from these platforms we can express our ideas and feelings freely. What's more, freedom of speech is recognized as a human right in a United Nations treaty.

So do we really have anything to fear regarding our right to free speech? The answer is yes. In fact, in many places, freedom of speech isn't so free after all. And that's what these blinks are about.

In these blinks, you'll find out

  * why _The Satanic Verses_ is banned by many world leaders;

  * how libel laws affect people's freedom to express themselves; and

  * how the financial crisis led to less freedom of speech.

### 2. The violent reaction to The Satanic Verses was unexpected and ushered in a new era of censorship. 

In 1988, British-Indian author Salman Rushdie released his book, _The Satanic Verses_. While he knew it would upset some of the world's Muslim population, he had no way of predicting how the outcry against his book would escalate and how it would affect censorship worldwide.

In his book, Rushdie depicts Muhammed being tricked by the devil. Neither Rushdie nor his publisher, Penguin Books, could have anticipated the _fatwa_ — the decree from Iran's Ayatollah Khomeni requiring Rushdie's execution — and the negative consequences it would bring.

Yet Rushdie's two previous books, _Midnight's Children_ and _Shame_, had also challenged culture and religion. His controversial views had always earned Rushdie his fair share of critics, but never before had his safety been threatened.

So what was so different about _The Satanic Verses_?

Basically, the writer and his publishers had underestimated the rising power of Ayatollah Khomeini, the Iranian religious leader who led the Islamic Revolution in 1979. In a move to gain even more clout in the Muslim world through the use of fear tactics and violence, Khomeini called for the murder of Rushdie and a worldwide protest against _The Satanic Verses_.

Bookshops carrying work considered blasphemous, like _The Satanic Verses,_ were bombed. Using terror to silence dissenting liberal voices was, in fact, effective: world leaders such as India's Rajiv Gandhi quickly banned the book, fearing further violence and the potential loss of the Muslim vote.

Soon, Islamic leaders around the world were preaching that there existed a global conspiracy to persecute Islam. They argued that the murder and persecution of liberal voices was justified as these voices were insulting Islam.

For example, author and film director Theo Van Gogh, highly critical of the tenets of Islam, was murdered in 2004. Separately, writer and journalist Kåre Bluitgen couldn't find an artist willing to illustrate his children's guide to the life of Muhammad and the Koran.

In other words, fear of violence has silenced many people from criticizing or even discussing Islam.

### 3. Over time, liberals began to censor themselves out of fear. 

After the backlash against Rushdie, fear quelled liberal voices that had criticized Islam's more conservative tenets. Soon, those liberal voices shifted away from defending Rushdie, too.

Even those in the West kept quiet, for they feared that even though they lived in free democracies, they would still have little defense against reprisal from zealous individuals.

In his book _Culture of Complaint_, Robert Hughes explains that while liberals in universities spoke freely and loudly against racism and sexism, when it came to murderous threats to free speech they were silent. Their sympathies seemed to lay less with authors like Rushdie and more with unintended victims, such as the owners of bombed bookstores.

Such attitudes made it seem as if liberals who had "provoked" the anger and backlash were partly to blame for the murderous violence.

In the past, liberals had directly railed against perpetrators of blatant injustices, as they did during apartheid in South Africa, for example. Now, united by fear, Western liberals shied away from direct action and instead redirected criticism from radical Islam to broader issues, such as racism and Western religions.

For instance, although many non-Western societies hold oppressive or even violent attitudes toward Christians, Western writers and comedians seem to much prefer criticizing Christianity than Islam.

For instance, journalists reported heavily on the poor treatment of prisoners of war at the hands of Westerners in the Iraq War, although such coverage risked exposing troops to danger. Yet, these same journalists downplayed atrocities committed by Islamists, a strategy to ally themselves with Islamic leaders and thus protect themselves from violent retribution.

It's not only the direct suppression of speech that was endangering liberal voices, but also the information that was unwritten, due to the conditions that made self-preservation feel necessary.

### 4. To maintain the illusion of being persecuted, religious countries react violently to minor offenses. 

Religious leaders have constantly found new ways to keep their followers offended by liberal voices and criticism. Why? Because liberal views challenge their authority. Religious leaders use the illusion of being a persecuted minority to maintain the support of their followers.

Such leaders benefit from a general population that is kept under a veil of fear and outrage. That way, the religious government can act as the savior, promising to alleviate such anxieties. So, to keep a population continuously enraged, governments detect and highlight offenses when none may truly exist.

This tactic is by no means limited to Muslim countries. In the 1990s, India's Hindu majority became nervous that competing religious parties and non-Hindu sectarian groups were gaining influence. The Hindu majority decided to inflame followers with claims that non-Hindu groups were making art that criticised or insulted Hinduism.

Indian and Muslim artist Maqbool Fida Husain, for instance, became a scapegoat in this political maneuver. His traditional Hindu paintings included depictions of nudes, which were suggested to be insulting to Hindus, since the artist was Muslim.

Sadly, such inflammatory tactics worked, and a gallery showing Husain's work was vandalized.

Leaders of certain countries like Iran know that artists, journalists and writers can have a huge influence on a population. As a result, their leaders respond to minor slights and criticism with disproportionately heavy responses or punishment.

This, in turn, quiets future dissenting voices, because a common liberal reaction to violence and threats is self-censorship. Unfortunately, this refusing to publish works that might stir outrage plays, of course, right into the hands of the oppressors!

### 5. The sudden growth of wealth in China and Russia has impaired their populations’ freedom of speech. 

Maybe you live in a country that values free speech, but in many other countries, freedom of speech is either threatened or nonexistent.

In fact, as places like China, Russia and the Middle East have continued to grow in terms of wealth, so too has the suppression of freely shared information intensified.

As a narrow segment of a population grows richer, their power to censor increases. For instance, 80 percent of the world's oil and gas is supplied by state-controlled companies in Saudi Arabia, Iran, Venezuela, Russia, China, India and Brazil. These countries are notorious for bribery and corruption, but voices that call these issues out are few and far between.

Why? Most of the governments in these countries also own and control the media, leaving little to no room for independent, critical voices.

Opposing such corrupt and sometimes omnipotent entities comes with considerable risk. Many of these same countries have a history of human rights violations. Thus individuals who challenge the status quo may face serious repercussions, such as imprisonment or extortion.

Meanwhile, the super-wealthy continue to flourish by sharing their wealth with those who will protect them and hold the power to suppress dissent. In some instances, the elites of these countries hire private armies to protect them from people who work or speak out against them.

One source of this influence is that the enormous power and holdings of many oligarchs were insulated from the effects of the worldwide recession in 2008, thus giving them an advantage over Western powers. While most of the Western world suffered from the financial fallout, countries such as China boomed and the international press looked the other direction, mostly interested in Western affairs.

Meanwhile, these oligarchic states conspired with well-funded private interests to align and control public information. And with such financial power, states could suppress any critical voices.

### 6. Whistleblower laws in Britain have quieted dissent in the workplace. 

In the quote unquote free world, citizens have a right to speak out against their government. The workplace, however, maintains different rules when it comes to freedom of speech.

Despite existing safeguards, people who speak too freely risk some undesirable consequences. Workers in Britain, for example, are often afraid to speak up because of complicated whistleblower laws.

While whistleblowers are protected under the law, employees can make a claim against an employer only if they feel certain that they would directly suffer otherwise. In other words, the interests of the employer come first. If, and only if, an employee faces direct repercussions from an employer's behavior can a whistleblower successfully win a case.

Let's say an employee makes a public complaint against an employer and has reasonable belief that the employer would destroy the incriminating evidence or fire him for speaking up. In court, however, all the employer has to do is refute the employee's claim, and the case would be dismissed. Potential whistleblowers thus often won't bother bringing an issue to light in the first place.

The alternative is for a whistleblower to raise a complaint directly and internally with an employer. But in such cases, company lawyers will petition the courts for a gag order, which courts rarely refuse.

For instance, the _British Medical Journal_ cites a case where a doctor tried to expose an unsafe heart surgery conducted in his hospital. Because confidentiality clauses favor the employer in Britain so strongly, the doctor's concerns were never made public. His career stalled, and he moved to Australia to start over.

In sum, employees often need to self-censor if they want to climb the job ladder and attain financial and professional security. Some workers are even financially rewarded for their silence.

The 2008 recession proves that there's a climate in which people feel powerless to voice dissenting opinions. Workers' self-censorship about flaws in the banking and mortgage industry ultimately contributed to the implosion of the global economy.

For example, Frederick Goodwin, the lead executive of the Royal Bank of Scotland, was well-known for bullying employees, ensuring they were too afraid to speak out.

### 7. England’s outdated legal system is used to suppress freedom of speech, even beyond English borders. 

Like rich and famous people all over the world, British elites seek to protect their names from negative press and innuendo. Luckily for them, their legal system works in their favor.

How so? English judges today uphold a legal system that stems from the feudalistic thirteenth century.

In 1275, King Edward I declared that no one could say or publish slanderous tales that may create discord between the king and his people.

But how does this outdated declaration hold today? Let's say you publicly voice speculations on a powerful businessman and his allegedly shady dealings. The businessman can then, in turn, sue you for provoking hatred, ridicule and contempt. Now you're a defendant in a libel case, and the burden of proof is placed on you to provide evidence that the story against the businessman was indeed true.

Normally in such libel cases, the burden is on the claimant — the businessman — to prove himself innocent of the alleged wrongdoings. So with this example, you can clearly see how the English system is rigged in favor of the powerful.

The rich can use libel laws to suppress free speech even outside of England, too. Since global online publications can be read in England, these are also subject to English libel laws.

For someone to sue for libel, all the person has to do is to claim he or she has a reputation in England. This can be "proven" by the simple act of buying property or owning a business in the country. What this means is that, by buying a house in London, international oligarchs can target journalists across the world through the English court system.

As a result, lawyers of newspapers and magazines often pressure editors to scrap pieces or force journalists to weaken their arguments with softer wording. Writers then refrain from submitting certain articles, given the hassle and expense of a potential lawsuit, which amounts to a form of self-censorship.

Given the exorbitant costs of bringing libel suits in England, it's not hard to see how easily the moneyed can buy silence.

### 8. The internet has made it harder for organizations and governments to control and censor information. 

Before the internet, governments and powerful media conglomerates could relatively easily control the flow of information.

Of course, today the internet has changed everything; and in fact, the second Iraq War was a key turning point in this shift.

Before a global "war on terror" began in 2003, countries like England could directly pressure publishers, and censor or control what reporters published. During the second Iraq War, however, journalists and other writers began to share openly contemptuous opinions of political and military leaders. This was partly because the growing number of internet news outlets gave them the opportunity to do so.

The internet opened up new sources of information that Western states could no longer intimidate into silence or control, and nothing could stop individuals from publishing leaked confidential information.

For instance, though the US government did punish Bradley Manning for posting government documents on WikiLeaks, the government was indeed powerless to stop the press from publishing the secrets in those documents once they were in the public sphere.

This kind of open access to information enabled many activists to hail technology as one way to free society from oppressive political forces.

In one instance during the Clinton administration, Christian conservatives tried to include a clause in a proposal to deregulate the telecommunications industry to control indecent material on the internet. This proposal was struck down, however, as a violation of the First Amendment, which guarantees free speech.

Such cases seemed to signal that the state could not control the internet and that even attempts to do so were pointless. The internet removed physical barriers that previously limited the reach of journalists and informants, as documents could quickly be shared online in a borderless cyberworld.

What's more, the internet flattened the media playing field. To publish "news," all a person needed was a laptop, an internet connection and, perhaps, a video camera. Previously the dissemination of information was a right reserved for the rich, such as owners of newspapers, TV stations and those with access to expensive equipment.

Those who lauded the transparency of the internet saw the technology as sparking a revolution against censorship. But as we'll see in the next blink, this isn't always the case.

### 9. Powerful people seek to control the internet, and they’re starting to silence online voices. 

So with the internet, everyone on the globe has a voice and can use it freely. World problems solved, right? Well, not quite.

These days, oppressive leaders and the businesses that support them are working to control opinions and dissent on the internet.

Leaders aren't dumb, they know it's impossible to control the internet directly. So instead these people go after the internet's financial and hardware vulnerabilities.

In Russia, for instance, one tactic employed by the Kremlin is to punish businesses supporting government critics by seizing assets, such as computers and technology. Likewise, businesses that support the Kremlin are aided in purchasing major TV stations, which are then used to disseminate pro-government propaganda.

Harassment, even murder, ensures that dissidents know the potential consequences of disobedience.

For instance, Muslim women's rights activist Ayaan Hirsi Ali was threatened and harassed for writing a memoir that was critical of the Islamic faith. Thanks to the internet, the people who threatened her couldn't stop her from sharing her views directly. But, through threatening and harassing Ali's supporters, they could warn others of the potential perils of criticizing Islam.

The Chinese government, meanwhile, controls the internet by blocking or hijacking websites and limiting access to certain sites. Such methods are often executed in conjunction with private enterprises with ties to the government.

Another sign that the internet isn't the key to a perfectly free society is that the overwhelming number of voices online makes it difficult for many to be heard effectively. This difficulty has forced writers to rise above the din.

While not everyone can write a book or publish an article, anyone can start a blog. This creates millions of competing online voices, while the average person obviously has limited time to search and read them all.

So internet writers, in turn, must first convince the public that their work is as important and credible as traditional mass media, which is no easy task. Unfortunately, this means that for all their independence, bloggers face an uphill battle in getting a message heard. Lots of vital information is lost in this fashion.

While the internet was once seen as opening the door to a utopian future of free speech, today's online chaos proves that such a dream is far from becoming reality.

### 10. Final summary 

The key message in this book:

**Despite new forms of communication, censorship is still strong and is used by the powerful to control the public. Whether against the threat of violence from radicals or the propaganda of big government and big business, liberal voices that support free speech must struggle to have their message heard.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Net Delusion_** **by Evgeny Morozov**

_The_ _Net_ _Delusion_ tackles head on the beliefs we hold about the utopian power of the internet. Evgeny Morozov shows us how the internet isn't always a force for democracy and freedom, and reveals how both authoritarian and democratic regimes control the internet for their own interests.
---

### Nick Cohen

Nick Cohen is a British journalist, author and renowned liberal commentator who writes for _The Observer_. He has written five books, including _What's Left?_

