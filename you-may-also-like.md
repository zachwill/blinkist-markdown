---
id: 5831b27c8478e30004aeff67
slug: you-may-also-like-en
published_date: 2016-11-24T00:00:00.000+00:00
author: Tom Vanderbilt
title: You May Also Like
subtitle: Taste in An Age of Endless Choice
main_color: 2EC1E6
text_color: 1F8199
---

# You May Also Like

_Taste in An Age of Endless Choice_

**Tom Vanderbilt**

_You May Also Like_ (2016) dives into the ever-changing world of taste, or what you like and why you like it. Trying to guess whether a consumer will enjoy a movie or buy a product is both tricky science and big business, as a myriad of different factors influences the decisions you make daily.

---
### 1. What’s in it for me? Better understand what you like and why you like it. 

There's no arguing over taste, and yet we do so daily. Taste matters in society — consider the shame you might feel if something you "like" is called cheesy, vulgar or even strange.

So why do you like certain things and abhor others? And why do tastes change over time — remember that "classic" song you played constantly in your teens that today makes your hair stand on end?

In these blinks, you'll discover that taste is one part evolution, two parts experience. What's more, you'll learn how companies use the latest technology to try to predict your preferences — even though you'd be hard-pressed to explain them yourself!

In these blinks, you'll also find out

  * why some Germans prefer vanilla-flavored ketchup;

  * why Netflix knows more about your tastes than you do; and

  * how Hitler's favorite music might change what's on your playlist.

### 2. Many factors help determine what we like, but most preferences stem from pleasurable associations. 

Picking a favorite color may seem straightforward, but expressing such a preference is complicated, as it is based on many different factors.

Preferences are both _categorical_ and _contextual_. While you may treasure your favorite blue sweater, a blue egg would certainly be unappealing. What's more, even though you wear your blue sweater every weekend, at the office you might prefer to wear only black clothes.

Taste is also _constructed_. If you're asked about a favorite color, movie or song, you probably will pick the first good example you can think of and then come up with a reason to justify your preference.

Another factor is that humans are inherently _comparative_. From childhood onward, it is often the case that we simply like the things that other people like.

And aside from a few exceptions, taste is rarely _congenital_. The genes we inherit from parents and grandparents don't inform our individual preferences. When it comes to preferences for certain colors or foods, a better explanation for personal choice is to consider the item's association with things that are pleasurable.

For instance, many people prefer the color blue, as it evokes peaceful, pleasant things such as a clear, sunny sky or the ocean.

One German study found that adults are influenced by pleasures experienced as an infant. In the study, participants tried two different kinds of tomato ketchup: one natural and one flavored with vanilla. Interestingly, participants who were raised on baby formula preferred the vanilla-flavored ketchup. Researchers connected the dots: as German baby formula contains vanilla flavoring, they discovered that participants had unconsciously associated that flavor with the infant pleasure of being fed.

This kind of association applies to color as well; but like many preferences, it can change over time.

Children instinctively are attracted to yellow-brownish colors, and researchers believe that this could be because these colors remind of a mother's nipple. But as children grow older, they lose this preference, as other things that are yellow-brown — such as feces or vomit — have unfavorable associations.

> _"A favorite color is like a chromatic record of everything that has ever made you feel good."_

### 3. Evolution guides some food preferences, but experience and expectation also determine your taste. 

Did you know, the question, "What do you want for lunch?" is partly determined by human evolution?

It's a fact that your evolutionary wiring has a considerable say over what appears on your plate.

Humans have evolved to prefer certain things in food. We like things that are sweet because this tells us that the food is rich in calories. We register bitter flavors as unpleasant, as it could indicate the presence of toxins.

And even though humans are _omnivores_, we're wired to be wary of unusual foods and to notice slight variations in what we eat daily. For this reason, you might not appreciate it if a barista adds pumpkin spice unexpectedly to your regular black coffee.

While evolutionary preferences are strong, however, culturally determined tastes can be even stronger.

Even though instinct informs us that we should be wary of bitter foods, many adults enjoy strong coffee or hoppy beer, since these items have become a common part of our food culture. And since we've consumed plenty of coffee and beer over the years without coming to serious harm, the foods' bitter profile is now accepted as safe.

Memory and the anticipation of repeating pleasurable sensations also play a big part in diet preference.

If you have a great meal at a restaurant, chances are you'll remain faithful to the establishment even if there are dozens of other promising spots in town. You'll hold on to the memory of the delicious flavors you experienced and look forward to experiencing those positive feelings again.

We are also influenced by our expectation of how pleasurable certain foods will be.

That dark chocolate cake with plump, red strawberries in the bakery window certainly looks delicious; as you gaze at the cake and perhaps salivate a bit, it's certain that if you tried a piece, you'd be more likely to enjoy the cake given your already high expectations of pleasure.

The opposite holds true as well. As airplane meals have such a bad reputation for being flavorless and inedible, it's almost a guarantee that you'll hate your chicken in red sauce given your already low expectations of the meal on a transatlantic flight.

### 4. Algorithms and online review sites try to glean your preferences, but they’re certainly not perfect. 

Not so long ago, you had just a small handful of stations from which to choose on television or the radio.

Today, media choices are abundant and often overwhelming — and algorithms are here to help you sort through it all. Algorithms, or data-crunching computer programs, can act as a filter to sift through the wealth of choice, presenting you with a selection of recommendations tailored to your tastes.

The online streaming service Netflix uses algorithms to determine your viewing preferences, by analyzing how you use the service. It examines your search terms and which films you then decide to watch.

These methods of determining user preference are often more accurate than basing recommendations strictly from user ratings, as people tend to be overly concerned with how their ratings might appear to others. For example, even though you might have enjoyed a cheesy horror movie more than the latest award-winning drama, you'll probably give the prestigious film more stars.

This distortion in ratings means that sites, such as Yelp, which rely on user reviews are prone to distorted results.

Yelp's business is built on public feedback, ideally from locals who offer honest reviews based on first-hand experience of a restaurant, club or bar. While there are bound to be fake reviews, a user can usually identify them by the review's over-the-top language or lack of detail.

Yet with user reviews, there are other potential distortions to consider. A place that gets a ton of five-star rankings will inevitably experience an equal backlash of negative reviews.

This is partly because people who have had a positive experience aren't motivated to add more praise to the pile, which leaves unhappy customers the only reviewers eager to have a say.

In essence, one could say that user reviews are less about a product and more about the social effects of other people's' preferences.

### 5. A person’s musical taste can provide personal insight, but preferences never tell the complete story. 

Would you be upset if a friend judged you based on the music you enjoyed?

Musical taste can shed light on a person's character, as people listen to music for many reasons. Music is both a way to express individuality and belong to a community.

A person's taste in music thus speaks to who they are, or more accurately, who they want to be. And there is a lot of information behind what we like and dislike in terms of tunes.

We care about what musicians and bands represent culturally — is the music rebellious, sophisticated or liberating? The more we identify with a certain style of music, the likelier we are to become a fan.

But on the other hand, if someone told you that Adolf Hitler was a huge fan of the composer Richard Wagner, you might find it challenging to enjoy the opera _Tristan und Isolde_ in the same fashion as before.

Music can also reveal a person's political leanings. Statistically speaking, if an American enjoys rap music, chances are that person doesn't vote Republican. But if that same person loves country music, then he might identify with the Republican party.

Yet it's best not to make assumptions about a person based on their preferences alone, as such generalizations don't always add up.

One study suggested that people who walk out of a movie before it's finished are also more likely to walk out of a marriage and divorce. Obviously, this doesn't mean that your marriage is doomed if your partner left the theater halfway through _Bridesmaids_!

And with the wealth of musical options in society, people today can listen to a wide range of musical styles. It's altogether too easy for people on opposite ends of the cultural spectrum to listen to and enjoy the same musical groups or songs.

Whether a favorite food, band or television show, preferences too can simply be the result of upbringing — the influences we had while growing up.

Ultimately, a person's preferences can provide clues about who they are, but taste never tells the whole story.

### 6. We don’t always know why we like something or whether we should enjoy what we do like. 

Does this sound familiar? When someone asks you why you like something, you respond with, "I don't know why, I just do!"

Even if you don't know why you like a certain clothing style or song, to explain to others your preferences, you will often construct a reason after the fact.

When you first look at a painting in a gallery, for example, it takes no more than 50 milliseconds — no time at all — to know whether you "like" it. This is, of course, faster than the time it takes for your brain to comprehend the details of what your eyes are seeing.

Since this process is essentially unconscious, we can't truly understand why or why not a work of art pleases us. Yet we will immediately construct any number of reasons on the spot, postulating that the work is a masterpiece of postmodernism or a failure given its poor composition.

But consider this: after seeing a painting you like a lot, you read a negative review from a respected art critic. You then question whether you _should_ like the piece, even though initially you thought it pleasing.

Art is subjective; there's no standard for what makes something good. What's more, critics are always disagreeing with each other over the merits of this artist or that work.

Another issue is that it's next to impossible to look at a piece of art and judge it solely on its own merits. When looking at a painting, watching a movie or listening to music, you can't help but compare it to everything you've seen and heard before, and all your previous opinions of this or that work.

Sometimes it's also tough to determine what is a piece of art. People debate whether a spray-painted mural on the side of a building should be judged similarly to an oil painting hanging in a gallery. Yet even at a fine art exhibition, attendees have mistaken a fire extinguisher hanging on the wall for a work of art!

Figuring out why you like something — or why you don't — isn't a simple matter. And of course, your tastes can change, too.

> _"Art is what the art world says is art."_ — Arthur Danto

### 7. Your taste can and will change over time, but you can’t predict how or when it will happen. 

Impressionist master Vincent van Gogh died penniless, unsuccessful in his own time. The reason? Back then, many critics disliked the works of the Impressionists, preferring art that today is all but forgotten.

Tastes, styles and trends certainly change over time; the evidence is all around us. You probably have tons of embarrassing photos of yourself wearing clothes or sporting a hairstyle that you wouldn't be caught dead with today!

Strangely, we still tend to look at our preferences today as things that will never change, a phenomenon called _projection bias._

Part of the reason for this delusion is selective memory. While we happily remember favorite songs, books or movies, we tend to forget the things we might have liked that now are embarrassing.

By continuing to underestimate how much time and experience changes taste, we remain blissfully ignorant, confident that we've been consistent in our preferences all along.

But even if you acknowledge that your tastes do change, you can't predict how or when it will happen since there are so many different factors to take into consideration, some of which are quite random.

Just consider what can affect our preferences for children's names. When Jacqueline Kennedy became First Lady in 1961, suddenly the name "Jacqueline" became popular.

Another indicator for popular names is destructive hurricanes, such as Katrina or Andrew. After these traumatic weather events, a boom was observed in parents giving their children names that begin with the first letter of the hurricane's name, such as Keith or Alex.

But being part of a popular trend isn't for everyone; some parents won't consider a name if it looks as if it will become common.

### 8. Experts are excellent judges of quality, but even their judgment can be distorted. 

Many people turn to experts for guidance for tasks in which they lack certain skills, such as cooking a fancy meal or choosing a bottle of wine. Experts are essentially high-caliber judges who have superior experience in a particular field.

We often act as judges ourselves, while watching a dancing or singing competition on television, for example. When we arrive at an opinion of a performance, we're comparing this moment to everything we've seen and heard before. The more performances we watch, the more experience we gain; and over time, our judgment becomes more nuanced and informative, until we reach an "expert" level.

A wine expert, for example, has seen, smelled and tasted thousands of wines, and in doing so, is now able to distinguish subtle variations.

With a wide variety of prior knowledge and experience, experts create a base that gives context to their opinions when observing a new performance or sipping an unfamiliar glass of wine. They then can rate quality, examining every nuance of the dance, song or bottle.

This experience also comes with a specialized vocabulary which allows experts to compare and categorize details objectively. Wine experts, for example, can talk to each other easily, distinguishing "oaky" flavors from "earthy" ones.

Yet experts certainly aren't perfect. Research into the consistency of Olympic judges has shown that, even at this expert level, judgment can get distorted.

After being shown video clips of gymnastic performances of varying quality, researchers found a pattern in how judges evaluated performances — they would routinely rate a performance higher if it followed an excellent showing. Likewise, if a performance followed a poor showing, it would receive a poor rating.

Also, if the judges were told two different clips featured athletes of the same nationality, the judges would pay special attention to the athletes' similarities. And if judges were told the gymnasts were from different countries, the experts paid more attention to the athletes' differences.

In short, nothing exists in a vacuum. Outside influences always work on our preferences, even if we're not aware of them.

### 9. Final summary 

The key message in this book:

**Your taste is always evolving, yet to explain your preferences is no easy thing. Quite often, people are not aware of why they "like" something. Taste depends on many variables, ranging from personal experience, friends' preferences and specific context — not to mention random factors that can't be predicted.**

Actionable Advice

**If you want to experience true flavor, try bland food.**

One problem with a hearty meal is that we like it less by the time we're finished. Therefore, we often break meals into courses and look for variety in dishes and flavors. When you choose a richly flavored, exotic dish, the experience is burned in your sensory memory, yet despite the riot of flavors, you may quickly sour on the experience. If you choose a bland dish instead, the experience of eating will equally be benign — the reason why fare served to soldiers is often bland and thus forgettable. So opt more often for blander foods, to accentuate and heighten your experience when you order something truly tasty.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Affluenza_** **by John de Graaf, David Wann and Thomas H. Naylor**

This book is about our serious addiction to consumption: affluenza. Since the Industrial Revolution, we've become addicted to shopping, believing we can buy happiness. Affluenza affects us and our society like a disease, and this book offers advice on how we can immunize ourselves against it.
---

### Tom Vanderbilt

Tom Vanderbilt is a writer whose work on culture, technology and design has been published in _The New York Times Magazine_, _Rolling Stone_ and _The Wall Street Journal_, among others. He's also a contributing editor for _Wired_ and _Artforum_, and the author of the bestselling book _Traffic: Why We Drive the Way We Do (and What it Says About Us)_.

