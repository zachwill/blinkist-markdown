---
id: 5489dafd3363370009410000
slug: you-only-have-to-be-right-once-en
published_date: 2014-12-17T00:00:00.000+00:00
author: Randall Lane
title: You Only Have to Be Right Once
subtitle: The Unprecedented Rise of the Instant Tech Billionaires
main_color: B09635
text_color: 7D6A26
---

# You Only Have to Be Right Once

_The Unprecedented Rise of the Instant Tech Billionaires_

**Randall Lane**

Based on _Forbes_ magazine interviews with some of Silicon Valley's most successful tech entrepreneurs, _You Only Have to be Right Once_ outlines how today's tech wunderkinds achieved their successes.

---
### 1. What’s in it for me? Discover how the hottest entrepreneurs of the 21st century made their billions. 

Did you know that Airbnb started out as an inflatable mattress and WhatsApp as an online address book with the ability to share status updates?

Over the last few years, a few entrepreneurs have struck it big, very big. In a relatively short space of time, the people behind success stories like Dropbox, Airbnb and WhatsApp have taken an initial simple idea and made it into businesses worth billions.

These blinks describe the journey that many of these _tech billionaires_ have taken. Based on interviews with _Forbes,_ they teach you that in the modern digital world the possibilities from simply having a good idea are endless.

In these blinks you'll discover:

  * how one entrepreneur got into Silicon Valley through selling carpets;

  * why the genius behind Tumblr didn't want to run his own business; and

  * which innovative product began life as a disposable camera strapped to an arm.

### 2. Many tech-billionaires achieved their success by boldly pursuing a dream. 

If you want to be successful in life, what's the best path to take? Here's the conventional answer to that question: work hard at school and then find a mentor to help you along your career — which you should pursue at a slow and steady pace.

Although this approach probably sounds familiar, most of today's mega-successful young entrepreneurs — the _tech-billionaires_ — followed a different path. For them, success was all about boldly pursuing a dream and ignoring what society expected of them.

Let's look at one example: when he was just 23-years-old, Evan Spiegel, the co-founder of Snapchat, turned down Facebook's offer to buy his company for $3 billion. Although it's too early to tell whether his decision was wise, we can still learn from Spiegel's stratospheric path to success.

Like many other successful entrepreneurs, Spiegel learned how to get his own way at a young age. When his high-powered lawyer parents split up, Spiegel used it to his advantage: his dad refused to buy him a BMW, so Spiegel moved in with his mother. It wasn't long before his dad caved and bought him the car.

There's another similarity between Spiegel and his entrepreneurial peers: that he dropped out of college to pursue his startup. He and Snapchat co-founder Bobby Murphy came up with the idea for their app while they were still in college, but struggled to get the concept to take off. Although their parents pestered them to look for "proper jobs," the duo stayed true to their dream.

And finally, they received an investment from a venture capitalist. Spiegel promptly quit college (weeks ahead of graduation) and started on Snapchat day and night.

And of course, you know how this story ends: Spiegel's efforts paid off and Snapchat became one of the world's most popular apps — so popular that Facebook offered to buy it for $3 billion.

### 3. Plenty of outsiders have broken into the closed world of Silicon Valley. 

After hearing that last story, you might be wondering, "Hmmm. Was Evan Spiegel really an outsider who achieved success solely through hard-work and boldness? After all, weren't his parents high-powered lawyers? Didn't he go to Stanford?"

Although there's some truth to that objection, plenty of tech-billionaires come from different backgrounds. Here are two stories about mega-successful entrepreneurs who came into the tech industry as complete outsiders.

First we have Pejman Nozad. Born in Iran, Nozad's family fled to Germany when he was a boy. Later, he made a bold move to San Francisco with just $700 in his pocket. Although he didn't speak English, he soon learned the language and found a way to survive by doing odd jobs.

Eventually, without having prior experience, he started working as a salesman at a carpet store. He turned out to be a great salesman and an even better networker. Through his sparkling conversational skills and personal warmth, Nozad built up relationships with many of the businessmen and tech leaders who came to the store. And eventually, he leveraged these connections to start the investment fund Amidzad, which invested early in successful companies like Dropbox — earning over $100 million in the process.

Our second story centers around Adi Tatarko, who co-owns _Houzz_, a home-design community worth $2 billion.

Tatarko is one of the few women who achieved great success in the tech industry despite the overwhelming gender bias in Silicon Valley. (Consider that 43 percent of the 150 biggest companies in the Valley don't have a single woman on the Board of Directors!)

How did she manage to cut through the sexism? Well, it had a lot to do with sheer energy: Tatarko can work _nonstop_. And for motivation, she looks up to her grandmother, who worked as a designer when fashion was still a male-dominated industry.

### 4. Tech entrepreneurs get to the top with a little help from their friends. 

Whenever we read about the latest wildly successful tech wunderkind, we're tempted to think, "Wow—he must be a genius to accomplish so much at his age!" Not to detract from anyone's well-earned success, but it's important to remember that all of these people had lots of help along the way.

To that end, let's look at the story of _Tumblr_. Today, the wildly successful social network is a top tech enterprise, yet, founder David Karp never planned for it to be a business. He was just happy to have the tool for his personal use.

Luckily, he had a mentor. Entrepreneur Fred Seibert recognized Tumblr's vast potential for success, and managed to persuade Karp (pretty much against his own will) to turn his product into a business.

At first, Karp hated being a manager and overseeing the expansion of his company. But he persevered and Tumblr flourished. Eventually, the company's success attracted Yahoo!, and the tech giant bought Karp's blogging platform for $1.1 billion. So, thanks to Seibert's advice, Karp turned his small idea into a major success.

Here's another story about mentorship in the tech world. It concerns Palmer Luckey, the creator of the _Oculus Rift_ virtual reality (VR) headset. It's important to note that VR is a notoriously tricky field, and that many have tried and failed in this area — including games giant Nintendo, whose 1996 foray into VR was so disastrous that it literally gave players a headache when they used it.

Nevertheless, Luckey ventured forth. And luckily, he had the help of VR pioneer Mark Bolars, who shared his trailblazing research with Luckey for free. Luckey also had the help of video game programmer John Carmack, who demoed Oculus Rift to the games industry.

All this assistance paid off: Facebook eventually bought the company for $2 billion.

So now that we've seen a few tech-billionaire origin stories, let's delve deeper into what it takes to turn a great idea into a phenomenal success.

### 5. Many billion-dollar companies have created simple solutions to small, everyday problems. 

What does it take to come up with a great idea? Surely it's a matter of thinking long and hard about life's great problems.

Well actually, no — not at all. To be successful, you don't have to come up with a formula for cold fusion or design a perpetual motion machine. Rather, success often lies in simplicity, in solving a small, everyday problem.

This was the case for Nick Woodman. After his first business idea failed, Woodman took some time off to surf in Indonesia and Australia. When he wanted to record some of his surfing adventures, he faced a problem: how was going to take pictures while riding the waves?

He came up with an incredibly simple solution and strapped a disposable camera to his wrist. When he told his friend about his DIY creation, his friend urged him to take his simple idea to the next level.

Woodman eventually created a wearable camera with durable casing, so that it would be impact resistant and waterproof. This tool developed into GoPro and Woodman went on to sell $1 billion worth of cameras.

Next, we have the story of Jack Dorsey. Dorsey is best known for his world-changing product, Twitter, but he also has a $1 billion stake in a payment processing company called _Square_.

Square was inspired by a real-life problem: Dorsey's friend, a glassblower, lost a $2,500 sale because he didn't have the tools to process a credit card payment.

Dorsey set out to find a solution, and ended up creating a very cheap smart-phone-based terminal that would allow small businesses to accept credit cards.

It was a very straightforward solution, and it was also Dorsey's second billion-dollar idea.

### 6. Challenging established companies and business models can lead to success. 

If you want to rise to the top, you're bound to upset a few people along the way. And tech-billionaires aren't scared to.

Consider Aaron Levie, the founder of _Box_, a company which allows you to share and edit files and documents on any device.

Before we dive into his story, it's important to note that for years, the computer software industry was dominated by four main companies: Microsoft, SAP, Oracle and IBM. Their dominance didn't lead to a lot of innovation in the tech industry: Since there was so little competition, clients paid high fees for tech services and upgrades; and furthermore, the quality was often not the highest. Even today, these companies lack many adequate products for the mobile and tablet markets.

For startups like Box, this presents an opportunity. The Box app allows people to share, open and edit files on any device. This kind of software has traditionally been Microsoft's domain, but the tech giant has been slow to adapt its Office Suite for mobile. And what's more, Box offers a free basic service without requiring users to pay for upgrades.

The company's nimble, efficient approach has proven to be a huge success: By 2013, Box had $124 million in revenues and even traditional powers were clamoring for a partnership.

But for startups, success isn't always about challenging established leaders. For companies like _Airbnb_, it's about creating entirely new ways of doing business.

Faced with a market completely dominated by hotels and guesthouses, founder Brian Chesky thought outside of the box and developed a whole new model for hospitality. Airbnb allows people to connect online to find private lodging while traveling.

Although the company's share of the hotel market is still small (Airbnb has $100 million of a $1 billion industry), established companies have taken note. Airbnb poses such a threat to the traditional way of doing business that the hotel industry is counting on government regulation to curb the company's growth.

### 7. For tech entrepreneurs, persistence and the ability to overcome failure are crucial to success. 

In the case of any challenging, competitive venture, success is largely a matter of persistence.

And the story of _Dropbox_ founder Drew Houston epitomizes this lesson. When he was just 14 years old, Houston already knew what he wanted to do. When a teacher asked his classmates what they wanted to be when they grew up, his hand shot up: "I want to run a computer company," he said.

From that point forth, he never gave up pursuing that dream. As a high school student, Houston started working as a beta tester for an online game. The company spotted his talent and hired him as a network programmer. Later in college, Houston worked for five more tech startups.

And then one day, Dropbox was born: Houston needed to pull up a few files, but realized they were on another computer. He started working on his idea right away, developing the basic technology which would allow him to synchronize files over the Internet.

Five years later, Houston has a $600 million stake in his $4 billion company — and he's made his childhood dream come true.

Of course, many aspiring entrepreneurs experience failure on their path to success. This was the case for Sean Parker, the former president of Facebook and founder of Napster.

Parker's path was riddled with failure. For example, while he was president of Facebook, police found cocaine in a house that was rented under his name. Although the incident led him to resign from the social network, Parker didn't give up.

He went to Spotify, the super-successful music streaming service which was then in its early days. Parker helped secure deals with major record labels like Universal and played an instrumental role in Spotify's social network integration. As you can see, Parker didn't let obstacles hold him back.

### 8. Successful entrepreneurs are prepared to tweak their products to develop what the users want. 

It's easy to feel protective of a company you've started, but resisting change will get you nowhere. The most successful companies know how to be flexible and respond to what their customers want.

Kevin Systrom, co-founder of _Instagram,_ exemplifies this principle. Today, Instagram is an incredibly successful photo-sharing network, which was recently purchased by Facebook for $1 billion. But when it first started, Instagram was a very different kind of company.

Systrom and his co-founder first created Instagram as a combination of Foursquare, which allows users to "check-in" their location, and a photo-sharing service. This concept fell short: the overall product was slow and cumbersome, and people didn't respond to the check-in feature.

But then, while they were vacationing in Mexico, Systrom had an idea. Why not add filters to the photography feature? These filters would allow users to add instant nostalgia or poignancy to everyday pictures. Within a month of introducing the filters, Instagram's user base grew to one million.

Facebook's other recent purchase, WhatsApp (which was purchased in 2014 for $19 billion), is another great example of a flexible startup.

Jan Koum, the brainchild behind Whatsapp, was a poor immigrant from the Ukraine. He and co-founder Brian Acton originally launched their company as an address book for the iPhone which allowed users to update their status (e.g. "at the gym," "battery low").

Although the initial idea didn't win over users, things changed when Joum and Acton combined the concept with Apple's new "push notification" feature. Now every time you updated your status, all your WhatsApp contacts would get a notification about it.

Remarkably, people started using the app in a completely different way than originally intended, as an instant messaging service. WhatsApp quickly changed its focus, and its user base grew to 480 million before it was acquired by Facebook in 2014.

We've learned a lot about how tech-billionaires achieved their fortunes. And now in the final blink, we'll look how they've managed to maintain their success.

### 9. Maintaining success is never easy; successful entrepreneurs will always face competition from other companies. 

So now that we've learned about how a few tech-billionaires managed to turn simple ideas into billion-dollar companies, it's time to find out what it takes to maintain that level of success. Because for tech wunderkinds, it's not enough to stop at the billion-dollar valuation.

This is especially true because the giant established companies — the ones which were challenged by the upstart entrepreneurs discussed in these blinks — are always trying to reclaim their market dominance.

Consider Palmer Luckey's story. (We've already talked about how Luckey was able to turn his design for a virtual reality device into a company, Oculus Rift, which sold to Facebook for $2 billion.)

Despite his immense success, Luckey (who still runs Oculus Rift, even after the sale to Facebook) still faces a challenge in keeping his company at the top of the market.

There are plenty of competitors encroaching on his turf: For one, electronic giant Sony is developing its own VR headset for its latest console, Playstation 4.

Amazon is also rumored to be entering the market. The e-commerce company may be planning to create VR shopping malls. So instead of just looking at a photograph of an item, these "malls" would allow you to virtually pick up a product, look at it in detail and even try it out.

Because of competitors like Amazon and Sony, for Luckey the work never stops. He must constantly seek out new ways to stay ahead of the competition.

And that's exactly why he used the money from the sale of Oculus to purchase other companies that might allow him to improve his product. Among his purchases were RakNet, an open source game networking engine (which allows people to create and share their own games), and Carbon Design Group, a product design studio.

### 10. Final summary 

The key message:

**In today's age of rapid technological innovation, there are plenty of opportunities for success. And as we can see from the stories of today's top tech billionaires, all you need is one great idea — and the courage and the determination to bring it to fruition.**

**Suggested** **further** **reading: The Innovators** ** _by_** **Walter Isaacson**

Walter Isaacson is an American writer and biographer. He was formerly the editor of TIME magazine as well as CEO and chairman of cable news channel CNN. Isaacson has written best-selling biographies of Albert Einstein, Benjamin Franklin and Steve Jobs, and is also the author of _American Sketches_, published in 2003.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Randall Lane

Randall Lane is the editor of _Forbes_ and the former editor-at-large of _Newsweek_ and _The Daily Beast_.

