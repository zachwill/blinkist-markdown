---
id: 547c8b5063633000096e0400
slug: youll-see-it-when-you-believe-it-en
published_date: 2014-12-03T00:00:00.000+00:00
author: Dr. Wayne W. Dyer
title: You'll See It When You Believe It
subtitle: The Way to Your Personal Transformation
main_color: 9A617E
text_color: 9A617E
---

# You'll See It When You Believe It

_The Way to Your Personal Transformation_

**Dr. Wayne W. Dyer**

_You'll See It When You Believe It_ is your guide to finding your true self. It will show you how to achieve harmony with yourself, your fellow people, and the universe at large. When you learn to transform yourself, you'll find inner peace and the strength to realize your greatest dreams.

---
### 1. What’s in it for me? Learn to transform yourself and realize your true potential. 

Many of us want to transform our lives. We want to feel happier, healthier or more fulfilled. But to achieve this, most of us concentrate on the wrong things. We think we can be healthier simply by hitting the gym, or we can be happier simply by changing our jobs.

These sorts of paths won't lead you to what you're really searching for. Instead of looking for happiness from the outside, you need to find it _internally –_ inside your mind. The only way you can truly transform your life is to change the way you think, and the way you understand the universe.

These blinks will show you what you need to do to achieve this kind of transformation. You'll learn how to let go of all external constraints, stop depending on others, and feel truly fulfilled by what you have and who you are. You'll also learn:

  * why letting go of things gives you more control;

  * why the universe is like a giant song;

  * why forgiving the people who've hurt you gives you more power;

  * what a remote-controlled car has to do with destiny; and

  * why nothing in life is a coincidence.

### 2. When you realize that 99 percent of who you are exists outside your physical body, you'll be able to fully transform yourself. 

Have you ever felt desperate to make positive changes in the way you do things? Most people feel this way at some point in their lives. But how exactly can we go about doing this? How can we transform ourselves into who we want to be?

Usually, when we decide to transform ourselves, we start by trying to change our bodies. We imagine that self-improvement starts with living a healthier lifestyle, so we go on diets or start exercising.

When we do this, we neglect an incredibly important part of ourselves: our mind.

You can only truly transform yourself when you realize you're much more than just your _form_. Our "form," or body, is the part of ourselves we can see, touch and smell. It can be detected by our physical senses. But, it's also only one percent of who we truly are. If each person was only a collection of bones, organs and muscles, then everyone would be the same.

In fact, 99 percent of what makes us who we are _can't_ be seen or sensed. It exists beyond physical comprehension.

This part of us is our _higher consciousness_ : our feelings, thoughts and mind. If you can concentrate on making positive changes to _this_ part of yourself, the change will be stronger.

Unlike your physical form, there are no restrictions on how much you can change your mind. You can exercise as much as you want, but eventually there will be a limit to how much you can bench press. Your higher consciousness is different, however: you can develop and transform it as much as you'd like.

This means that change is ultimately dependent on you. You're in control — there are no external barriers at all.

> _"Observe a tree and contemplate the intelligence within or behind that tree that allows it to function perfectly in form."_

### 3. Nourish your thoughts so you can transform them into reality. 

What does your favorite childhood memory have in common with your dream of a perfect future? Well, you experience both of them as _thoughts_, or images in your mind. In fact, everything you do, believe or experience comes in the form of a thought. Thoughts make us who we are.

When we learn to use our thoughts effectively, we can transform our dreams into reality. Here are some helpful steps you can follow:

First, realize that every single one of your actions is based on a prior thought. What you envision in your brain determines what you'll do. So concentrate on what you want.

If you constantly think of yourself as being shy in front of crowds, for instance, those thoughts will manifest as shyness in reality. You'll be afraid of public speaking because you'll imagine yourself failing.

On the other hand, thinking positively about public speaking will help you. If you imagine yourself as the world's best public speaker, you won't be afraid. So strive to visualize your perfect scenario for any situation.

Next, realize that whatever you think of, the ingredients to make it happen exist right here. This is especially challenging because we tend to think our imaginary perfect life exists in another reality. But that perception is invalid. Everything you will ever need for the perfect life is already here, you just need to find out how to use it.

Next, will your thoughts to happen. People often imagine that persistence is what they need to realize their goals, but that's wrong. What they truly need is _willingness_.

You need the will to do anything to turn your dream into reality. That might be moving to a new city, quitting your job or leaving your friends. Willingness is all that really matters.

Finally, remember that there's no such thing as failure if you are willing to try for your dream. So don't give up.

> _"We think as we choose, and we are what we think. Not what we eat, what we think."_

### 4. All people and all things in the universe are intimately connected as one. 

How many history documentaries have you seen? Probably quite a few. How many of them featured a war? Probably quite a few.

War and conflict are depressingly common themes in human history. Why?

Well, we tend to think of ourselves as individuals separate from everyone else. Therefore, we pursue our selfish, individual goals, which leads to conflict.

Fortunately, there's another way to live! We have to realize that we aren't separate individuals — we're all part of the same whole.

To understand this, consider the way our bodies work. Every person has billions and billions of bacteria on his body. We all have bacteria in our intestines, eyelids, toenails and pancreas, and these bacteria enable us to live.

Even though these bacteria are all different and most of them never interact with each other, they're all part of one whole: _us_.

We should think of reality in the same way. Even though we'll never meet most other people in the world (or other beings in other worlds), we're all part of the same whole: _the universe_.

When you understand this, you'll immediately realize why we need to live harmoniously with each other.

Think of our bodies again: we're all made of billions of cells. If even just a few of them decided to work against the rest, we'd be in serious trouble. In fact, this is what cancer is. Cancer occurs when rogue cells decide to attack the others.

The world works the same way as the body. If we want to live in the happiest and healthiest environment we can, we must live in harmony. If even just a few people reject this idea, we'll all be in danger.

> Fact: The word "universe" can be translated into "uni"_–_ one and "verse"_–_ song. We're all part of one song.

### 5. When you believe in abundance, your life will have no boundaries. 

How many of us are devoting our lives to something we love? Sadly, hardly any of us. Instead, most of us have jobs that place limitations on what we truly want. We can't wear our own clothes, or work when we want or where we want.

In short, our freedom is limited. Why do we do this to ourselves?

We do this because most of us live with a _scarcity mentality_. This means that we concentrate on the things we don't have enough of.

We often find ourselves thinking, "I'd be happier if only I had more of this..." It could be money, time, opportunities, or anything else.

For example, our perceived scarcity of money is what pushes us into unhappy jobs. We spend our time working at jobs we don't like because we're consumed by our fear of not having enough money.

This scarcity mentality doesn't just stop us from doing what we love — it also limits what we can achieve in life. We've already seen how our thoughts determine our actions. If we go through life thinking about not having enough, these thoughts will surely become a reality.

So what's the alternative? We need to embrace the _principle of abundance_.

The principle of abundance means that there are no limits to what you can achieve. The universe has no boundaries, and neither does your mind. Anything is possible. You just have to believe it.

It's difficult to think this way, but there are some things you can do to work towards it:

One such tactic is to show your gratitude for _everything_. Be thankful for the sun that gives you energy, your friends and family, and the simple fact that you're alive. When you focus on what you're deeply thankful for, you'll realize that there's actually very little scarcity in your life. You already have everything you need to achieve whatever you want.

### 6. When you stop being unhealthily attached to things, you'll become a better person. 

When you examine the entirety of human history, one thing will become very clear. Those of us in the West currently have a higher standard of living than anyone else who's lived before. It's also higher than most people living today.

This wealth should make us happy, shouldn't it? Well, it doesn't, and in fact the opposite is true. Most of us are unhappy and lonely. Why?

Well, our attachment to our possessions is actually disrupting our lives. We become so dependent on certain things that we feel we can't live without them.

These unhealthy attachments aren't necessarily always related to material objects. We can be overly attached to other people, money or the past. We can become too attached to anything we feel we need to survive.

This stops us from thinking about the things that really matter. It stops us from living fulfilling lives.

To understand how harmful attachment can be, consider this analogy: when you eat a piece of lettuce, it goes through your digestive system without you controlling or even thinking about the process. Your body absorbs the nutrients and expels the waste, all in perfect harmony.

This same thing happens in society, and the universe at large. Things flow naturally and perfectly.

However, our unhealthy attachments stop this natural flow. It's part of the flow that things will leave us, and we have to learn to let them go. When we can't let something go, we disturb the natural rhythm of the universe.

So, stop craving the things you don't need, and instead, practice detachment. You'll find yourself flowing through life with much greater ease.

> _"Everything that has to happen had to happen; everything that must happen cannot be stopped."_

### 7. When you allow yourself to flow through life, you’ll experience synchronicity. 

Have you thought about a long lost friend, then suddenly they called you the next day? Maybe it was just a coincidence. But perhaps you also connected on a spiritual level.

The more we let our lives flow, the more we'll experience this kind of _synchronicity_.

Synchronicity occurs when your friend calls you after you think of them, when you experience deja vu, or when you run into someone you've just had a dream about.

Synchronicity can seem like a random connection between separate events, but it's not. It's much more, because everything in the universe happens for a reason.

This phenomenon is difficult to comprehend because we fail to see the links between our thoughts and our reality. So if we experience synchronicity, we often just assume it's a random coincidence.

It's much easier to understand connections we see visually. Imagine a child hangs a toy on a piece of string. We understand the connection between the toy and string because we can clearly see it. Connections in synchronicity however, are invisible, so they're more difficult to make sense of.

Thinking about a remote-controlled car can help you overcome this. We know the car and the controller are connected, even though it's not physical. The same thing happens with synchronicity: everything is connected, we just don't see it.

When you accept synchronicity and the interconnectedness of the universe, you'll realize that everything happens for a reason. The path of universe is already laid out — there is no randomness. Therefore there's no point in worrying about things you can't change like your social position, your possessions or your job.

### 8. You have to embrace forgiveness if you want to achieve true transformation. 

After reading this, you might be thinking you're well on your way to transforming your life and reaching higher consciousness. There's still one incredibly important part, however: _forgiveness_.

We're constantly putting blame on other people, and it prevents us from reaching our true potential. If you truly want to transform, you have to learn to forgive people instead.

What do you do when someone does something bad to you, or something you don't like? If you're like most, you probably get angry. You might hate the person, or blame them for creating a problem in your life.

What do you gain from blaming them? Nothing. In fact, concentrating your negative emotions and thoughts on someone only gives them more power in your life. It stops you from concentrating on what you really want to achieve.

You also need to remember that everything happens for a reason, even painful things. Every bit of pain someone else causes you is a lesson you can learn from.

So, instead of hatred or resentment, show forgiveness to the people who've wronged you. You'll be able to let go of negative thoughts, so you can focus on the thoughts that really matter.

Remember to forgive _yourself_ too. Most of us have done things we regret in life. Maybe we've done something that goes against our principles, like lying to a loved one or eating that whole bag of chips. This makes us feel guilty or ashamed.

What does our guilt achieve? Again, nothing. If you beat yourself up about something, you're only more likely to do it again.

Instead of feeling guilty, forgive yourself. Reassess if what you did matches your values, and move on. Don't dwell on the past or on your mistakes. Focus on the future instead.

### 9. Stay focused on your transformation, and implement awakening in your life. 

Now that you know the secrets of personal transformation, here are some tips for applying them in your daily life.

First, make an effort to regularly go outside your comfort zone. Be kind to yourself everyday, and view every experience and thought as a learning opportunity. Also, remember to be thankful for what you have and what you are, and remind yourself that you deserve only the best from life.

Meditate. Go to a quiet place, close your eyes and pay attention to your breathing. This will calm your mind and pull you back in the present moment, which is the only moment that truly exists.

You'll be able to accomplish more when you're relaxed and free inside. So stop worrying, quiet your mind, and practice trusting your intuition — it's definitely your best advisor. Just surrender your life to the forces and principles that are always at play in our perfect universe.

Also, commit yourself to forgiving the people you've conflicted with. Send them a gift or call them to see how they're doing. And don't forget to wish them well!

Accept that abundance is coming towards you when you let things move in their own rhythm. Imagine you have a guardian angel or other loving observer as a part of your consciousness.

Ultimately, just let things happen, and remember that you're perfect exactly as you are.

> _"I celebrate the place in you where we are all one."_

### 10. Final summary 

The key message in this book:

**The universe is perfect. Everything in it is connected, and everything happens for a reason. When you accept this, and learn to flow and forgive, you'll find harmony with yourself and others. That harmony is part of your higher consciousness — and it will allow you to transform into what you truly want to be.**

Actionable advice:

**Transform your mind, not your body.**

The next time you feel bad about your body, remember that it's only one percent of who you really are. Concentrate on transforming the remaining 99 percent — your mind — and the transformation will be much more meaningful.

**Suggested further reading:** ** _Awaken The Giant Within_** **by Anthony Robbins**

_Awaken The Giant Within_ argues that, ultimately, we're all in control of our own lives, and that by changing our habits, controlling our emotions and believing in those things we want to believe, we can make our ideal life a reality.
---

### Dr. Wayne W. Dyer

Dr. Wayne W. Dyer is a motivational-speaker, psychotherapist, lecturer and world–famous author. His first book _Your Erroneous Zones,_ written in 1976, is one of the best selling books of all time, with an estimated 35 million copies sold.

