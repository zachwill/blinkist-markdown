---
id: 547c9b5563633000099c0400
slug: young-money-en
published_date: 2014-12-03T00:00:00.000+00:00
author: Kevin Roose
title: Young Money
subtitle: Inside the Hidden World of Wall Street's Post-Crash Recruits
main_color: 20A29C
text_color: 16706C
---

# Young Money

_Inside the Hidden World of Wall Street's Post-Crash Recruits_

**Kevin Roose**

Kevin Roose spent three years following eight young Wall Streeters in an attempt to find how the 2007 crash — and its aftermath — influenced the financial industry. _Young Money_ paints a decidedly grim picture of junior analysts who find themselves on a non-stop rollercoaster of all-nighters and extreme stress, while earning six-figure incomes.

---
### 1. What’s in it for me? Learn what it’s really like to work as a young analyst on Wall Street. 

The 2007 financial crisis started on Wall Street, but was felt around the world. In the years that followed, public pressure forced the financial gods to re-organize their small Manhattan kingdom. _Young Money_ shows us Wall Street through the eyes of young financial analysts, who enter business fresh from their Ivy League ivory towers, only to find Wall Street less comfy than they'd imagined.

They're paid enormous salaries, but the punishing work schedules they're assigned make you wonder: Is it worth it?

In the following blinks you'll learn

  * how an arts major can easily become a Wall Street analyst;

  * what the top one percent's secret club gets up to; and

  * how earning $100,000 a year might not make you much richer than someone working for minimum wage.

### 2. Wall Street hiring is aggressive and begins at college. 

Scoring a job on Wall Street seems like a rare honor. But in fact, companies on Wall Street are as eager to hire a smart young person right out of college as graduates are to make it to Wall Street.

Wall Street firms begin their search early to ensure they get only the best graduates, and that means bagging them before they've finished their degrees.

Well before other fields even consider setting up interviews, Wall Street firms start pursuing young minds on university campuses.

As early as the fall of their senior year, most Wall Street firms inform student applicants about job openings. Take a look at the statistics: the majority of Ivy League graduates who have job offers prior to graduation are going into the financial sector. That's 46 percent in Princeton in 2006 and 28 percent in Harvard in 2008.

The best Wall Street companies spare no expense on enticing the smartest young minds and ensuring the hiring process is as simple as possible. But although it's an easy process, it's also aggressive.

Top firms hold presentations on campus, often boasting Jay Z-style promotional videos. One presentation by Morgan Stanley at Penn's Wharton Business School spouted phrases like "Boundaries will be shattered," and "In the finance world, every day is a new day," to a pop-rock soundtrack.

As well as the presentations, companies also provide interview workshops, treat potential candidates to expensive dinners and follow up with phone calls. The interest they show in exceptional students is comparable to "polite stalking."

So do any students actually fall for these gaudy presentations? You'd be surprised.

> _"Make no mistake: financial firms will never have a problem filling their ranks with smart, capable 22-year-olds."_

### 3. Most people end up at Wall Street because they’re unsure what else to do with their lives. 

When we think of Wall Street we think of finance, right? Of course, but it's not essential to have a background in finance in order to work there.

All Wall Street firms want to hire students from the best schools — period.

Wall Street firms look specifically to Ivy League colleges such as Harvard, Penn and Brown when they're hiring. This means all students there are very well positioned to land a job on Wall Street, even if they're an arts major.

There are some exceptions, however. Young analyst J. P. Murray (some names in these blinks have been made up to protect interviewees' anonymity) was one of the few recruits to _Credit Suisse_ who came from a regular college rather than an Ivy League school.

So what is the main attraction of Wall Street for students? Sometimes it's simply because it's the easiest thing to do when they graduate.

Wall Street recruitment begins with a "two and out" plan, meaning that graduates are hired as analysts for only two years at first.

Because the pay is so alluring, and because students often graduate with massive debts, heading to Wall Street seems like a pretty grand idea. After all, two years' work seems like a fairly good trade off for paying your loans and gaining valuable business skills.

A lot of students think this way and this results in a class of "accidental financiers" who spurn the threadbare job market and snap up an early offer, convincing themselves it's just for two years.

So, picture yourself with $100,000 in student debt and add to that the uncertainty of what to do with your life after you graduate. Of course Wall Street is going to seem appealing, even if finance isn't exactly your passion.

It does seem tempting, but the following blinks will reveal some pretty heartbreaking stories from young financiers.

### 4. The working conditions for new analysts on Wall Street are shocking. 

All right, so you've gotten yourself an enviable spot on Wall Street. Or so you thought. Now let's see exactly what that entails.

A first year analyst on Wall Street will typically work themselves to the bone. They are known to clock up 100 hours of work a week, meaning 16-hour weekdays and ten hours on Saturdays and Sundays. If you add commuting times on top of this, you wouldn't be the only one wondering how they even find time to sleep.

For the majority of analysts, the hardest thing about the long hours is that they need to be constantly available. They might find the odd hour to sit around, but they are also expected to jump into action and start collecting information for the next project at 3 a.m., or on Christmas morning if necessary.

They basically work non-stop, even if their actual working time is "only" 16 hours per day.

Ricardo Hernandez, an analyst at J. P. Morgan, sometimes works what is known as the "banker nine-to five," starting at nine o'clock one morning and finishing at five o'clock the _following_ morning, for weeks at a time.

In case the long work hours weren't a big enough drawback, the way young analysts are treated by bosses is also appalling. Most first-year analysts are dealt with unfairly by their superiors; every time you put a foot wrong, expect to be yelled at for it. The reason? It's always been like that.

What if you perform well? Then you can enjoy your annual bonus — but forget about encouragement and support.

When Bank of America Merrill Lynch analyst Chelsea Ball made a mistake with a newsletter she was supposed to compile, none of her bosses stood up for her. Even though they had access to the information and could have spotted the error easily, they chastised her for adding confidential information which she couldn't have known _was_ confidential.

> _"Being young on Wall Street has always been a bizarre combination of glamor and masochism."_

### 5. Most people who go to Wall Street sacrifice their personal lives and their health. 

When you're a Wall Street analyst subjected to massive daily pressure while putting in 100 hours a week, the consequences are serious.

What usually ends up happening is that most young analysts barely have a personal life.

Seeing how little time he had for her, the girlfriend of Derrick Havens, an analyst at Wells Fargo, eventually proposed an ultimatum: the job or the relationship.

While she understood the long hours and the constant need to be on call, Derrick chose work over a romantic dinner one too many times.

Yet, faced with the ultimatum, Derrick chose work, and threw away a four-year relationship.

There are a few factors that keep analysts at Wall Street cocooned in their office buildings. Some firms have in-house gyms, coffee shops and even barbers. This way, bosses limit the contact employees have with the outside world.

Aside from having an adverse effect on relationships, working around the clock also affects physical health.

Extreme sleep deprivation, huge stress levels and limited movement make a dangerous cocktail that often ends up endangering the health of Wall Street analysts — a risk they claim to be ready for.

One extreme example is Arjun Khan, a young analyst at Citigroup.

Arjun was diagnosed with Goodpasture syndrome, an autoimmune disease that causes organs to collapse.

It's not likely that the disease itself was directly caused by his work, but Arjun's lifestyle habits, such as never exercising, staying up all night and partying with colleagues, likely exacerbated his condition and finally landed him in the hospital. From there he was forced to seriously review his career choice.

### 6. For most young people, working in finance is a job like any other. 

During the protests after the 2007 market crash, young analysts could relate far better to the protesters than the older, more seasoned Wall Street professionals.

Occupy Wall Street was a movement against everything Wall Street stood for, including the insatiable and ruthless pursuit of power and money.

Older bankers, however, weren't so concerned, as they didn't think the protesters' demands were credible.

But the younger analysts felt differently. They were the same age as the protesters themselves, and many of them had friends or family members who backed the movement.

For instance, when protesters marched under Goldman Sachs analyst Jeremy Miller-Reed's window, he felt he was on the wrong side of the glass. He started to lie about his job when he spoke to others, because he was ashamed to work there.

Miller-Reed was aware of what Wall Street represented to many, but other young analysts appeared not to understand the full impact of their work.

For a lot of Wall Streeters, their job is like any other. Even though they earn more money than people in most other industries, they still fall within the "99 percent" of the population. They almost never get to have their say on deals and are often assigned mundane support work such as working on Excel sheets or pitch books.

This is why one J. P. Morgan analyst was angry that the public didn't differentiate between the people at the top and the juniors like himself. He even went so far as to say it was like accusing all the athletes on a team of a heinous crime, when only one is guilty.

### 7. Working on Wall Street changes you for the worse. 

So far we've focused on first-year Wall Street analysts. But what happens to those who decide to stay on after 12 months? Working on Wall Street starts to have a major impact on the personalities of the people who work there.

After the first year, most analysts finally get to enjoy some spare time and freedom. Many of them start seeking out more easygoing positions, such as jobs in private equity or hedge funds.

Although most young analysts on Wall Street just want to get through the first two years and leave, they often end up changing their minds.

Some analysts succumb to the temptation to stay on because they get accustomed to the atmosphere and to maintaining a secure, well-paying job. Suddenly this seems a lot more appealing than changing careers.

But for those who acknowledge the Wall Street mentality and continue, slight personality changes begin to take place: they go from being upbeat, happy graduates to frayed, short-tempered worker bees.

Relationships with others start resembling business transactions, and cynicism becomes a normal reaction. Remember Derrick, who chose his job over his girlfriend of four years?

Those who stay on Wall Street can easily become part of the "one percent," but they pay the price with their values and morals.

When the author approached top Wall Street executives to ask what their lives were like, he only received PR-department answers.

He therefore decided to sneak into the induction dinner of _Kappa Beta Phi_, a secret club founded in 1929 for the top one percent. Once there, he witnessed an extravagant dinner with a stage full of "neophytes," each of whom was expected to do a trick. The grand finale of the humiliating show included the newcomers wearing Mormon missionary outfits, singing about God's plan for them to earn seven-figure bonuses.

### 8. There are many other industries that offer adequate compensation, with far better work conditions. 

If you're weighing up the pros and cons of working on Wall Street, first make sure you consider what alternatives are available.

Being on Wall Street for two years might seem like the path of least resistance for some, but it's unlikely to be a route that will make you happy.

It's easy to forget that two years can be a long time when you're trawling through tedious, almost meaningless work which prevents you from having any free time.

People with creative potential shouldn't forgo their talent for the security of a lucrative job. Society pays a big price when individual talent and creativity are squandered.

Jeremy Miller-Reed, the Goldman Sachs analyst, eventually had an epiphany and decided to leave Wall Street in order to start his own company. He announced this with a Facebook status update reading "The nightmare is over."

Miller-Reed understood that Wall Street's monetary compensation was ultimately not worth it.

So how much do young analysts receive for selling their soul? The figure might sound impressive before you understand the sacrifices they make for it.

Most analysts on Wall Street earn a base salary of about $70,000 per year, topped with a yearly bonus of between $20,000 and $90,000. Clearly these are salaries not to be sniffed at; even with a bonus of "only" $30,000, this places the analysts among the top earners in the country.

However, when you take into consideration the number of hours they work, this sum turns out to be rather underwhelming.

One analyst took into account his overtime and what this would amount to as an hourly wage. The result was around $16. Not exactly worth toiling away for 100 hours per week.

Finally, let's take a look at the future of Wall Street and its analysts.

### 9. Since the crash and the protests that followed, potential recruits have become cautious. 

The 2007 financial crisis had a massive impact on Wall Street and on the desires of graduates to sign up.

A number of regulations were introduced for financial institutions, and there was a drastic fall in profits. This in turn led to a shake up involving layoffs and diminished bonuses.

All of a sudden, a position on Wall Street was no longer a secure option for talented college graduates.

A two-year contract couldn't be guaranteed and bonuses shrank, so the money no longer compensated for the sacrifices junior analysts were expected to make.

The public protests against Wall Street also affected young people's opinions of it. After Occupy Wall Street, many college students began to see the industry very differently. Students began blogs and wrote articles in college papers in order to dissuade their peers from being lured into working there.

Their efforts were successful. The percentage of Harvard graduates who took jobs in finance after their diplomas dropped from 28 percent in 2008 to 17 percent in 2011.

Not put off yet? While a job on Wall Street doesn't make you evil, and no one can say that it should be forbidden, you should always ask yourself first if finance is your real passion. Because if not, you'll find the costs of working there far outweigh the benefits.

> Fact: In 2011 the program _Teach for America_ attracted more Brown and Columbia graduates than Goldman Sachs.

### 10. Final summary 

The key message in this book:

**Working on Wall Street as a young analyst is far less glamorous than most people think. The perks often come at a price of countless sleepless nights, 100-hour work weeks and extreme pressure. However, after the 2007 financial crash and the Wall Street protests that followed, the new public consciousness may well have saved many creative people from landing a job that destroys their lives.**

**Suggested further reading:** ** _Liar's Poker_** **by Michael Lewis**

_Liar's_ _Poker_ tells the story of Salomon Brothers, a leader in the bond market in the 1980s. This tell-all account of the author's experiences at Salomon Brothers explains how the firm became one of the most profitable investment banks on Wall Street through its role in establishing the mortgage bond market, and what it did once it reached the top.
---

### Kevin Roose

Kevin Roose is the author of _The Unlikely Disciple_, and has written for the _New York Times_ and _New York Magazine_. He is currently senior editor and co-executive producer for the _ABC_ — _Univision_ joint venture, _Fusion_.

