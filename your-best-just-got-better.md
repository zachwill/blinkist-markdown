---
id: 552bd7e964663400071e0000
slug: your-best-just-got-better-en
published_date: 2015-04-13T00:00:00.000+00:00
author: Jason W. Womack
title: Your Best Just Got Better
subtitle: Work Smarter, Think Bigger, Make More
main_color: 1A3B75
text_color: 1A3B75
---

# Your Best Just Got Better

_Work Smarter, Think Bigger, Make More_

**Jason W. Womack**

_Your Best Just Got Better_ (2012) outlines the most effective techniques for setting your goals and using your time to work toward them. These blinks reveal which parts of your life are draining your motivation, as well as what you can do to bring your ambition to the next level.

---
### 1. What’s in it for me? Learn how to maximize your potential in life. 

Have you ever stopped to think if your working day is as effective as it can be? Are you really doing all that you can do? Chances are you aren't.

Truth is, we are all suffering from a distinct lack of efficiency in our daily lives. For a start, just think about how much time you spend every day being interrupted by colleagues. It might seem trivial to talk about the game last night with a workmate, but do that half a dozen times and you've lost an hour.

We are far from working at our full potential, but what can we do about it? Luckily these blinks have the answers. Read on and you will discover how to start working toward your full potential.

In these blinks you'll discover

  * why life is a marathon not a sprint;

  * why you should evaluate the people you work with; and

  * how to stay motivated at every stage.

### 2. Set your goals, manage your goals, and take action now! 

_Just Do It –_ you're probably familiar with this catchy Nike tagline. But have you ever considered that those three words could be much more than an ad campaign? _Just Do It_ is one of the simplest yet most valuable pieces of advice for anyone who wants to achieve _any kind of goal_. Read the slogan again, and this time think — what does the _It_ mean for you?

Defining exactly _what_ you want to do will jumpstart your journey toward working smarter. Setting a clear goal takes self-honesty, which you can achieve just by standing in front of your mirror in the morning and asking yourself the following question: "If I could be better at one thing, what would that be?"

Then, when you get home at the end of the day, return to the mirror and ask yourself another question: "What did I do today to bring myself closer to achieving my goal?" Let's say your goal is to run a marathon. Did you go jogging? Take steps toward a healthier diet?

This leads us to another vital aspect of setting your goals: breaking them down into smaller parts. For instance, your marathon dream may seem daunting. Why not break up your training into smaller units? You could start with 5 km, then move to 10 km, and work your way up. If you can turn your seemingly huge goal into a series of manageable tasks, taking action becomes so much easier.

It's also important that you approach your tasks one at a time. Though it's great to be excited about achieving your goals, always be careful not to get ahead of yourself. If you're anticipating doing too many things at once, you'll find your energy and motivation draining away.

This applies not just to tasks related to your goals, but to other distractions too. Read on to find out how you can combat this and make the most effective use of your time!

> _"A goal properly set is halfway reached." — Zig Ziglar._

### 3. Keep a good pace and value your time. 

What tasks did you put off today? We all postpone things on our to-do lists, promising we'll come back to them as soon as we have a little more time, while the list only grows longer and longer. This will set you back as you chase your goals, but you can combat it by _keeping good pace._

So how do you find a good pace? It's all about balance: you shouldn't move so fast that you run out of steam before the day's over, nor should you go so slow that you lose motivation. If you can find the middle ground that you're happy with, then you'll have found your pace. And, once you've found it, stick to it!

The author had to learn the lesson of sticking to his pace the hard way. An avid triathlete, he was running in the Wildflower Triathlon and found that he'd completed the first mile in just six minutes, though his planned pace was eight minutes per mile. Sure enough, by the five mile mark he hit the wall and had to slow down. This wouldn't have happened if he'd kept to his pace!

Keeping pace is one way to use your day more effectively. But there's another way to make the most of the time you have. How? Simple — by _valuing_ it!

Consider this: 15 minutes is around one percent of a day. Still, it's enough time to get a lot of things done. When the author had to wait 15 minutes for a meeting, he managed to write a thank you card, review his calendar for the coming three weeks, book his hotel and rental car for an upcoming trip, check his voicemails and briefly plan out an article.

Think about that the next time you've got 15 minutes to spare. Inconvenient waiting times can turn into windows of opportunity to tick those little items off our to-do lists. This in turn will free up mental space for more important tasks, which is vital, as we'll see in the following blink.

### 4. Move closer toward your goal by eliminating distracting tasks – and people. 

You know that feeling when you're reading a book, find your mind wandering and suddenly reach the bottom of the page with no clue about what you've just read? Distractions are seemingly ever-present! But if we want to get through that book, or move closer to our goals, we've got to fight them.

Distractions often weigh upon us and keep us from focusing on the present moment, sometimes more than we realize.

Once, a new executive coaching client asked the author "Where do we begin?" He responded by placing a ream of paper in front of her and asking her to write down one thing that she was thinking of on the first sheet. Then he asked her to put the sheet aside and write down the next thing she was thinking about on the following page, and so on.

Nearly all of the things she wrote down were tasks she had to do, from hiring a new employee to organizing a summer camp for her daughter. After four hours, she had identified 500 things that required her attention!

It's easy to see how having so many uncompleted tasks would drain your energy and draw you away from your goals. Ticking those distractions off your to-do list is a crucial step toward working smarter.

But it's not just errands that can distract you from your goals — people can too. How much time do you spend with people that limit your thinking?

Try writing a daily assessment of the people you spent the most time with and how they affected your productivity and engagement. After a couple of days you'll see which people truly inspire you. These are the people you should try and spend at least an hour or two with each week or month. This will keep you both motivated _and_ open-minded.

### 5. Track your productivity and remember why you do what you do. 

We can do our best to complete everything on our to-do list, but there are still ways that we're losing time without even realizing it. How often do you have to interrupt your work to chat to colleagues during the day, for example? Over time these interruptions add up.

So how can we manage them? By tracking them, measuring them, and making a change.

One of the author's clients measured how many times he'd been interrupted by a colleague over two days: 27 times! You can imagine how this must have damaged his productivity. Having measured and realized how much time was being wasted, the client knew he had to do something about it.

Try measuring your lost time for yourself, for example, by how much time you spend away from your desk, or how many times someone asks "for a minute." You may be shocked at what you find!

Keeping track of your productivity on an everyday basis is incredibly helpful, but it can't stand alone. It's also important that you continue to know _why_ you do the things you do. Keep asking yourself what your true purpose is — this will keep you on the right track.

A great way to evaluate your sense of purpose is by formulating some "So that..." statements. It could be things like "I work _so that_ I can send my children to college." You can keep these statements somewhere you'll see them every day, to keep you inspired to achieve the things that matter.

> Fact: According to a report in the _Harvard Business Review_ we lose on average 96 minutes of productivity every day to interruptions.

### 6. Seek feedback, listen, and never stop improving! 

Despite what we might think, nobody can truly change all by themselves. Luckily, the world is full of people who have different perspectives that you can learn from as you work toward your goals. All you have do is ask!

But first, consider the last time someone offered you constructive criticism. Was it from your boss? Your spouse? What exactly did they say?

If you can't remember, it's probably a sign that you should listen more carefully. One way you can improve your listening skills is by seeking feedback more often, and seeking it actively. Don't just wait for it!

A great way to gain more feedback is by creating a mentor/mentee program. This is how it works: Think of someone you think could meet for weekly sessions over the next two months. During these meetings explain your current projects to your partner, listen to their feedback, and then listen to their projects and offer them feedback in turn.

Feedback is a great way to ensure you're constantly improving. But if you're serious about growing, you've got to make sure you never rest on your laurels, no matter how tempting it may be!

Once we've settled into a job or done something for a long time, we tend to get comfortable and assume we know the best way to complete our tasks. But if you want to achieve more, never let yourself get comfortable — instead, keep practicing!

One way to do this is by adopting the mind-set of a beginner. Take the author's mentor, Jim Polk, for example. Even though he's a seasoned pilot with hundreds of flying hours, he still practises the basic lessons he did when he first learned how to fly.

> _"I like to listen. I have learned a great deal from listening carefully. Most people never listen." - Ernest Hemingway_

### 7. Final summary 

The key message in this book:

**Setting clear goals, eliminating distractions and valuing your time will allow you to work smarter. Your ambitions will never be out of reach when you keep your focus on them and surround yourself with people who inspire you.**

Actionable advice:

**Make the most of your waiting time!**

If you've got a few minutes to spare, try writing a short thank-you card to a colleague, or anyone who has helped you out lately. Not only will this keep you productive, it'll also help you focus on the present and the things that you're thankful for — a better energy boost than any cup of coffee! So make sure you always have some postcards and stamps with you, just in case.

**What to read next:** ** _The Effective Executive_** **, by Peter F. Drucker**

As you've just discovered, identifying your goals and measuring your progress is crucial to success. But what about helping others do the same? After all, leaders and managers must to do more than guarantee their own success; they have to ensure the success of those around them.

In _The Effective Executive_, Peter F. Drucker highlights the techniques and principles that make certain managers so great. To get insights into these success-ensuring techniques — and to find out what you can learn from FDR's policy failures — check out the blinks to _The Effective Executive_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jason W. Womack

Jason W. Womack is an acclaimed speaker and coach of CEOs, managers, employees and entrepreneurs. He has held more than 1,200 workplace performance workshops around the world, sharing his expert advice on how to work more intelligently and boost performance.

© Jason Womack: Your Best Just Got Better copyright 2012, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

