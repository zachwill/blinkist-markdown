---
id: 533ae3593430340007090000
slug: your-brain-at-work-en
published_date: 2014-04-01T09:44:53.000+00:00
author: David Rock
title: Your Brain at Work
subtitle: Strategies for Overcoming Distraction, Regaining Focus, and Working Smarter All Day Long
main_color: 9BDA58
text_color: 467317
---

# Your Brain at Work

_Strategies for Overcoming Distraction, Regaining Focus, and Working Smarter All Day Long_

**David Rock**

_Your Brain At Work_ (2009) explores the inner workings of our brains and provides many methods for us to optimize our thinking. Drawing upon thousands of neuropsychological studies conducted in the last 25 years, the book presents many strategies that will help us overcome distraction and become more focused.

---
### 1. What’s in it for me: find out how to gain control over your brain. 

More and more, it's becoming typical for people to work in occupations that are mentally and creatively demanding. But with the constant advances in communications technologies, we're also increasingly surrounded by potential distractions, which means that using our brains optimally will only get more difficult.

By exploring how our brains function in everyday life, _Your_ _Brain_ _At_ _Work_ provides many effective strategies for staying focused amid the noise and delivering great work under pressure. In short, the book will help you to not merely survive but to actually _succeed_ in today's demanding work environment.

In these blinks, you'll discover that pushing yourself to work harder when your mind is already exhausted will always lead to poor performance.

You'll learn why leaving your smartphone on and your email open while you work can actually lower your IQ.

You'll discover why it's always better to help others reach their own insights than to give them direct feedback and instructions.

You'll find out why making yourself laugh when you're faced with a challenging problem can actually help you to find the solution.

Finally, you'll learn how to trick your brain into being interested and alert simply by using your imagination.

### 2. Your ability to think well is a limited resource, so conserve the resource at every opportunity. 

We all know what it's like to "burn the midnight oil." The later we work into the night, the less we're able to think clearly, and when we notice that our attention is waning we tell ourselves that we should simply try harder.

But there's reason to believe that you should give your brain a break instead.

We use a massive amount of energy in all our interactions with the world, which fatigues our ability to think clearly. This indicates that our capacity for active thought is limited.

Evidence for this limitation can be found as early as 1898, in a study where subjects were instructed to perform a mental task while putting as much physical pressure as possible on a machine that measures force (a "dynamometer").

The results revealed that, when the subjects were engaged in active thought, their maximum physical force was reduced by up to 50 percent.

Furthermore, as you'd expect, performing more than one conscious process simultaneously is even more taxing. The result is that our performance quickly declines when we try to do several mental tasks at the same time.

For example, one study indicated that the constant distraction of emails and phone calls reduce performance in an IQ test by 10 points, on average. This reduced mental capacity is similar to that we experience after missing a night's sleep. One explanation for this effect is that such interruptions force the brain to spend too much time in a state of alertness.

So if we want to maintain a good level of performance, we have to conserve the brain's energy for only the most important tasks.

This can mean prioritizing certain tasks above others. But be aware that prioritizing is itself a task that drains energy, so make sure you prioritize when your mind is alert and fresh.

Another way to conserve energy is to turn tasks into routines, as these can be stored as patterns that won't require you giving your full attention to a task.

### 3. Our attention is very easily distracted, but there are strategies for staying focused. 

In many ways, advances in modern technology have certainly made our lives easier. But the convenience they provide also comes at a great cost to our ability to focus: the ubiquity of always-on technologies, like the internet and smartphones, means that we have more potential distractions in our lives than ever before.

Whenever we're distracted, our attention is diverted, and refocusing that attention takes time and effort.

In fact, one study revealed that over 2 hours of office workers' time is consumed by distractions. Another study showed that we spend around 11 minutes on a task before we're distracted, and don't return to our work until 25 minutes later.

And it's not only external distractions we have to deal with. There are also many _internal_ distractions: the constant stream of thoughts that surface into consciousness and impair our focus — like wondering whether we paid the electricity bill or not.

Moreover, every time we try to resist being distracted, we decrease our ability to do so. This is because self-control is a limited resource.

This was demonstrated by a famous study, where participants were placed in a room, alone, with a bar of chocolate. Some of the participants were instructed to resist eating the chocolate, while others were allowed to eat it. Afterwards, they were all given a challenging mental task to perform.

Those who had managed to resist eating the chocolate gave up on this mental task much sooner than those who hadn't exercised their self-control, suggesting that self-control can become fatigued simply through being used.

So, how then can we maintain our focus when we're surrounded by potential distractions?

Put simply, you have to prevent those distractions from seizing our attention. One way to do this is to develop the habit of "vetoing" those behaviors that distract us. This requires us to turn off all communication devices whenever we need to do any kind of active thinking.

### 4. Optimal mental performance requires just the right level of arousal of your brain. 

Which would you watch more intently: a once-in-a-lifetime solar eclipse that lasts only a minute, or the tree outside your window?

If you're like most people, you focus better when you're faced with a tight deadline, or when you are presented with something new and interesting.

That's because being alert and interested sharpens our focus.

For us to be alert, our brains require exactly the right level of norepinephrine, the chemical that's triggered when we're scared. Fear activates our alertness, and alertness is a crucial part of being focused.

And for our interest to be aroused, our brains need just the right amount of another chemical: dopamine. Dopamine is triggered whenever we experience something new or unexpected, which means it's an important factor in our becoming interested in something.

However, for our brains to function at their peak, it's crucial that the levels of norepinephrine and dopamine in the brain are just right: not too high, not too low.

If these levels are low, our performance will be poor. If they're high, our performance suffers because we're extremely stressed. But if the levels are in the "sweet spot," neither too high nor too low, this generates a positive level of stress which helps to focus our attention.

The good news is that we have some influence over our level of arousal. If you notice that you're not sufficiently focused, you could try imagining what will happen if you miss a deadline, as this can boost your norepinephrine levels. And you could think about the rewards of doing really great work, as this can increase your dopamine levels.

However, we sometimes need to _lower_ our arousal, as too much can lead to debilitating stress. This requires reducing both the amount and the speed of the information running through our minds. So, when you feel you might become over-aroused, try writing down your thoughts to free up your mind, or take a relaxing walk as this tends to generate activity in other parts of the brain.

### 5. Insights make it possible to overcome mental blocks that limit you to the same small set of solutions. 

Have you ever had to deal with a challenging problem that demanded a creative solution, but were simply unable to get past a certain point? It's very common for people to "hit a wall" in their thinking: to reach, in other words, a mental _impasse_.

Often, the only way to break through this wall is through _insight_ — a perfect solution that seems to suddenly appear out of nowhere, and which recombines what we already know in a brand new way.

And the findings of studies support this view: we do not arrive at insights by logical reasoning, but rather we simply, and suddenly, find ourselves on the other side of the impasse.

How we solve certain word puzzles demonstrates this process. Say you're presented with a series of words, and asked to come up with one word that connects the others — for instance, "pine," "crab" and "sauce."

If you managed to find the solution to this puzzle — the correct word is "apple" — it's likely that you didn't get there by a logical progression, but rather that the correct answer appeared to you suddenly. This is how most people arrive at the solution.

The reason for this is that insight depends on the unconscious mind, which frees us from the established logical thought processes that can obscure other possible solutions. Thus, an insight is more likely to arrive when you're not using your conscious mind to solve a problem.

Fortunately, there are ways you can increase the chances of arriving at an insight, and thus breaking through an impasse. You could take a break from consciously thinking about the problem. For instance, you could stretch your legs or meditate and allow your mind to wander, which enables your unconscious to process the problem. Or you could voice the problem out loud, as hearing yourself speak about it often helps to observe your own thinking more clearly and objectively.

### 6. Mindfulness can help you focus by actually changing the structure of your brain. 

Improving the way your mind works, and thus increasing your ability to focus, first requires that you're able to observe your own thinking. This process is known as _mindfulness_, a concept that also refers to living in the present, having a real-time awareness of our experiences, and an acceptance of what _is_.

Although most people are able to be mindful to some extent, those who practice mindfulness and become adept at it have far more control over what they pay attention to.

For instance, if you take a day off from a stressful job to spend a day at the beach with a good friend, mindfulness will help you to notice whenever your focus slips from the wonderful weather and beautiful scenery onto the pile of work you have to slog through the next day. Once you observe this tendency in your thinking, you're then able to shift your attention back to the present moment and the great day you're having.

Furthermore, by practicing mindfulness regularly, you can actually alter the structure of your brain.

Among the many ways to practice mindfulness is periodically focusing your attention on physical sensations and incoming stimuli — perhaps the feeling of the chair you're sitting on — just for a ten seconds or so. This is more difficult than it sounds: you'll probably notice how your attention keeps slipping away from the present moment, and your mind starts turning over the same old thoughts. However, the more you practice, the better you'll get at noticing your focus shifting from real-time awareness of your experience to the mind's distracting chatter and interpretation.

Moreover, practicing mindfulness will increase the strength of those areas of your brain that handle mental control and attention switching. Like everyone else, you have the ability to make these changes, and you can do it whenever and wherever you like — while you're walking through a park, talking to a friend, or eating a meal. You just have to take the time to focus on your sensory experiences.

### 7. Feelings of certainty and control are very rewarding to the brain, and you can activate these feelings yourself. 

We've all experienced uncertainty and lack of control, and the deep emotional responses they trigger. Perhaps this happens while you're waiting to hear about that job you applied for, or when you're buried under a pile of work so high that you can't possibly manage it.

But why do we respond this way to uncertainty and the sense that we're not in control?

The answer lies in the brain, which constantly craves certainty and autonomy. As brain scans reveal, when our need for certainty and autonomy is satisfied, we experience a strong sense of reward.

It's no surprise then that entire industries are dedicated to dispelling uncertainty — like financial consulting, which provides executives with a sense of certainty by means of strategic planning and financial forecasting.

In fact, scientists have shown that we feel less stressed when faced with a stress-inducing task over which we have control and autonomy than when we have to deal with a similarly stressful task _without_ a sense of control.

The strong emotional response we experience when we lack control and autonomy actually stems from our appraisal of the situation. Fortunately, we can change our appraisals and, by doing so, transform our emotional responses .

This process of choosing to interpret a situation differently is called "reappraisal," and there are many benefits to it. In fact, one study revealed that people who reappraise more experienced a higher satisfaction with life than those who choose instead to inhibit their emotions.

One kind of reappraisal is humor, as it involves a shift in perspective that can transform one's experience of a situation from gravely serious to funny and lighthearted. For example, one CEO adopted the strategy of telling jokes whenever a meeting became tense, because he'd discovered that laughing at a difficult situation made it easier to find solutions.

So, by reappraising you can better manage the strong emotions generated by the lack of certainty and autonomy.

### 8. Regulating your expectations is key to a general feeling of happiness. 

Have you ever become so excited over a certain prospect — say, when you truly believe you were just about to win a game of Texas Hold 'Em — that you felt terrible when it didn't transpire?

Why do unmet expectations make us feel so bad?

In a word: dopamine.

If what transpires exceeds our expectations, our brain's dopamine levels increase, generating our interest and desire. For example, unexpected rewards, like receiving a surprise small bonus at work, have a far more positive effect on our brain chemistry than expected rewards, like receiving exactly the pay raise you had expected.

On the other hand, expecting a reward, like a pay raise, but not getting it causes our dopamine levels to plunge, and this results in us feeling something very similar to pain.

It follows, then, that to maintain a positive mental state we have to manage and regulate our expectations.

The most effective way to do this is to get into the habit of paying attention to your expectations. This will allow you to consciously adjust and lower them, so that you enable your expectations to be exceeded.

For example, imagine that you want to get upgraded to first-class on a long, international flight. If you keep your expectations low, you'll likely feel fine if you don't happen to get the upgrade and have to fly coach. But if you _are_ upgraded, you'll feel delighted.

On the other hand, if you have high expectations, and let yourself get excited about the possibility of travelling first-class, you'll be extremely disappointed if you aren't upgraded. Moreover, if you are upgraded just as you expected, you'll feel only slightly happy.

### 9. A feeling of relatedness to others and a sense of being treated fairly are primary rewards for the brain. 

We often don't think our feelings of relatedness to others, and of fairness, are crucial to our physical survival, but, actually, they're as important as food and water.

This is because, for the brain, feeling socially connected and able to relate to others is a primary reward.

In fact, as studies show, when the brain responds to our social needs it activates the same neural networks that are used for our basic survival. This indicates that our sense of relatedness to others in our society is crucial to our minute-by-minute existence. In short, for our brains, friends are as necessary to our wellbeing as food.

Why is this?

When we feel connected to others, able to relate our own thoughts and emotions with theirs, our brains release oxytocin, a neurochemical that produces a sense of pleasure. Dancing with other people, playing music together, or even just having a good conversation releases oxytocin.

Indeed, studies have revealed that people are less responsive to stress if they have plenty of rewarding friendships. This lack of stress frees up the brain's resources to deal with planning and thinking, and even reduces the chances of heart disease or stroke.

In particular, a sense of fairness is very important to our wellbeing, as it can be even more rewarding to the brain than money. For example, one experiment called "The Ultimatum Game" showed that people will turn down offers of money if they perceive them as unfair — even when that means they walk away with nothing. This suggests that defending what we believe to be fair is so important to us that we'll even sacrifice financial gain.

What's behind such a strong response to fairness? One possibility is that our sense of fairness emerged out of our need trade efficiently. Those who had the ability to detect fairness had an evolutionary advantage: they could identify cheaters, whose behavior could compromise the type of trading that was necessary to the survival of hunter-gatherers.

### 10. We are wired to feel rewarded for increases in our status, and can trick our brains into status rewards. 

If you've ever gotten a kick out of winning an argument, spent money on clothes from a designer fashion store, or even felt good about meeting someone worse off than yourself, you know that feeling an increase in status can trigger a sense of reward.

This is because when we perceive an increase in our status, we get a positive emotional response: our dopamine and serotonin levels (chemicals essential to happiness) go up, and our cortisol levels (an indicator of stress) go down.

But a sense of our increased status doesn't just make us feel happier; it also improves our ability to think. That's because the high levels of dopamine and serotonin increase the amount of neural connections we can make per hour, which means it takes less effort to process more information.

Clearly, having a sense of high status offers a great benefit to the brain. So how can you increase your status?

By finding a niche, _any_ niche, where you can feel superior to other people — for instance, funnier, richer or more intelligent.

Yet, you can even trick your brain into giving you a status reward by trying to be superior _to_ _yourself_. This is possible because our self-perception uses the same neural circuitry that the brain uses when perceiving others.

The benefit of this approach is that you can increase your status without offending or upsetting anyone else in the process. One way to do this is to work at improving your own skills — for instance, your golf handicap — as this provides you with the opportunity to feel a potentially ever-increasing status, without being a threat to anyone else.

So, a sense of your status increasing, even by a small amount, activates the brain's reward circuitry, and you can trigger the same kind of reward by playing against and trying to beat _yourself_.

### 11. Feedback rarely creates positive change in others; instead, try helping them arrive at their own insights. 

Have you ever tried to change another person's thinking? We often believe the best approach is to give that person lots of feedback, but giving feedback actually doesn't help improve performance. Instead, real change can occur only when the other person sees his errors _for_ _himself_.

The reason that giving feedback is so ineffective is that is tends to make people extremely anxious. In fact, as one study revealed, when people hit an impasse in their thinking, telling them what to do so they can overcome the block actually helps them only 8 percent of the time, while telling them what not to do helps just 5 percent of the time.

Instead, a better approach to facilitate change in people is to try to guide them towards their own insights regarding the problem.

To do this, you need to help people to get into a conducive, reflective frame of mind. This can be achieved by reducing any anxiety and increasing any positive feelings and sense of autonomy they may have.

For instance, if you want to help a coworker with a particular problem, rather than ask them "Why did this go wrong? What was the problem?" you could reframe the question in a more positive way — like, "I'm sure you did your best with this. Let's just work together and see if we can find a way to rescue the situation."

By doing this, you raise the other person's status because you're implying that they are capable of good ideas that are worth exploring.

Another way to help others arrive at their own insights is to reward them for giving feedback to _themselves_. Consider, for example, the neuroscientist Matthew Lieberman's technique: the grades he gives the work of his students are based on the skill with which they integrate their own prior criticisms into their writing. This approach actually allows the students to improve, continually.

### 12. Final Summary 

The key message in this book:

**It is a common misconception that, if you aren't delivering your best work, you should simply try harder. Often, however, the brain just needs more fuel and rewards to keep going. The reason for this is that your brain gets tired and distracted easily, and is strongly rewarded by status, certainty and control. So, for your brain to work optimally, it helps to eliminate day-to-day distractions, find out ways to have more choice and autonomy in your life, and, most importantly, train your ability to reflect on your thinking.**

Actionable advice:

**Identify and eliminate distractions.**

Recognizing and then dismissing everyday distractions is important if you want to stay focused. So, next time you have to do work that requires active thinking, remember that your performance will benefit greatly with your mobile phone switched off and a closed email program. This way your brain won't have to use its limited energy on thinking about the missed calls and all the accumulating unread mails. What's more, if people learn that you only answer your mails during a limited time window, chances are they will send you only the most important mails.

**Lower your expectations.**

A good way to maintain a positive frame of mind is to actually lower your expectations just a tad. Next time you catch yourself getting overly thrilled by the thought of getting a promotion or any other kind of reward, try to push aside the urge to get too excited. This way it will be a pleasant surprise if you actually are rewarded, and won't be as painful if you aren't as lucky.
---

### David Rock

David Rock is an author and business consultant. Rock works to connect neuroscientists with leadership experts to establish a science for leadership development (he coined the term "NeuroLeadership"). He is also the author of _Quiet Leadership: Six Steps to Transforming Performance at Work._

