---
id: 5360da0e6233360007170100
slug: your-first-1000-copies-en
published_date: 2014-04-29T11:18:41.000+00:00
author: Tim Grahl
title: Your First 1000 Copies
subtitle: The Step-by-Step Guide to Marketing Your Book
main_color: BCAD25
text_color: 8A7F1B
---

# Your First 1000 Copies

_The Step-by-Step Guide to Marketing Your Book_

**Tim Grahl**

In _Your First 1000 Copies_, Tim Grahl outlines a step-by-step guide to his connection strategy for authors. He reveals how you can create a platform to find your audience and keep them in the loop so they'll buy your next book. Packed with vital information on how to keep readers interested in your work with good online content and how to sell without being sleazy, _Your First 1000 Copies_ is a must read for any aspiring or established author.

---
### 1. What’s in it for me? Find out how to get loyal readers who’ll pay for your book. 

It's no secret that the internet has changed book publishing forever. Increasingly, both novice and established authors have to learn to adapt to this new environment. And while the sheer range of options for marketing one's work may seem overwhelming at first, _Your_ _First_ _1000_ _Copies_ aims to help you focus on the essential, most useful tools.

To that end, author Tim Grahl presents a system for attracting a large audience of readers before you've published a single word. Imagine having a readership in place as you write your book: an audience you can talk to directly, sharing free and interesting content and materials. By the time it comes to actually publishing your book, you'll have an established following of people who have gotten to know you, who trust you, and who want to give something back in return for all the free content you've provided.

Of course, there will be many traps and obstacles to be avoided along the way. And self-marketing is tough work. But _Your_ _First_ _1000_ _Copies_ will be essential company during those times. 

In these blinks, you'll find out how to sell without seeming like a used car salesman. 

You'll learn how to keep your readers' attention with free, valuable content that doesn't take up all your time and energy.

You'll also discover how to network using existing platforms and influencers to reach a wider audience.

You'll find out how to determine which online marketing strategies work for you and which don't. 

Finally, you'll learn a variety of strategies for marketing and selling books that can give you insight into pitching and generating sales for almost any content or product online.

### 2. The key to marketing is not flashy gimmicks, but helping people for free. 

Imagine you're in the market for a new car: the first thing you do is make your way to a local dealer. An eager salesman greets you and immediately begins to show you all the latest models.

After struggling to process the onslaught of advice he gives you, you decide to take a moment to look around the showroom alone. This is when you notice that last year's models have all of the same features as the new models — for $10,000 less!

While the salesman's pitch might've been smart, he missed the most crucial aspect of selling: looking out for the customer's needs.

Despite popular belief, good marketing isn't about tricking or coercing people into buying a product through gimmicks or intrusive self-promotion. Rather, the best marketing strategy is to focus on your customers' needs and desires.

Take the marketing guru Seth Godin: he markets his books through his blog, where he writes daily blog posts offering valuable advice to his readers, even responding personally to his readers' comments and emails.

In addition, the best way to look out for the customer's needs is to provide supplementary material for free. And there are many ways to do that: write a blog, or create a podcast, audio book or informative PDF document.

Author Chris Guillebeau, for example, generated interest and excitement for his best-selling book _The_ _Art_ _of_ _Non-Conformity_ by providing a free online manifesto of his central ideas and theories. The manifesto, called _A_ _Brief_ _Guide_ _to_ _World_ _Domination_, was downloaded 100,000 times and was a crucial factor in the ultimate success of his published books.

### 3. Create a marketing blueprint and then decide what the best tools are to implement it. 

Now that you've learned the key to successful marketing, it's time to get more specific and look at how marketing can be used to sell books.

The internet has transformed and expanded the ways books can be marketed effectively. While the advantages it provides are undeniable, the sheer number of marketing options is intimidating to even the most established author. So what are the first steps you should take in selling your book online, and how do you go about it?

First, you need to come up with a _marketing_ _blueprint_.

Imagine, for instance, that you were instructed to build a block of apartments. Would it be sufficient for the company to provide you with all the building materials and top-of-the-line equipment you could ever need?

Of course not. You'd also needs the plans, or blueprints, for the building, since you have to know what you're building before you begin the process.

The same is true of marketing a book online. While the internet provides you with a plethora of marketing tools — social media, blogs, podcasts and so on — you will need a blueprint if you want to use these tools effectively.

So, how do you create your marketing blueprint?

First, you must determine what the aim of your marketing is. In terms of marketing books online, it should be to establish a long-term relationship with your readers.

Second, once you know your aim, you must build your blueprint, or plan, around it. Authors' blueprint should be to locate those who are or might be interested in them and their books, and then to maintain that interest during the (often very long) time period between books. It's best to base your choice and selection of marketing tools on this blueprint.

### 4. Starting an email list is the first step in building a long-term engagement with readers. 

What do you do when you're bombarded with marketing you have no interest in? Do you buy the product or completely ignore it?

Most likely, you ignore it. In order to get potential customers on your side with your marketing, you have to make sure that you have their _consent_.

How?

The best method is to ask your fans to join an email list. By agreeing to join, they give permission to market to them, and thus are more likely to read whatever you send them. Once you have their permission, you can provide them with regular, up-to-date information about your work — at zero cost to you.

Moreover, if you get enough fans on your email list, the results can be amazing. For example, author Ramit Sethi released his first book — _I_ _Will_ _Teach_ _You_ _To_ _Be_ _Rich_ — without a publicist or any media exposure. However, because of a particularly strong email list, the book made it onto the _Wall_ _Street_ _Journal_ 's best-seller list. Today, Sethi continues to market his books in the same way, earning over one million dollars per year.

But isn't social media, like Facebook and Twitter, supposed to be key to online marketing?

Actually, despite what many people think, the email list is a much stronger marketing tool.

In contrast to social media, where users are overwhelmed by a constant stream of rarely useful information on their Facebook and Twitter feeds, which thus serves merely as a distraction, people will probably read every email they receive, as they could possibly contain important information.

And this translates directly to sales. For example, Pam Slim, the award-winning writer of _Escape_ _From_ _Cubicle_ _Nation_, discovered that, for every sale she made promoting her book via social media, she made fifty from her email list.

### 5. Give readers an incentive to join your email list – and don’t give them reasons to leave. 

So you've set up your email list and are ready to fill it with potential customers. Now you have to get people to join.

How?

First, you should provide people with an incentive. By providing a weekly newsletter brimming with useful content and advice, for example, you give customers a reason to subscribe.

It's also important to ensure that any benefits to signing up to your newsletter are very clear to potential customers — for example, "Sign up to this newsletter and see what it can do for your wallet!"

Additionally, you have to ensure that anyone potentially interested in joining your list will actually _notice_ your offer. One way to do this is to present a sign-up message in multiple online locations: for example, include a sign-up form on your homepage and any time you make a blog or social media post, whether it's on Facebook, your own blog, or other people's blogs, etc.

Finally, once you manage to get people to join your list, you have to give them reasons to stay subscribed.

This means never sending them content that could be construed as frivolous or pointless — like ads for other products or your holiday snapshots — otherwise your subscribers might consider your emails spam and unsubscribe.

It also means making sure that the content of your emails and newsletters is interesting and compelling. While this is in no way an easy task, there is a surefire way to test how compelling your content is: get someone drunk and then ask them to check it out.

While this might sound like an unusual approach, drunkenness — as an experiment by blogger Xianhang Zhang demonstrated — mimics people's distracted behavior when they're online, therefore providing the ideal testing conditions for the effectiveness of your content.

### 6. Don’t be scared of oversharing content: just make sure it’s relevant in the long term. 

Now that you've connected with fans and potential customers via your email list, blog and so on, you might be thinking that there's a limit to what and how much content you should share with them.

But that's impossible: there's no such thing as oversharing. Fans of your work will appreciate almost any sort of content you offer them.

One type of content that will be particularly well-received by your fans is that which you create for yourself as you research and write your book. During the writing process, you'll probably generate many notes, plans, drafts and summaries. Your fans will love this type of content and probably even share it in the fan community, helping you generate buzz around your book when it's finally ready. And the great thing is that you won't need to take time out of your schedule to create it!

You should also make sure that, whatever content you decide to share, it's something that stays relevant over the long term. As increasing numbers of people join your email list, you'll benefit from being able to re-share content you've posted already.

For instance, latecomers to your list should be introduced gently to your content, rather than being thrown in at the deep end with the latest email.

An effective and easy way to do this is set up a system whereby new subscribers are automatically sent emails over the first couple of weeks containing a selection of your best blog posts. This will give the newcomers a sense of what the list offers and serve as useful context for what's to come.

For this reason, you should make sure that any content you post will remain relevant in the future. Don't share things that will make sense only on a particular day or week. Content that focuses on the very latest news or gossip simply won't be of interest down the line.

### 7. To reach the widest audience, connect with the movers and shakers. 

The email list and other online marketing tools we've looked at so far are all very useful in reaching your audience. However, it's equally important to reach out to people who are in a position to influence their own audience to read your books.

These "movers and shakers" are called _influencers_, or anyone who has an online audience of their own who'd be potentially interested in your work. Influencers include other writers and bloggers you're friendly with, publicists, an agent booking you for an interview — or even a trusted reviewer on Goodreads.com.

But how do you get influencers to spread the word about your book? Unlike your fans, who will appreciate receiving the content you share, influencers are usually busy with either writing their own books or creating and sharing other kinds of content. Therefore, the best way to get influencers to help out is to make it easy for them by sending them content that's ready to be shared with their online audience.

Before that can happen, you have to catch the attention of influencers. And to do that, you need to actually get out into the world.

Even in a world that's heavily mediated online, marketing doesn't only involve using the internet (e.g., social media) to draw attention to yourself and your product. Particularly when you need to find influencers, it remains incredibly important to meet people in person so you can establish an instant connection with them.

Where can you find such people? Many influencers tend to gather at live events and conferences, so this should be your first port of call.

For example, the author used the SXSW Festival to connect with agents and publishers, and by meeting them face to face he established several good relationships with key influencers.

### 8. When the time comes to sell your book, don’t be afraid to make a pitch. 

As you've been writing your book, you've also been connecting with your readership by keeping them updated with great content. So now that your book is ready, it's time to request that your fans actually buy your book.

And what's the best way to do so?

Before you actually release your book into the world, try to generate excitement and anticipation by issuing teasers prior to the official release date. One way to do this is by sharing content that didn't make it into the final work. You can tease people by telling them that, if they like what they see, they'll be thrilled with what _did_ make it into the book.

Then, once the book is truly ready to be sold, you can generate readers' interest by enticing them with a free sample. This is a very effective way to get people interested. A perfect way to make this snippet available is as a free, downloadable PDF file.

You could even follow Amazon's lead, and offer the first 10 percent of your book as a free sample. If people like what they see in that small sample, then they'll buy the whole book.

Also, when you do make your pitch to sell the book, whether you do this on your email list, blog or on an influencer's website, you should include supplementary material — like short reviews of the book from well-regarded reviewers, photographs and a short bio. This will let people know more about you as a writer and a person, which will give them a reason to buy your book.

Finally, you should repeat your offer many times so that people get the message. People tend to act only after receiving a message on multiple occasions. Therefore, you should make your offer repeatedly on multiple platforms — your blog, email list, social media pages, etc.

### 9. Examine your analytics to find out where you can tweak your marketing strategy. 

How can you tell whether your marketing strategy is working?

Aside from actual book sales, there are a few other indicators that show that you're reaching people. And, in the internet age, these indicators are easily accessible by way of analytical software.

One crucial thing to track is the effectiveness of your emails. Analytical software can reveal both the _open_ _rates_ (how many people open your emails) and _click_ _rates_ (how many open the links contained in those emails).

You should aim to get at least 25 percent of recipients to open the emails, and 50 percent of those people to actually click the links provided. If your figures are below these recommended ones, you should consider whether the content of your emails is interesting, relevant or useful enough.

By being fully aware of the success of your email campaign, you're in a prime position to ensure that it is effective and up to date.

Analytical software also allows you to measure how people use your website. For instance, you can track the number of people who visit the site. This is an important bit of data as it reveals the number of people interested in you, and the growth or decline of that number over time.

Moreover, you can get even more specific: you can find out which of the pages on your website are most frequently visited. This information is particularly useful as it indicates which content is more popular on your site.

Finally, you can check the analytics for your social media campaign by tracking how many people are _viewing_ and _sharing_ your posts and, in doing so, you can learn what kind of information your followers find interesting and valuable.

### 10. Final Summary 

The key message in this book:

**To connect with your readers, authors must create a direct, long-term connection with them. The best way to do this is via an email list that provides readers with consistently great content that's interesting and valuable to their lives. By doing this, and continuing to reach more readers via other platforms and influencers, when the time comes to actually sell your book, you can confidently ask your readers to purchase it, knowing that you're providing them with an invaluable service.**

Actionable advice:

**Show your work.**

Whenever you're working on a book, you'll generate a lot of material that won't end up in the final published version. This might include research, sketches, plans and chapters that you know will be eventually cut. All of this material will be of interest to your readers, and it'll help sustain their interest in your upcoming book as they wait for its final release.

**Don't be afraid to ask your fans to buy your book.**

All the work you've put into attracting readers to your project has to pay off at some point, so don't be shy about requesting that they purchase your book when it's finally available. You'll have been providing your readers with consistently interesting and compelling material for a while without exploiting your relationship with them (e.g., by spamming them with advertisements), so most of them will be happy to pay money for your book.
---

### Tim Grahl

Tim Grahl is an author and consultant. Aside from penning his own best sellers, he has worked with best-selling authors Daniel Pink, Chip and Dan Heath, Dan Ariely and Pamela Slim. Grahl is the president of Out:think, a company that helps authors develop marketing strategies, stay connected with readers and, crucially, _sell books_.

