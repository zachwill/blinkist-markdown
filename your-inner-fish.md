---
id: 53b3bdd46337320007070000
slug: your-inner-fish-en
published_date: 2014-07-01T00:00:00.000+00:00
author: Neil Shubin
title: Your Inner Fish
subtitle: A Journey into the 3.5-Billion-Year History of the Human Body
main_color: B0F9D9
text_color: 33A674
---

# Your Inner Fish

_A Journey into the 3.5-Billion-Year History of the Human Body_

**Neil Shubin**

Drawing on findings from paleontology, genetics and developmental biology, _Your_ _Inner_ _Fish_ describes the evolutionary history of the human body, tracing it back to the fish. The author shows how studying fossils, genes and embryonic development can help us understand our complex evolutionary past.

---
### 1. What’s in it for me? Find out how fish can tell us a whole lot about ourselves. 

Researchers are endlessly at work exploring the history of our planet and its many species, both living and extinct, to gain new insights into our past.

In 2004, the author and his team discovered the _Tiktaalik_ fossil, representing the bridge between life in water and life on land: a milestone on our path towards understanding the evolutionary development of humans. If animals had never emerged from the water, we humans wouldn't exist.

But it was a long evolutionary journey before today's human was fully developed, and now we're trying our best to understand this gradual transformation.

In these blinks, you'll find out:

  * what humans have in common with fish,

  * how hiccups are due to our fishy past,

  * the price we pay for our ability to talk and

  * why a fish doing push-ups might not be as strange as it sounds.

### 2. Layer by layer: fossils teach us about the landscape, climate and species of former times. 

Fossil fuels are very important for meeting the high energy demand in our modern society. But fossils have another significance: they help us understand our planet's past and the creatures that lived a long time ago.

After all, sea shells have been found on Mount Everest — evidence that an ocean once existed there — and fossils from warm-adapted animals have been found in the icy Arctic, proof that the region once had a tropical climate. Moreover, many well-preserved fossils have been found in hot, arid, sparsely populated deserts like the Sahara, indicating that they were once full of water and life.

And so, layers of rocks and the fossils they contain can expose the evolutionary development of different species and animal groups because the order of the layers always reflects their age.

Let's take the tetrapods, an animal group comprising amphibians, reptiles, birds and mammals, all of which are characterized by having four limbs, a backbone and lungs.

Scientists found fossils of the first land-living tetrapods in rocks dating back no more than 365 million years, meaning that this group of animals appeared 365 million years ago. But they had no evidence of how the tetrapods developed, since there was a gap in the fossil record: fish fossils had been found in rocks that were older than 380 million years, but nothing else had been found in between these two animal groups.

That is, not until the author's workgroup discovered the _Tiktaalik_ fossil in a 375-million-year-old rock. _Tiktaalik_ had body features similar to a fish, such as scales, fins and webbing, but also a flat head with two eyes on top and a neck — the characteristics of a land-living tetrapod. In other words, it had traits from both groups of animals, thus representing a link between life in water and life on land.

> _"Tiktaalik is a wonderful intermediate between fish and their land-living descendants, but the odds of it being our exact ancestor are very remote. It is more like a cousin of our ancestor."_

### 3. It’s in our bones: similar bone designs can reveal how different species are related. 

Can you imagine a fish doing push-ups, with its gills flapping and fins flopping? A pretty absurd image, it's true, but the disposal of bones shows that _Tiktaalik_ was actually able to do push-ups. The creature could place its fins flat on the ground, bend its elbows and use its chest muscles just like humans.

Some of our joints can be traced all the way back to animals like _Tiktaalik_, part-fish, part-tetrapod. Even though _Tiktaalik_ looked like a fish with the head of a crocodile and lived in the water, it shared some body features with the human species.

For example, the elbow is a joint formed by the upper arm bone and a bone in the lower arm. And, as we all know, fish don't have elbows. But _Tiktaalik_ actually did have a simple elbow that allowed it to move its lower arm. It even had a primitive wrist as well. So although its fins had fin webbings, they also had clear similarities to human arms.

Naturally, the elbow and wrist and their movements have been refined in the human body. But the fact that we have the same kind of joints as the Tiktaalik shows us that we are somewhat related to a 375-million-year-old species.

That's because, from amphibians to birds and mammals, vertebrates who've settled on land all have a similar bone design. For example, all species in this group have two lower leg bones connected to a strong upper leg bone and an ankle. The ankle is connected to several bones ending in toes. This general bone design of tetrapods enables them to walk on land — a feat impossible with fins.

In conclusion, different species can have similar bone designs, and we'll now see how their bones have changed to adapt to different lifestyles.

> _"All of our extraordinary capabilities arose from basic components that evolved in ancient fish and other creatures."_

### 4. Bones and teeth show us how variations of the same body parts fit different types of lifestyles. 

Different species face different challenges and have thus needed to adapt to their environments. Scientists can track these adaptations with the help of bones and teeth.

Different kinds of limbs help tetrapods manage their lifestyles. To ensure their survival, tetrapods colonized different parts of the world and developed different abilities — e.g., birds gained the ability to fly and whales returned to the oceans.

For whales to be able to colonize the water again, their shortened yet strong upper-arm bone and elongated fingers developed into fins, which enabled them to swim efficiently. Then there were the frogs, whose elongated and partially fused leg bones made it possible for them to jump and thus survive. In contrast, horses can't jump as high relative to their body size, but they are excellent runners thanks to the structure of their toes: over the course of evolution, the central toes elongated and the shorter outer ones disappeared.

Teeth can also tell us about the eating habits of different species, giving us insight into their lifestyles.

Humans are omnivores, which means that we eat plants as well as meat, and for this we have different types of teeth. We have incisors to cut food, molars to chew food, and premolars to both cut and chew food. We're thus able to chew precisely and efficiently because our upper and lower teeth fit perfectly together.

The carnivorous crocodile, by contrast, needs to hunt and instantly kill its prey, so it has bigger teeth and the ability to replace those teeth during its lifetime. Crocodiles need this capability because they'd starve to death if they lost their teeth. Unlike humans, they have an imprecise occlusion because they don't need to chew but can just gobble down their food instead.

### 5. Some of our genes can also be found in other species, revealing an evolutionary relationship. 

We know that the more similar the DNA of different species, the stronger their relationship. And tetrapods all have a shared gene that initiates the development of limbs.

In one experiment, scientists took a chicken embryo early in its development and transplanted a small bit of tissue from the pinky side of what would become a limb bud to the thumb side. The result was surprising: the chicken developed an extra, mirrored set of fingers on the thumb side. How could this have happened?

Scientists found out that the transplanted patch was a special, genetically active tissue that regulates the development of limbs with the help of a specific protein _._ They identified the protein and the related gene and called it _Sonic_ _hedgehog._ This gene initiates the synthesis of the body's own protein, which in turn triggers limb development. But _Sonic_ _hedgehog_ isn't only active in chickens — it exists in all animals with limbs, including humans.

Scientists then wanted to know whether fish, as limbless vertebrates, also had this gene. Ultimately, they were able to find it in sharks and skates. In a subsequent experiment, they transferred the S _onic_ _hedgehog_ protein from a mouse to the fin tissue of a skate. Would the otherwise limbless skate develop a forepaw?

Indeed, the transferred protein did trigger the development of extra, mirrored fins. The ability of the mouse protein to induce this development shows that the genetic basis of appendages must be similar, since only similar genes can initiate the synthesis of similar proteins.

We can thus deduce that the genes for the development of limbs evolved from fin genes because fish species appeared on earth before tetrapods.

### 6. Bodies started to develop when multicellular organisms had enough oxygen. 

Our earth is full of creatures with extremely diverse appearances today. But life on earth looked quite different a billion years ago: our planet was home to exclusively unicellular organisms like bacteria. So why and how did such different-looking bodies develop?

Simply put, because there are benefits to having a body. In a world full of unicellular organisms, multicellular organisms and bodies can easily find prey and avoid becoming prey: that's a big selective advantage. Bodies can also move faster and travel for longer distances.

However, the more cells an organism has, the more oxygen it needs to survive. So when the oxygen content of the planet increased a billion years ago and remained constant, it marked the beginning of multicellular organisms: they finally had enough oxygen to cover their requirements.

Then, in order for bodies to develop, their cells need to be able to physically connect and communicate with each other. Think about it: unicellular organisms only need to manage one cell, whereas bodies need to control up to trillions of cells.

Body cells have molecular rivets that only bind to similarly structured rivets on the same type of cells. That's why skin cells stick to skin cells and form tissue, whereas hair cells attach to each other. Also, body cells communicate by sending signals to each other, which starts a chain reaction in which the signal is shared with further cells. This ensures that cells divide and die when it is necessary — otherwise they could cause deformities or death. So at the point when cells had enough environmental oxygen to connect and communicate, bodies could start developing.

### 7. A body has a special plan to put everything in its right place. 

At the beginning of embryonic development, animals are nothing but a mere cluster of cells, and the cells need to organize themselves so that all the organs develop in the right position.

In fact, all vertebrate organs grow from three layers of tissue called _germ_ _layers_ during embryonic development.

Two weeks after fertilization, vertebrates develop their germ layers. Vertebrates have three of these, which serve as the basis for all organs and body structures. In every vertebrate — from fish to humans — these three layers are necessary for the same organs. The outer layer is responsible for the development of the skin and nervous system, the middle layer forms muscles and the skeleton, and the inner layer triggers the development of several inner organs.

In one experiment, scientists transplanted a small patch of tissue with all three germ layers into a salamander embryo and the salamander developed a twin. So this little tissue includes all the information needed to develop the body.

Then there are the _Hox_ _genes_, found in every animal with a body, that determine our body plan. All animals with bodies have variations of Hox genes responsible for our proportions, meaning that they determine the size and shape of our body regions, like our head and back. In addition, Hox genes determine the front-to-back organization and the body axis.

If one of these Hox genes is missing or mutated, it has far-reaching consequences for the body plan of the animal.

For example, when a variation of the Hox gene is injected into the frog embryo at the right point, the animal ends up developing two body axes and consequently two heads. Or when Hox genes are manipulated in the middle segment of a fly and it causes the midsection to either be stunted or lost. Scientists have also discovered that if a mouse loses one of the Hox genes for its thorax, its back will be deformed.

### 8. The embryonic development of our head shows us our inner fish. 

Our head is a very complex and powerful body part, which makes it hard to believe it was once nothing but a glob. But out of that glob, the arches develop: four structures that look like swellings and initiate the development of the head.

Here's what each of them do: the first arch is responsible for the development of the two ear bones and the jaws; the second forms the third ear bone and muscles needed for facial expressions; the third and fourth are in charge of the development of the throat, which allows us to swallow and talk. Each of the arches also forms the appropriate nerves and muscles.

Should these arches develop incorrectly, however, they can cause birth defects. For instance, a child without the first arch won't be able to hear because she'll have stunted jaws and lack two of the ear bones.

Interestingly, we can find these arches in fish, too.

There are some differences between the development of arches of humans and sharks in adaption to their different lifestyles, but the blueprint is nearly the same. When you compare the head region of a developing shark and a human embryo, they look extremely similar at the beginning. Even in adult sharks and humans, you'll find very similar head structures.

For example, the first arch in sharks also forms the jaws but not ear bones. Their second arch is responsible for forming additional, supporting jaw bones to move the upper jaws. This is impossible for us, but we have instead developed more complex ears. The shark's third and fourth arches develop tissue that controls its gills, whereas ours allow us to talk.

Aside from these differences, sharks have nearly all the same nerves as us. These shared features provide yet more evidence that we're related to fish.

### 9. Our ears evolved from bones and organs that have nothing to do with hearing. 

Mammals have many defining body features in common, such as milk-producing glands, but there are also less obvious parallels, such as the three middle ear bones.

Actually, those three middle ear bones evolved from jaws.

Scientists used fossils to help them search for the origin of the three mammalian ear bones. They found that reptiles have only one middle ear bone that's very similar to ours, and that the origin of this ear bone can be traced to fish jaws.

Fossils also reveal the origin of the two other mammalian middle ear bones. As it turns out, bones from the back of the reptilian jaw grew smaller over time, shifting from the jaw to the ear during the evolutionary development until they formed the two other ear bones in mammals.

Parts of our inner ear's origin aren't linked to hearing, either, but to the sensory organs of fish.

Our inner ear is not only used to hear but is also responsible for our sense of balance. The inner ear consists of bony tubes and sacs that are filled with moveable gel. So when you tilt your head, the gel moves and the sensory hair cells bend, sending a signal via the nerve to the brain. That's how we sense an altered position of our head.

Fish also have a system of sensory organs along their body called the lateral line. This consists of sensory receptors connected to a jelly-filled sac, which is linked to the brain through sensory hair cells and a nerve. The flow of water deforms the sac, which bends the sensory hair cells and stimulates the nerve, which sends a signal to the brain. The process strongly resembles the one that takes place in humans. That's why fish are able to sense direction and potential distortions of the water flow.

In conclusion, parts of the human's inner ear and the fish's lateral organ have similar structures and functions, illustrating their common origin.

### 10. Our sense of smell shows our relationship to other animals. 

Our sense of smell is incredibly important to us. For example, it helps us differentiate between edible and poisonous food. And this rather sophisticated sense of smell can actually be traced all the way back to fish.

Fish also have a sense of smell, though there are some differences in the structures of our organs.

When fish smell, water passes through their nostrils and arrives at their skull. There, a specialized tissue with sensory receptors that bind the odor molecules with the water sends signals to the brain via the nerves, which the brain interprets as smell.

We humans also have these structures — i.e., both nostrils, sensory receptors and nerves. So when we breathe, the air, with its invisible odor molecules, passes through us the same way that the water does in the fish — yet another example of our evolutionary relationship.

But there is one major difference regarding the smell organs of humans and fish: we've developed different smell receptors because the chemical reaction of smell is different in air than in water.

Smell receptor genes are one of the things that mammals have in common: actually, three percent of all mammalian genes are connected with the sense of smell. You can find the same genes in all mammals, but many of them have lost their functions.

For example, evolution has reshaped the nasal passages of whales into a blowhole, which they use to breathe but not smell. So even though the smell genes are present in whales, they're completely functionless because the whales don't need them the way their land-dwelling ancestors did.

Like whales, we humans also have all the smell receptor genes, which speaks to the fact that we belong to the mammal kingdom. However, 30 percent of these genes are now functionless since we don't need to smell as well as our primitive ancestors did.

### 11. Our visual system became more important and has changed for the better. 

Sight is arguably the most important of the senses for humans — so much so that around 70 percent of all human sensory cells are light-sensing cells. But, looking back at our primal ancestors, we can see that it hasn't always been that way.

Fossil records show that the flora of our planet changed approximately 55 million years ago. Before that occurred, the forests consisted mainly of green plants, like fig and palm trees. However, when the changes took place, the biodiversity of plants started to increase and forests became more colorful.

It thus became necessary for monkeys to differentiate between the new, colorful plants so they could eat the right ones. That's why monkeys developed the ability to see colors — and why we're still able to see colors today.

And we've come a long way since then! The light-sensing cells in our eyes show how our visual system has changed for the better. These cells detect and collect light and send it to the brain via chain reactions, where information is analyzed and an image is built.

The light-sensing cells have a protein called opsin, which initiates the chain reaction. We can find this opsin in the eyes of every animal, but the structure and number of opsins in different species is adapted to the lifestyle of the animal.

For instance, many animals capable of seeing colors only have two different kinds of opsins for color vision (plus one opsin to detect black and white), but we humans have three different kinds of opsins for our color vision (and one extra one to detect black and white). This shows that our visual system once adapted to a more detailed and precise color vision.

We've now seen that our evolutionary background influences our body plans from our limbs to our sensory organs. In the following blinks, we'll see some problems that our animalistic ancestors caused.

> _"When you look into eyes, forget about romance, creation, and the window into the soul. With their molecules, genes, and tissues derived from microbes, jellyfish, worms, and flies, you see an entire menagerie."_

### 12. Some problems and everyday inconveniences are related to our evolutionary background. 

Our human lifestyle is very different to that of our animal ancestors. Rather than spending all day outside hunting, the majority of us spend most of our time sitting at our desks in front of computers.

And since our bodies were built to be more active than most of us are, many people suffer from heart diseases and obesity. But there are other adaptations to our lifestyle that have caused problems for us.

Sleep apnea, or a sudden lapse of breathing during sleep, is one example of a problem that stems from our evolved ability to talk.

When our body plan needed to be adjusted to enable us to talk, we evolved flexible walls in the back of our throats, controlled by muscles. When we sleep, nearly all our muscles relax — including the throat muscles. Sometimes this leads to a collapse of the throat muscles, which stops us from breathing for a while. So in this way, this evolved ability has potentially dangerous side effects.

Hiccups, for instance, are also an inconvenience that can be understood in light of our evolutionary background.

Hiccups are caused by a spasm of the nerve that controls breathing. When the nerve convulses, it stimulates the contraction of muscles in the diaphragm and leads to sharp breathing, which can continue for some time.

And this is also related to our inner fish. In fish, the brainstem controls the muscles of the gills and the throat, which are needed for breathing. The position of the brainstem is beneficial in fish because it's close to the gill region, meaning that the nerve only has a short way to go between the brainstem and the muscles.

In mammals, the brain stem also controls breathing but is placed further away from the controlling muscles. Therefore the nerve has a lot of opportunities to be disturbed and we start to hiccup. In this way, hiccups are due to our development from our fishy ancestors.

> _"Each of these examples show that we were not designed rationally, but are products of a convoluted history."_

### 13. Final summary 

The key message in this book:

**Every** **living** **being** **has** **ancestors** **that** **determine** **their** **genetic** **blueprint** **and,** **as** **a** **result,** **their** **body** **plan.** **Our** **ancestors** **are** **fishy** **creatures** **and** **to** **this** **day** **we** **have** **much** **more** **in** **common** **with** **fish** **than** **meets** **the** **eye.** **A** **lot** **of** **our** **body** **parts** **have** **the** **same** **origin** **or** **evolved** **from** **fish** **and** **other** **animals.**
---

### Neil Shubin

Neil Shubin is a paleontologist who teaches anatomy and biology at the University of Chicago. He and his work group discovered _Tiktaalik,_ a fossil that helped clarify an evolutionary step from fish to human. Shubin also wrote _The_ _Universe_ _Within:_ _Discovering_ _the_ _Common_ _History_ _of_ _Rocks,_ _Planets_ _and_ _People_.

