---
id: 5da083616cee0700074e2c2e
slug: your-retirement-salary-en
published_date: 2019-10-15T00:00:00.000+00:00
author: Richard Dyson and Richard Evans
title: Your Retirement Salary
subtitle: How to Use Your Lifetime of Pension Savings to Pay Yourself an Income in Your Retirement
main_color: ED9A72
text_color: BA6034
---

# Your Retirement Salary

_How to Use Your Lifetime of Pension Savings to Pay Yourself an Income in Your Retirement_

**Richard Dyson and Richard Evans**

_Your Retirement Salary_ (2019) tackles a knotty question asked by savers approaching retirement age: How do you transform your savings into an income that will see you through your sunset years? Drawing on years of experience addressing readers' personal finance questions for a leading British newspaper, Richard Dyson and Richard Evans provide a wealth of insights into getting the most out of your pension pot.

---
### 1. What’s in it for me? A no-nonsense guide to pensions. 

Retirement is a hard-earned reward for a lifetime's work. Pensions, however, aren't what they once were. Today, individuals around the world fret about making ends meet once their working lives come to an end. Their biggest worry? Stretching their savings to cover increasingly long lifespans. 

Then there are the vexed questions of taxation, investment portfolios and annual withdrawals. Throw technical jargon like "annuities" and "natural yields" into the equation and it's no wonder people feel overwhelmed. 

It's time for some help. These blinks help clear a path through the minefield of retirement. Along the way, you'll find out how the British pension system works, as well as plenty of actionable insights you can apply to your own retirement, wherever you live. 

In these blinks, you'll learn

  * why retirees are increasingly shouldering the burden of planning for retirement; 

  * how to stick to a safe withdrawal rate and avoid breaking the bank; and

  * what to look out for if you're thinking about remortgaging your home.

### 2. Pensions mean different things to different people and different generations. 

What is a pension? Well, there are a couple of definitions. 

The first is simple: a pension is the money you live on once you have retired. A retiree might, for example, say something like, "I can just about get by on my weekly $300 pension."

The second is that its a sum of money or an investment portfolio earmarked for retirement. This is also called a _pension pot_. 

That's basically everything you have accumulated over the course of your working life. It can include savings from your salary, investments, and contributions from your employer. A pension pot isn't the same as an income — it's a pool of money put aside during your working life that you will need, somehow, to turn into an income when you retire. 

Older generations also have their own understanding of pensions. If you worked for a big company in the 1960s, 1970s or 1980s, you'll probably be familiar with the concept of a "gold-watch retirement." This refers to the custom of giving long-standing employees a golden watch on their last day of work to thank them for their service. 

This went hand-in-hand with a more paternalistic style of employment. When workers came to the end of their careers, they collected their watch, went home and put their feet up. A good part of what they had earned in wages in one month became their pension in the next. This was known as a _defined benefit_ or _final salary pension_. 

Say you worked for 25 years with the same company and were earning $67,000 a year when you retired. Companies "defined" your pension by multiplying a percentage of your final salary by the number of years served. Typically, that might be 2.5 percent. Multiply that by 25 and you get a pension of $41,875 a year.

This type of pension secured a comfortable retirement for millions of people in countries like the UK and the US. Some retirees in their seventies and eighties are still reaping the rewards of this generous arrangement today. It's a very different picture for their children, however, let alone their grandchildren. Why? Let's find out.

### 3. Longer lifespans and falling interest rates ended the golden age of pensions. 

The substantial pension arrangements enjoyed by the postwar generation came to an end in the 1980s. Two factors explain this development: increasing average life expectancy and falling returns on investments. 

In the past, people didn't live nearly as long as most people in the developed world do today. On average, anyone born in Britain or the United States before 1940 was lucky to make it past 70. Today, however, we assume that our parents and grandparents will live into their eighties or nineties. 

That means retirement is longer now. A 65-year-old man, for example, should plan for another 19 years of living expenses. For a woman the same age, that goes up to 21 years. Pensions, in other words, cost a lot more than they once did.

That brings us to the second factor — _falling investment returns_. Assumptions about returns are informed by past experience. People working and saving in the 1980s expected high rates; folks in the 2010s generally reckon with low rates. 

That's hardly surprising: returns on investments were higher in the twentieth century than they are in our own. Annual returns in the 1980s averaged nine percent; twenty years later, they were closer to three percent.

This slowdown coincided with rising pension costs caused by mushrooming life expectancy. Companies are bound by law to honor agreements concluded in the past, and many found themselves saddled with spiraling pension costs. The only solution was to reform their pension schemes. This is what killed the generous pensions of yesteryear. 

So what replaced them? The most common pension scheme these days is the so-called _defined contribution_ model. Companies typically pay a set amount toward your pension each month. Let's say that's four percent. Half of that is deducted from your salary; the other half is paid by the company itself. 

That money is then put into a pot and invested by a pension provider. When you retire, the total amount in your pot is your pension. Unlike the old arrangement, employers don't guarantee retirees an income — only the contribution is defined. 

This distinction is crucial because it means the burden of providing an income has been shifted from employers to employees, businesses to individuals. Put differently, _you_ must now play an active role in managing your retirement. 

In the following blinks, we'll take a look at some strategies to help you do just that.

### 4. Living off income generated by investments is an ideal way to fund your retirement, but low interest rates make that tricky. 

However large or small your pension pot, you will need to withdraw enough money from it to cover your living costs without running out of cash before you die. Let's kick things off by focusing on how to manage one part of your pot — returns on investments. 

Some investments produce income automatically. Quarterly dividends generated by a stock market fund, for example, are simply deposited in your personal pension account. The same applies to investment trusts. 

Technically, you can withdraw this cash whenever you like, but it's worth remembering that funds and trusts typically charge you for each withdrawal. For this reason, it's a good idea to withdraw money every three months. 

In general, investments have two components: _yield_ and _natural yield_. 

When you put money in a savings account, you entrust your capital to the bank. In return, the bank pays you interest. This is the yield. If you own an income property, the rent your tenant pays you is also a type of yield. Owning a house, however, can be expensive. If the roof starts leaking or the furnace breaks down, you'll need to fix them. This means that a portion of the yield must be set aside for maintenance. 

That brings us to natural yield. This is income produced by an investment as distinct from any rise in value of the investment itself. In this case, that would be your rental income minus costs. 

But here's the catch: as we saw in the last blink, investment return rates are at historic lows right now. Take the FTSE 100, the share index that lists the 100 largest companies on the London Stock Exchange. The yield for giants like HSBC, Vodafone and BP is currently just 3.7 percent. That gives you a return of just $37 for every $1,000 you invest. If you wanted to generate an annual income of $30,000, you would need to invest $798,000 in a spread of FTSE shares! 

Investment properties, meanwhile, suffer from serious drawbacks. Whereas income generated by pension investments is tax-free in many countries, rental income is generally subject to both income and inheritance tax.

The upshot is that only the very wealthiest can live off natural yield. That means you might need to think about selling some of your assets.

### 5. Selling assets can make up for shortfalls in your income, but this requires caution. 

Imagine a retiree called David with a $300,000 pension pot. He owns his house and needs just $15,000 a year to cover his expenses. To hit his target, his investments must generate an annual natural yield of five percent. The market, however, only gives him 4.24 percent. That's $12,709, leaving him $2,291 short.

Like many savers, David's assets don't generate the income he requires, and he'll need to sell some of his capital. Isn't that a slippery slope leading to financial ruin? Not necessarily. The trick is knowing what proportion of your pension you can sell off without running into trouble later on. 

Retirees who cover their costs by selling assets usually run into problems because they sell too many shares and fund units when their market value is either lower or higher than average. 

Let's assume you need to raise an additional $500 every quarter to cover your deficit. If, for example, the price of shares in your fund is one dollar apiece, you would need to sell 500. If the price dropped to 90 cents, however, you would have to sell 555 shares.

Selling those additional shares might cover this shortfall, but it will lead to problems down the road. That's because you now have fewer shares producing income. This creates a vicious circle, forcing you to sell even more assets come your next withdrawal. 

The same goes for selling income-producing assets when the price rises. If, for instance, those same shares rise by ten cents, you might be tempted to sell a large number of them while the price is buoyant. But that windfall has a sting in its tail: with fewer assets producing income overall, you'll be forced into making more sales later on. 

So here's the rule to follow if you want to avoid these dangers: _sell a maximum of one percent of your original funds or shares each year_. 

That equates to a manageable one percent decline in your income-producing assets every year. You'll also want to spread these sales across every fund in your portfolio, and sell no matter how well or badly they are performing. This not only takes the guesswork out of selling, but also preserves the portfolio's overall division of income-generating potential among shares, bonds and property.

### 6. Annuities can provide risk-free income in later life. 

As you take an income and sell a portion of your capital, your pension pot will shrink. As we saw in the last blink, this process can be managed by selling no more than one percent of your income-generating assets every year. If you knew how long you would live, this method would work perfectly until the end. Longevity, however, is an unknown factor in your calculations. 

The older you get, the more likely it is that this system will break down. That's a scary prospect: every extra year you haven't planned for is a year without income. This means you need a back-up plan.

That's where _annuities_ come in. An annuity is an insurance contract. In return for a lump sum such as the money in your pension pot, an insurance provider guarantees you an annual income for the rest of your life. This rises by around three percent every year. 

Sounds great, right? Well, here's the problem: annuities are rarely a viable way of transforming life savings into income _throughout_ your retirement. A typical rate for a 65-year-old, for example, would be 2.8 percent. With a $300,000 pension pot, that's just $8,400 a year. 

That changes as you get older. On average, a healthy 65-year-old woman lives to 86. That's 21 years that insurance providers will have to cover. Consequently, the rates they offer are low. For every year shaved off that expected lifespan, the rate increases. 

Let's say you have a $100,000 pension pot. Your starting income at age 65 would be $3,214. By 70, that rises to $3,806; at 80, it would be $6,015. By your late seventies, in other words, you should be able to get an annuity that replaces the target income from your portfolio. 

An annuity also has other benefits. Take security. Annuity providers are legally bound to honor your agreement. That's great for your peace of mind. With an annuity, you never have to worry about the stock market taking a tumble or the economy slowing down. More importantly, you know that essentials like bills and food will always be covered. And if you die before your partner, he or she will receive around 50 percent of your income. 

Annuities are also low-maintenance. Unlike portfolios, you don't need to manage your annuity actively. That makes this an ideal arrangement for people in their eighties who might not have the energy or motivation to monitor investments.

> _"Annuities provide risk-free income, and they pay out for all of your life, however long that may be."_ — Tom McPhail, head of retirement policy at Hargreaves Lansdown

### 7. Funding your retirement by releasing cash locked up in your home is expensive and risky. 

If you own a house, it's likely your biggest asset. Does that mean you should be using it to fund your retirement? In most cases, the answer is "no." Let's see why. 

Taking money out of your home is called _equity release_. As the name suggests, you are freeing up or "releasing" a property's value or "equity" without selling it outright. Generally, this is something older folks do, and the majority of equity-release mortgages are only available to borrowers aged over 55 or 60. 

Borrowing in this way is a big decision. You're essentially draining value from an asset that you've spent a large part of your life working to pay off. This will obviously affect your children or other heirs who will be inheriting your home. Equity release is also expensive. Despite the global fall in interest rates, this remains an expensive form of debt. 

Talking of interest, it's important to remember that this is _compounded_. In other words, you will be paying interest on the interest you already owe. So say you borrow one-third of your home's value at a rate of six percent. Assuming house price growth of four percent, your debt after 35 years would be roughly two-thirds of your property's total value. To put that into numbers, a house worth $1,213,730 would carry $812,355 worth of debt. 

As you can see, this is an expensive business, which is why equity-release mortgages are best treated as a last resort. The rule of thumb here is never to borrow in this way unless you have a clear and urgent need for cash.

There are fairly tight restrictions on how much of the property's value you can borrow. This is linked to age. The younger you are, the less you can borrow. Older borrowers who can tap into more of this value, by contrast, need to ensure that their contract features a _no negative equity guarantee_. This is a promise made by your bank that the size of the loan will never exceed the value of the house. 

The most common type of equity-release mortgage is the so-called _lifetime mortgage_. That means you will be paying a fixed rate for as long as you live or as long as you remain in your home. You don't make monthly repayments, however — instead, you pay down the interest every three years.

### 8. If you want to leave money to your heirs, you need to consider what will happen to your assets after your death. 

It's an unpleasant subject, but there's no getting around it — we all need to plan for what will happen to our assets and income after we die. This is vital, because understanding this issue may change how you plan your retirement, especially if you want to deliver better tax outcomes for your heirs. 

Annuities and final salary pensions can be inherited and will continue to be paid out to your nominated heirs after your death. Typically, your spouse will receive half or two-thirds of your income, though this depends on the terms you concluded with your provider or employer. In most cases, this arrangement has no immediate tax implications. 

Most assets are subject to inheritance tax. The way this works is relatively simple. After death, inheritors of an _estate_ — the total assets of the deceased — are given an _exemption_. This is a portion of the estate on which taxes do not have to be paid. In the UK, for example, the first $400,000 inherited by beneficiaries is tax-free; exemptions for married couples and civil partners can bring that up to just under $800,000. The remainder of this estate is then taxed at 40 percent — the same rate applied in the US. 

Pension pots, by contrast, are treated differently in many tax jurisdictions. Whereas an inherited house will usually be taxed, assets in a pension pot are partially ring-fenced. Say an 82-year-old woman living in a house worth $1 million inherits a pot worth $330,000 from her deceased husband. If she wants to leave something to her two daughters, her best bet is to keep the pot intact, as her house will be liable to inheritance tax regardless of what she does. 

The word "partially" is important because there are complications. While an inherited pension pot isn't subject to inheritance tax in the UK, it may be subject to income tax. This is determined by the age of the deceased. If you die _before_ 75, recipients of the pension pot will not pay taxes; if you die _after_ the age of 75, on the other hand, withdrawals by your heirs will be taxed at the same rate they pay on their own income. 

Right — now that we've broken down the essentials of pension planning, you should be in a good position to start making your own arrangements. As we've seen, there are plenty of tools at your disposal. The key is combining them in the correct order and, where possible, avoiding big risks.

### 9. Final summary 

The key message in these blinks:

**Pensions have changed over time. The generous company pensions of the second half of the twentieth century have been eroded by a combination of falling interest rates and increasing lifespans. Today, savers play a more active role in managing their pension pots. Because most retirees aren't in a position simply to live off the revenue generated by their investment portfolios, they need to sell off their assets gradually to make up shortfalls. As they get older, they can guarantee their income by making use of annuities. Together, these strategies ensure a comfortable retirement while providing for spouses and heirs.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next: Playing with FIRE**

We've just seen how increasing life expectancy means that folks now spend more years in retirement than in the past. That can be a cause for concern — after all, extra years require larger pension pots. The strategies we've explored in these blinks should have put those worries to rest. But what if you could add even more years to your retirement? 

Well, serial entrepreneur and Silicon Valley star Scott Rieckens thinks you can. Welcome to FIRE, a movement that's redefining the way millions of people around the world think about their work-life balance and helping them retire in their 40s. Sound too good to be true? Check out our blinks to _Playing with FIRE_ and decide for yourself!
---

### Richard Dyson and Richard Evans

Richard Dyson is a prize-winning financial journalist and a regular contributor to both specialist and non-specialist titles including the _Express_, the _Mail on Sunday_, _Investors Chronicle_ and the _Daily Telegraph_. Dyson was also the Head of Personal Finance at the Telegraph Media Group. 

Richard Evans is one of Britain's best-known financial commentators, savings experts and investment analysts. His knowledge of the industry is the fruit of years of research and hundreds of one-on-one interviews with the UK's top fund managers and professional investors.

