---
id: 5e4d23786cee07000620a014
slug: youre-not-listening-en
published_date: 2020-02-20T00:00:00.000+00:00
author: Kate Murphy
title: You're Not Listening
subtitle: What You're Missing and Why It Matters
main_color: None
text_color: None
---

# You're Not Listening

_What You're Missing and Why It Matters_

**Kate Murphy**

_You're Not Listening_ (2020) casts a spotlight on the undervalued skill of listening. It's filled with examples of talented professional listeners, as well as practical advice for getting more out of conversations with others — not by saying more yourself, but by listening more closely to others.

---
### 1. What’s in it for me? Discover how to listen when everyone around you is shouting. 

It might not happen often, but you know when you're being listened to. When someone gives you their undivided attention and truly focuses on whatever you have to say, it's a special sensation that can create an instant feeling of closeness — whether that person is your best friend or someone you've just met in an elevator.

Sadly, in this age of instant communication and social media, as our attention spans shrink ever lower, listening sometimes seems like a dying art.

But that just means we need to try harder. Through looking at how some of the world's most effective listeners work, and considering a range of advice, you can become a better listener — which won't just create a positive atmosphere around you, it'll help you grow as a person too.

In these blinks, you'll find out

  * why so many people would rather confide in a stranger than their loved ones;

  * how good listening helps CIA and FBI agents do their job; and

  * how to craft the perfect question to show you're listening.

### 2. Listening is a rare skill – especially today. 

We're not really encouraged to listen these days. It's pretty much the opposite, in fact: we're encouraged to broadcast ourselves. We train ourselves in public speaking, we constantly announce ourselves to the world on social media, and of course we chat away endlessly on our phones.

But when was the last time you truly felt someone was listening to you? And how about the last time you really listened closely to someone else?

It's ironic that even though we're more connected than ever before, we're also experiencing what some have called an epidemic of loneliness, as the population ages and younger people retreat to their phones and computers. It's easier to stay in contact with each other, but the quality of all this communication isn't cutting it; people still feel isolated.

In other words, they're not getting the attention they crave. You won't be shocked to learn that our attention span has gone down recently, but you might be surprised that — since as recently as 2000, according to research by Microsoft — the average attention span has decreased from twelve seconds to just eight. That actually puts us below the nine-second attention span of a goldfish.

What's to blame? Our phones certainly play a part, as well as all the other technological devices we're addicted to. In fact, distractions are everywhere around us — even in the form of the music piped into shops and cafes.

All of which means that there's something extra special about focusing your attention on what somebody else is saying. A good conversation can cut through all the background noise around us. And you'll be amazed at the things you'll learn. Everybody in the world is interesting, the author suggests — you just have to ask them the right questions.

How do you do that, though? The author spoke to some of the best listeners in the world to find out.

### 3. Even in the age of Big Data, listening can bring unique insights into what people want. 

Listening is a vital skill for people in professions from therapy to military interrogation. The author herself is a journalist who needs to know how to extract the most fascinating insights from her interviewees. But even she was struck when she met Naomi Henderson.

Known simply as "Naomi" to those in her field, she's a living legend of focus groups. Over her 50-year career, she's worked on everything from Kentucky Fried Chicken to Bill Clinton's presidential campaign; she was the one who told him to stop playing up his Southern accent. All in all, she has moderated some six thousand focus groups — more than fifty thousand people.

Something about Naomi makes people feel comfortable talking to her. She stays calm and focused, never crosses her arms or legs, and always seems to have plenty of time for you. Her facial expression conveys genuine interest. All of these attributes have made countless focus group participants want to share their insights with her — which is how she's steered so many clients toward what their customers really want.

Focus groups have been big business since the 1940s, but these days they face a rival in the form of _Big Data_ : research has shifted its emphasis from _qualitative_ research, like focus groups, to _quantitative_ research, based on numbers.

Quantitative research can of course be hugely revealing, but it can only ever give you direct answers to specific questions. Any information outside the scope of those questions will remain inaccessible: there's no place for asking "Why?" or "How?" Matthew Salganik, a sociology professor at Princeton University, has compared using a data set to a drunk person searching for his keys under a lamppost because that's the only part of the sidewalk he can see.

Naomi's approach, by contrast, can find the keys even if they're somewhere unexpected. That's proven by the part she played in the development of Swiffer, a mop-like cleaning product. Through an open-ended conversation with some cleaners, she discovered that many of them would mop the floor with lightly used paper towels. That's hardly a question anyone would think to ask in a survey, but it led to the development of an entire product that emulated paper towels in the form of a disposable cloth.

### 4. To be a good listener, you have to be curious about people, and say just enough to show you understand. 

Some of the very best listeners are the most naturally curious people. One retired intelligence officer proves this perfectly: Gary Noesner, former lead hostage negotiator with the FBI.

Noesner has an eccentric habit when he stays in hotels. He goes to the bar, chooses someone, and gets them to talk. The aim is to find out everything he can about them — not because they're under investigation, but simply because Noesner has an insatiable natural curiosity. One time, a salesman told him about his tightrope-walking hobby, and Noesner learned all about how a tightrope walker practices.

His curiosity is the personality trait that helped him talk effectively with terrorists and criminals in crisis situations. Like Naomi the focus group expert, he's instantly likable because of the way he focuses his attention on other people. People want to tell him things.

Another naturally curious former intelligence agent worked in a similar way. Shortly after the 9/11 attacks, Barry McManus, then chief interrogator with the CIA, managed to get a Pakistani nuclear scientist to admit to knowing Osama bin Laden. His method was simply to listen to what the scientist had to say. As it happened, he started talking to McManus at length about African-American people in the US, demonstrating his impressive knowledge of American history. After doing so, the scientist felt close enough to McManus to share his story about Bin Laden.

That example proves another key point about listening effectively: you don't have to say very much. What's important is simply that you're truly following the conversation. That doesn't just mean nodding or repeating bits back to the speaker — it requires effective interpretation.

Imagine a friend has lost his job. He's bound to be upset, but what aspect of the situation is causing him the most distress? It could be anything from money problems to the difficulty of telling his family. The best response — rather than just a "Sorry to hear that" — will focus in on what's upsetting him, and encourage him to open up about it. Then, chances are, he'll have plenty more to say.

> _"Everybody is interesting if you ask the right questions. If someone is dull or uninteresting, it's on you."_

### 5. Don’t assume you know what someone is saying – especially not the people closest to you. 

Who do you find it easiest to confide in: a stranger, or someone you're close to? Surprisingly, a lot of people prefer to talk to strangers.

This is known as _closeness-communication bias_, and psychologist Judith Coché is an expert in trying to break it down. Her specialty is couples' group therapy; she brings several couples together for long, regular sessions in which they can talk about their relationships in detail. When someone shares their problems, it's not just their spouse who's listening: it's the whole group. And the conversation changes dramatically when you're really being listened to.

Coché's couples make so many breakthroughs simply by being heard. When someone isn't listening to their spouse, the rest of the group can point it out to them. Coché recalls the revelatory moment that a "mansplainer" finally listened to what his wife was saying — she started crying.

The problem is that, over time, close relationships can lead to complacency, and the false belief that we know what our partner thinks and feels. But the past isn't a good guide to the present: people are constantly evolving as events affect them in new ways. So how do you keep track of who your partner is now? Simply by staying curious, and listening — without assuming you know what they're going to say.

Some people may prefer to talk to strangers, but assumptions can ruin those conversations too. We have a terrible tendency to stereotype people by categories like gender, race, and profession. Our preconceived notions about how someone from Texas will behave, for instance, affect how we listen to what they say. It's a form of _confirmation bias_ — we only hear things that support what we already believe. But people are actually far more complex than that.

That's why the author cautions against identifying yourself according to the categories you fall into — saying things like "Speaking as a gay person," or "As a millennial…" Whole categories of people like that don't share the same characteristics. Everyone is unique, with a unique set of life experiences.

So, we should never assume we know what someone else is going to say. In fact, we need to be open to the idea that they might have totally different opinions. Even more than that, we need to be ready to accept those opinions as legitimate. As we'll see in the next blink, that's not easy.

### 6. Listening to contrasting views is tough, but crucial. 

In 2016, a neuroscience study at the University of Southern California in Los Angeles discovered something remarkable. It took a group of people with strong political views, and scanned their brains while challenging their beliefs. The brain scans that resulted were similar to what it would have looked like if they'd been running away from a bear.

Strange though it may seem, it's just that difficult to listen to opposing views. Ahmad Hariri, a professor at Duke University, suggests that, because humans live in relative safety these days — bear chases are pretty rare — the biggest threats we generally face are social in nature. The _amygdala_ — the part of the brain that kicks in when we're under threat — can therefore go into overdrive even when faced with differences of opinion.

This is a tendency we should work to overcome, though. The poet John Keats coined an evocative phrase in 1817: _negative capability_. To achieve things, Keats wrote in a letter, you need to be capable of remaining uncertain and doubtful. Psychologists call this same ability _cognitive complexity_, and it's something good listeners need in abundance. Being able to accept gray areas and contrasting views without mentally fleeing the scene enables you to understand other people better, and will also help you become a better, more nuanced decision maker.

That doesn't mean that you have to _agree_ with other people, of course. And you shouldn't even expect to _understand_ them fully. In fact, misunderstandings are an inevitable and constructive part of a good conversation.

And yet, people are often reluctant to pause a conversation and simply say, "I don't understand." It's usually easier to just move on and hope that it doesn't matter. But clarifying a misunderstanding can be a great way to gain better insight into someone else's mind. Because we can't presume to know what someone else is thinking, it's inevitable that they'll sometimes say things that don't quite make sense to us. That's great news, as it gives us the chance to extend our understanding of them.

After all, we can't help but see the world from our own unique perspective. Though we can't fully know other people, if we know ourselves, we'll understand how our own views color the way we interpret other people's. Accept that, and you'll start to think of opposing views and misunderstandings as opportunities to listen more deeply and grow as a person.

### 7. One of the keys to good listening is good questioning. 

A good listener will probably find themselves saying less in a conversation than the person they're listening to. But whatever the listener _does_ say needs to be exactly right.

Sociologist Charles Derber of Boston College says that there are two basic types of conversational response: the _support response_ and the _shift response_. Say someone tells you that her dog recently ran away for several days. If you answer by explaining how your own dog never gets out, that's a shift response — you've shifted the emphasis onto you. A support response, though, might be to express sympathy and then ask a question — like where she eventually found her dog — that encourages the speaker to tell _her_ story more fully.

As you might expect, shift responses are more common — but a good listener is an expert in support responses. What's especially difficult about support responses is making sure that they're not subtle ways to impose your own view — it's better to encourage the speaker to share her own.

You should also try not to worry about appearing knowledgeable. Some people are so concerned about this that they ask questions that imply they already know the answer. But while a "Don't you think that…" sort of question might _seem_ like a support response, it's really a shift response in disguise.

Shift responses aren't necessarily pure selfishness. They can also be born of a genuine desire to help the other person. That's actually one reason that support responses are so tough to get right — they require you to acknowledge that you can't fix the other person's problems. Nobody can. A good listener is more like a sounding board, aware that the best they can do is help the speaker come to his own realizations.

The Quakers, a Christian group, hold "clearness committees" devoted to this type of careful listening. In a clearness committee, several members will ask someone well-chosen questions to help solve a problem that person's been having — a support response writ large.

In the 1970s, a clearness committee helped the Quaker Parker Palmer decide whether to accept a prestigious but demanding job. They asked what he'd like about the position — and calmly repeated the question when he responded with what he _wouldn't_ like about it. It was only through this close listening that Palmer realized he only wanted the job for the prestige.

Palmer went on to set up the Center for Courage & Renewal, a nonprofit organization that aims to bring clearness committee techniques to a wider public. Teaching people how to listen can be a lifetime's work.

### 8. Listening to others means not seizing control of the narrative, and trying to silence your own inner voice. 

A lot of people like to be the center of attention and control a conversation's narrative. A comedy improvisation session — if it's not a very good one — can be alarming proof of that, as people compete for the limelight and the funniest comeback. But in fact, for truly effective improv, listening skills are vital.

Improv is a great way to learn to maintain focus. At the famous Second City in Chicago, Artistic Director Matt Hovde runs a training program for beginners. One game they play is group storytelling. Hovde controls which group member narrates a made-up story, and also decides when the narrator changes. It forces the whole group to listen closely, so that, if they're called upon, anyone can pick up the thread.

In that situation, it becomes immediately apparent if you haven't been listening — or if you're so eager for a quick laugh that you ruin the whole narrative with something ridiculous. Business meetings, too, can be derailed by insensitive contributions, which is one reason why more and more companies are turning to improv to improve employees' listening skills.

It's not just in group scenarios that we vie for attention, though. Even in a one-on-one conversation, our overactive inner voices can cause our minds to drift. When we snap our attention back to what the speaker is saying, we may have totally lost track.

Another problem is that, even if we're still focused on the topic, we can become obsessed with what to say next: it's not just improv class participants who want witty comebacks. But by fighting this tendency, you'll hear more of what the speaker is saying — which will make responding easier.

Here's an even harder tip: embrace silence. Westerners, especially, go to bizarre lengths to avoid it. In Asia, where silence is often better tolerated, American businesspeople sometimes end up talking themselves into bad negotiating positions just to avoid awkward pauses. But does a pause have to be awkward? It can be a sign that you're really thinking about what someone's just said.

Now for a more challenging question: could you go a whole day without talking? Canadian composer R. Murray Schafer occasionally asks his students to try this out. By the end, they tend to be far more attuned to the extraordinary wealth of sounds that surround us all.

Don't be afraid of silence, then — and try turning off your own inner voice once in a while.

### 9. Listening is hard work, and sometimes you have to ration it, but it’s always worth the effort. 

As if everything we've covered so far about how to listen wasn't challenging enough, bear this in mind: you need to listen not just when others are talking, but also when _you're_ talking.

Every speaker has to read her audience. What are they actually interested in, and what leaves them cold? You might have a really fascinating story to tell, but it'll be an uphill struggle if the person you're talking to is thinking about something else, or simply has zero interest in the topic.

To read listeners effectively, you need to be sensitive to subtle cues, including both verbal responses and body language. If that still doesn't do it, check — ask if you're making sense to them.

But don't expect them to always be attentive. Nobody can listen closely all the time. After all, it's an active, demanding activity, and we should be sensitive to the effort it takes. Think about the short shifts that air traffic controllers do. Any longer than two hours or so, and they'd start to pose a risk because of concentration lapses.

So what do you do when you're the one out of listening energy? Politely take a break — don't just continue half-heartedly. Listening may be a rare skill, but everyone knows when they are and aren't being listened to; it's not something you can fake.

Think about why you lack the energy, though. There may be certain people that you find it particularly hard to listen to. It's worth stepping back, and asking yourself why that is. Do you find them repetitive or boring? Do you disagree with them? Maybe you're afraid of intimacy with them?

Whatever your reasons, consider whether those reasons say more about _them_, or about _you_.

Maybe it's ironic, but to be a good listener, properly attuned to what someone else is saying, you have to know yourself very well indeed — your biases, your tendencies, your limits, what _you_ really want out of the conversation.

That doesn't mean that good listening is a selfish act. Far from it. But it does mean that good listening doesn't just benefit the speaker — it benefits you, the listener, too.

### 10. Final summary 

The key message in these blinks:

**It's tough to listen closely in the modern world, and few people really do. But the benefits of listening are huge, whether you're a focus group moderator, a government agent, a therapist, or simply a partner or friend. By embracing your natural curiosity, being careful not to make assumptions, and asking carefully constructed, supportive questions, you can improve your listening skills — to the benefit of both yourself and everyone around you.**

Actionable advice:

**Listen to the people you're closest to.**

The best place to start improving your listening is right under your nose. It's common to feel you know your partner, or your close family and friends, so well that you can predict what they're going to say. Too often, though, that means that we don't really pay attention to them.

So, next time they talk to you, try out your listening skills on them. Listen to every nuance of their voice, watch their body language, take in what they do and don't say. They're bound to surprise you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Just Listen_** **, by Mark Goulston**

In these blinks, you've learned that being a good listener benefits you and not just the person you're listening to. But how exactly does that work?

In _Just Listen_ (2009), psychiatrist, consultant, and business coach Mark Goulston goes into more detail about how working to become a better listener helps to improve you. Good listening doesn't just help you understand yourself better; it can also improve your communication skills, helping you become more persuasive. So if you're ready to level up through listening, head on over to our blinks to _Just Listen_.
---

### Kate Murphy

Kate Murphy is a journalist from Houston, Texas. She has become a talented listener through her many interviews and articles for publications including the _New York Times_, the _Economist_, and _Texas Monthly_, as well as Agence France-Presse.

