---
id: 566ffd98cf6aa30007000052
slug: youthnation-en
published_date: 2015-12-18T00:00:00.000+00:00
author: Matt Britton
title: YouthNation
subtitle: Building Remarkable Brands In a Youth-Driven Culture
main_color: EFEC30
text_color: 99971F
---

# YouthNation

_Building Remarkable Brands In a Youth-Driven Culture_

**Matt Britton**

_YouthNation_ (2015) offers essential insights on modern-day youth — a generation, lifestyle and rising phenomenon that will be essential to any business's success in the future. These blinks will teach you the ins and outs of YouthNation and help you build an up-to-date strategy for your company.

---
### 1. What’s in it for me? Learn how to prepare for the huge influence of youth culture. 

Who determines the strength of a nation's economy? Politicians? Middle-aged people in pinstripes? The global market? Not necessarily. In fact, it's young people that hold massive, disruptive power for modern-day economies.

Together, the 80 million American citizens born between 1982 and 1998 form YouthNation. As a well-informed group, with fresh ideas, high-speed internet and all the necessary gadgets at their command, they have all they need to organize their own entertainment, exchange goods as they like and develop entirely new ways of social interaction. Without a revolution, they are transforming American culture, business and politics. 

Accordingly, businesses have to become aware of the ideals and practices of YouthNation, and must adapt their own strategies in order to succeed. How? Learn more about it in these blinks.

In these blinks, you'll also find out

  * how a blackout at the Super Bowl helped sell billions of cookies;

  * how hunting for experiences can prevent you from actually experiencing what you are doing; and

  * why people with jobs will become a minority in the near future.

### 2. The youth of today don’t care about traditional status symbols. 

If you're from an older generation, you might remember that young people used to be motivated by the promise of status symbols and would work long and hard to buy a beautiful home or a cool car as soon as they could. But in the '90s, youth culture began aspiring to different symbols of status than their parents did. 

An emerging hip-hop music scene and pop culture came together with a strong economy to deliver consumer goods to young people in a way they never had been before. 

For instance, one of the first status symbols to emerge in the early years of hip-hop came from the rap group Run-D.M.C. who, in 1986 released their hit song "My Adidas." The song quickly earned them a multimillion-dollar endorsement deal from the shoe manufacturer as their sneakers exploded in popularity. 

But another status symbol rose to prominence during the same period: logos. For example, companies like Gap and Abercrombie & Fitch began brazenly branding their outerwear with their companies' symbols, and clothing became highly fashionable just because of the label it bore. 

However, that all changed with the financial collapse of 2008, after which material status symbols became less important to many. Young people were seeing their families suffer from the economic collapse and, as a result, lose interest in the material symbols of wealth that had once been so essential to their identities. 

After all, asking your parents to cough up $200 for a pair of Air Yeezys when they can barely pay their mortgage doesn't exactly seem like a sensible idea.

### 3. Today, status is defined by what you experience rather than what you buy. 

So, if today's young people aren't enticed by material symbols of status, what are they looking for?

They want an unforgettable experience _right now_.

In recent years, a new metric and definition of status has developed, one that has little to do with material objects and everything to do with different experiences. Members of the _YouthNation_, Americans born between 1982 and 1998, are always looking to tell their friends about the things they've done. As a result, platforms like Instagram have become wildly successful as a means to share experiences online. 

Not only that, but being constantly inundated by photos of their friends doing amazing things is making all Instagram users yearn for those experiences themselves. So, the game has changed from collecting sports cars in your garage to collecting exotic stamps in your passport. 

But is this trend a good thing or a bad thing?

Well, it actually has both positive and negative effects. Because while there are definitely upsides to getting out into the world and experiencing life, the pressure to do so has had some negative impacts that need to be addressed by the likes of entertainers, teachers, parents and businesses. 

For example, we've become so intent on documenting our experiences that we often don't even experience them while they're happening. Think of a person who is so excited to snap photos of the most beautiful views on his trip that he spends the whole journey with his eye glued to a viewfinder. 

Or take the author, who had recently seen Coldplay in concert. At the show, the lead vocalist, Chris Martin, asked the audience to return the sea of smartphones he saw to their pockets, since the band was about to debut a song they had yet to release. The audience politely obeyed and something incredible happened: they actually experienced the music!

### 4. Present-day youth rent and share houses or cars rather than own them. 

Were you raised in a house that your family owned? Many Americans were, but in the future, buying a house might become an exception rather than a rule. 

Why?

Well, the financial crisis of 2008 forced young people to rethink housing. Mortgage lenders became averse to risk and therefore less eager to grant loans to young professionals. As a result, it became harder for young people to buy homes and they had to look to other housing options. In fact, between 1983 and 2013, home ownership among 18- to 34-year-olds dropped almost 20 percent!

But houses aren't the only thing affected — car ownership is also less popular.

The number of drivers between the ages of 16 and 24 has substantially decreased since 1997, and recently dropped below 70 percent for the first time since 1963. After all, why pay all the costs of insuring, parking and maintaining a car when you can rent one at a moment's notice?

So, the youth of today set out to find easier, less bureaucratic ways to share cars and apartments. Their search culminated in the emergence of two major tech companies:

The first is _AirBnB_, which lets any user rent their apartment or room to any other. Needless to say, it has really taken off: the site has over half a million active listings in almost 200 countries. 

The second is _Uber_, a ride-booking app that's so easy, it's making people question the desirability of car ownership. To get a ride using Uber you just set up an account once, open the app and push a button. 

After getting picked up and arriving at your destination, you just get out without the hassle of signatures or awkward tips, and everything is billed to your credit card. 

Both companies have excelled because of their easy to use services built for mobile devices, and other companies are following suit. The so-called _Uberization_ of the economy is reaching every economic sector, from food delivery to cleaning.

### 5. Traditional companies need to adapt to the changing nature of consumption. 

So, young people's consumption habits have changed and so has the economy. Nowadays young people do everything from dating to exchanging money faster and more easily through the power of the _peer-to-peer economy_. This also means that an increasing number of the products and services that YouthNation is consuming are coming from their peers instead of businesses. 

This trend is in large part due to the connective power of the internet, which allows all consumers to buy and sell anything they want directly to one another. The result is a disruption of every industry that stands in its path. 

Just think about it: why would you buy a camera at a store when you can get a brand new one on eBay for a fraction of the cost?

Given this shift, corporate America will need to reinvent itself if it's going to meet the changes brought along by the peer-to-peer economy. For instance, a recent study conducted by UC Berkeley found that just a single shared car entering the marketplace can mean lost auto sales of over $270,000. So, to survive, traditional corporations will need to adapt to a consumer base that favors access over ownership. 

How?

Well, to retain old clients and attract new ones in spite of changes in the economy, companies need to offer their customers new ways to share, connect and conduct business with each other. Several companies have already caught up with the trend and have fostered successful partnerships or investments that bring their business models into the peer-to-peer economy.

In 2014, Ikea came together with AirBnB to give the home-sharing site's users the option to spend the night in Ikea's Sydney, Australia store. Another great example is Ford, which now offers a discount on its Explorer SUV to Uber and Lyft drivers.

### 6. The people power of the internet is radically changing business as usual. 

Today, when practically everyone has internet access, it's easy to build a community of people who live all over the world. But as we've seen, the definition of a community isn't the only thing the internet is changing — it's also fundamentally altering the nature of business too. A great example is the rise of _crowdsourcing_, a tool for tapping into the knowledge and talent of people all over the world. 

Crowdsourcing is actually a legitimate and extremely cost-effective way of doing business for everyone from the largest corporations to one-person operations. Whether it's the design of a logo, data entry, real-time feedback or advice on advertising concepts, freelancers and volunteers are now an essential force in practically every industry. 

So, while crowdsourcing might seem like a strategy for small businesses and start-ups, it actually applies to everyone. For instance, General Electric recently ran into a problem when the brackets on their newly designed, high-powered jet engine were too heavy to maintain optimal fuel efficiency. 

To solve their issue, the company sought out a relatively unknown internet company named GrabCAD, which boasts the talent of over one million engineers and specialists. General Electric hosted a contest that offered a $7,000 reward for solutions to their problem; $7,000 being a pittance to their normal research and development expenses. 

Then, after looking over more than 1,000 entries, the company gave the prize to a young Indonesian engineer whose insight reduced the bracket's weight from five pounds to a measly 0.72 pounds!

But crowdsourcing isn't only good for gathering knowledge, it's also great for fundraising. In fact, crowd _funding_ is sweeping the world as a financing solution to everything from personal loans to start-up launches, all while disrupting the traditional structures of venture capital and banking. 

For instance, Kickstarter, founded in 2009, has become _the_ platform for crowdfunding in all creative industries. The site is simple: people present their ideas to the online community with a fundraising goal and an explanation of how they'll spend the money. Only if and when the goal is reached are the individual contributors charged.

### 7. Freelancing is the employment style of the near future. 

As you now know, the modern economy is developing an increasing number of ways for people across the globe to support themselves by freelancing. With countless new employment opportunities, easy access to tools and training and a technologically connected world in which the next job is just a tweet away, it's clear that we're transforming into a society of free agents. 

In fact, a recent study conducted by Edelman Berland found that a whopping 53 million Americans are currently engaged in some form of freelance work. That's over a third of the country's workforce!

As a result, freelancers are making a huge impact, producing $700 billion in wages for the American economy — and 38 percent of these independent contractors are millennials. Slowly but surely, the prospect of devoting your whole career to a single company is becoming less appealing. 

While people used to stick with one job to collect more retirement savings through social security benefits, the game appears to be changing to one in which people strive to earn as much as possible to secure their own retirements. 

Over the last 25 years, the proportion of private wage-earning and salaried employees that have joined traditional benefits plans has almost been cut in half, and now stands at just 20 percent of all workers. 

But that's not all that's changing with the new peer-driven economy — it's also producing a greater degree of independence. As the YouthNation expands to make up the majority of the working adult population, the community values it holds and beliefs it stands by, like a desire for flexibility, will dramatically shift the economy toward one of self-employment. 

For instance, the US Bureau of Labor Statistics has reported that millennials make up 36 percent of the current workforce; in ten years that number will jump to 75 percent! This means that in the near future, individual workers will likely be trading in jobs and employers for projects and clients.

### 8. Social media, done right, can launch your product to unforeseen heights. 

In the '60s, making an impact in advertising meant securing your product an ad during primetime TV. But social media has made expensive ad slots a thing of the past by opening up countless opportunities for you to make your message stick. 

In fact, in this day and age you can run a hugely successful marketing campaign using nothing but social media. For instance, do you remember the Super Bowl blackout in 2012?

Oreo jumped on the opportunity to tweet a simple image of a backlit Oreo that read, "Power Out? No problem. You can still Dunk in the Dark." The result of this post, costing not one penny of media support, earned the company over 525 million clicks across 100 different countries. Not only that, but this one little tweet became the symbol of a widespread change in advertising: it was the first time ever that the top Super Bowl ad didn't even run on TV!

However, millions of different messages are circulating on the internet and vying for attention. Your message is useless unless it gets to the right people at the right time. So, to take your message viral you need to attract the right supporters. 

The one thing that gives you tremendous power on social media is a base of people that capture the interest of many others. If you can get people like this to back your product, countless others will follow suit and it won't cost you a thing.

For example, remember that viral video Gangnam Style by South Korean pop star Psy?

It's gotten over 2 billion views, making it one of the most watched pieces of entertainment _ever_. While the video's rise to stratospheric fame might appear to have occurred by chance, it was actually due to a carefully planned internet marketing campaign. 

Here's what happened:

When the video started to gain traction in South Korea, Psy enlisted American mega-manager Scooter Braun, whose client list includes Justin Bieber. Braun instrumentalized Bieber's social media fan base to promote Psy, thereby summoning the powerful interest of millions of "Beliebers."

### 9. Final summary 

The key message in this book:

**The technological revolution has transformed the economy into one driven by youth, which has meant a complete disruption of the marketing strategies of the past. It's therefore essential to understand the driving forces of the new economy: a group known as YouthNation.**

**Suggested** **further** **reading:** ** _Paid Attention_** **by Faris Yakob**

How can you get people interested in your brand in an age of ad-blockers, vanishing attention spans and colossal consumer choice? _Paid Attention_ (2015) discusses the fast-changing media landscape, and maps out strategies for success that reach beyond banner placement and pop-ups.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Matt Britton

Matt Britton is the founder and CEO of MRY, a global creative agency that specializes in youth marketing. Their clients include Microsoft, Visa and Johnson&Johnson. Britton has contributing articles to _The Wall Street Journal_, _Bloomberg_ and _The New York Times._

_©_ Matt Britton: YouthNation copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

