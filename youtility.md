---
id: 52b17b0031303300120b0000
slug: youtility-en
published_date: 2013-12-18T11:21:25.282+00:00
author: Jay Baer
title: Youtility
subtitle: Why smart marketing is about help, not hype
main_color: 7FC1E9
text_color: 2E6282
---

# Youtility

_Why smart marketing is about help, not hype_

**Jay Baer**

_Youtility_ (2013) goes against the grain of accepted marketing methods by declaring that information, not promotion, is the way to win customers. Counterintuitive and refreshing methods are presented, repositioning the relationship between businesses and consumers. The book outlines examples from a wide spectrum of companies, big and small, driving home the message that by helping people and being useful instead of chasing sales, companies can prosper in the long-term.

---
### 1. When choosing products we increasingly seek personal advice on social media from friends, family and fellow shoppers. 

How do we decide what products to buy?

Until recently internet search engines dominated our product choices. When you wanted something, you would probably just Google it. However, this practice is declining fast. In 2004 83 percent of customers used search engines to find a relevant company website; by 2011 this had dropped to only 61 percent.

This is the result of social media making us more research savvy. Thanks to the rise of social media, we can access far more source — from Twitter to Facebook and beyond — to research our purchases. We now use more sources of information than ever before: the average number of sources used by customers when choosing products doubled from five in 2010 to ten just twelve months later.

Why have we switched to social networks to sound out potential purchases? Because they provide us with recommendations from people whose suggestions we trust. Whenever we want something we can directly appeal to our friends and family through tweets or status updates. Whatever product or service we require we need only ask, "does anyone know…?" and there's a flurry of replies from those who know us best, packed with opinions and recommendations. This feedback is personalized and therefore more directed than a search engine.

We can even appeal for advice from a wider audience beyond our friends and family: our fellow shoppers. On networks like Angie's List we can ask for consumers' personal recommendations and experiences of companies or contractors. As these answers come from first-hand experiences we value them more highly than we would the results of a mere internet search.

**When choosing products we increasingly seek personal advice on social media from friends, family and fellow shoppers.**

### 2. By bombarding people with unwanted messages, companies lose the trust that's vital for building audiences on social media. 

Social media is crucial in helping companies build up a huge audience — that's why firms spend vast amounts on promotional campaigns. But for these tactics to be successful, a company needs to establish a relationship of trust with their customers, because people are heavily influenced by reliable sources on social media. The more trust a company has, the more successful it is likely to be in its promotions.

However, even if a company expends a lot of effort on gaining this reputation for reliability, it can be incredibly difficult to preserve.

The main way companies can lose consumers' trust is by bombarding their news feeds with lots of intrusive or inappropriate messages. This leads to the situation where personal posts from friends and family are interrupted by various exclamations about how great a company's products are. This is actually the most common reason for people to "unlike" a company on Facebook, meaning that the resources used for promotion are ultimately wasted.

The toilet paper company Charmin is a good example of losing customer trust. They developed an app called Sit or Squat which saves users time and discomfort by showing them nearby public toilets and whether they are clean ("Sit"), or not ("Squat"). Charmin hoped to build up a huge network of followers by providing useful, relevant information.

This, however, backfired. The app encouraged users to leave toilet reviews which found their way onto their profiles on social media, so that their friends and family ended up reading these undesirable status updates. Many users, unhappy with this information being shared, vented their anger against the app and the company.

**By bombarding people with unwanted messages, companies lose the trust that's vital for building audiences on social media.**

> _"Sell more by selling less."_

### 3. Companies can gain trust by offering consumers information which is accessible, impartial and relevant. 

You'd think that a company which advises customers to do things like try a competitor's products would soon go bust. But this isn't the case. In fact, providing customers with impartial information can boost business in the long-term.

This is because customers appreciate free and honest advice and so by providing it companies can build up trust. One company that has successfully adapted the policy of providing advice is the garden supplies firm, Scotts Miracle-Gro.

In order to generate this trust, companies need to ensure that their advice follows three key principles.

Firstly, they provide _self-serve_ information. This means that customers should be able to help themselves to information without interference or promotion from the company. Scotts Miracle-Gro has an app that lets users educate themselves about weeds and pests. Users can independently access it without the company aggressively steering them towards its own products.

Secondly, dialogue with both current and potential customers should be _transparent_. This means that whatever questions are asked, the company should answer them accurately, honestly and quickly. Scotts Miracle-Gro has a policy of answering all enquiries — even those from non-customers — within two hours. The information they provide is completely impartial. In doing this they provide a helpful and well-regarded service.

Finally, the help should be _relevant_. It should be timely, localized and appropriate to each customer's immediate situation. The Scotts Miracle-Gro app allows users to upload photos of their garden or pest problem so that employees can help them out directly. The company also distributes informative email newsletters, customized to the customer's region — even to their grass type — so the advice they get is relevant.

**Companies can gain trust by offering consumers information which is accessible, impartial and relevant.**

### 4. Mobile technology allows companies to tailor information to customers' precise location and interests. 

We have become very attached to our mobile phones. It seems we can't live without them. In fact, the number of mobile internet users will soon overtake desktop internet users. Mobile technology's chief appeal is its ability to let us do many tasks on the go, so we can get more done. We spend relatively little time talking on our phones — in fact we value their other functions, like cameras and internet access, far more.

Location-based mobile technology is another feature which makes life much easier for us. This technology pinpoints the user's location and builds up a map of the surrounding area. It can be used in conjunction with other services to provide us with up-to-date information, like providing directions, lists of neighborhood stores and local weather forecasts. It helps us by providing immediate information about our locale.

The technology and the highly customized and relevant information it provides can also benefit companies. For example, location technology has led to innovative ways for companies to advertise.

Some buses in New York and Chicago display digital adverts on their sides that change every time they stop. Information based on the social media check-in data of people near the bus is collected, providing information on their age, gender and interests. This data is then analyzed to determine which advertisement would be most effective in this location. Then at each red light the display is changed to show the most appropriate advertisement.

While there may be privacy concerns arising from this use of our personal data, location technology has greatly helped companies to build up their customer knowledge.

**Mobile technology allows companies to tailor information to customers' precise location and interests.**

### 5. Software that analyzes websites, web searches and social media lets companies exploit current trends and perceptions. 

Knowing what the customer wants is crucial to developing the best products and services. A wealth of relevant information, such as customer search results and shopping habits, exists online. But it's tricky to extract specific information from such a massive global network. 

Software tools which analyze websites, web searches and social media are the only way to make these insights possible. Using these technologies, companies can target their products and services to those groups who will find them the most useful.

For example, your website can be assessed using tools like Google Analytics. These allow you to analyze visitor behavior on your site and also show you how the site is used on different devices, including, most importantly, mobiles. This is particularly vital because the economy is increasingly driven by mobile apps.

Software which analyzes search engines is another important tool. Understanding what people are searching for online can help a company tap into the Zeitgeist. This software can establish which searches are gaining or losing momentum, and also help identify your competitors. When companies know what people are looking for they can amend their own products accordingly.

It is also possible to use software to analyze collective trends on social media. For example, searching Twitter for all occurrences of users discussing a certain brand or product type. This provides a collective insight into whether products are seen as valuable or not. It could also help identify new marketing opportunities. For example, most tweets about the Corvette car manufacturer are warm and nostalgic about fathers, which could lead the brand to employ marketing related to Father's Day.

**Software that analyzes websites, web searches and social media lets companies exploit current trends and perceptions.**

### 6. Detailed and frequently updated blogs attract consumers hungry for information. 

The internet has unlocked knowledge for us all, and there is more competition for customer attention than ever before. Companies must therefore invest time and attention to keep customers attracted to their own products.

One way to capture customers' attention is to update blogs frequently. Those that blog more provide customers with the information that they desire, meaning that they continue to flock back with their queries. There is no question how successful a frequently updated blog can be: companies that blog for more than fifteen days a month get five times the traffic than those that don't.

While blogs should be updated regularly, the type of content is equally important — the posts need to be detailed. People want answers, solutions to problems and the best value for their money. Educated customers will also benefit sales. A customer who learns about the different product types and prices — for example through published blog posts — bypasses time-consuming conversations about the basics. They know exactly what they want.

This can be demonstrated by the actions of the pool-building company River Pools and Spas. In 2008 the company was hit badly by the financial crisis and in desperation they starting writing blog posts answering all the questions about pool building they'd ever been asked. Soon they had a rich, detailed blog which was popular with customers who were able to educate themselves about the pool building process. It worked: while competitors were going bust, River Pools and Spas were selling more than before the crisis started. In fact 80 percent of visitors who had read at least thirty posts bought a pool. Their informative and frequently updated blog attracted customers away from rivals and made them more successful.

**Detailed and frequently updated blogs attract consumers hungry for information.**

> _"Content is fire, and social media is gasoline."_

### 7. Religiously and honestly answering customer questions improves a company’s reputation and trustworthiness. 

McDonald's Canada were aware that people had doubts about the quality and safety of their food. So in a bold move they let customers ask whatever question they wanted and pledged to answer it. They intended to bare all in front of as large an audience as possible using social media.

One person asked whether the "100% beef" in their burgers meant everything, including internal organs, or just the standard cuts found in shops. McDonald's replied by listing all the cow parts they used along with the name of their well-known Canadian supplier. They were factual in their responses, without the usual product hype. As a result, customers trusted the replies and rated the company higher for "good quality ingredients," in contrast to popular perceptions of the brand.

McDonald's reacted in this way because they were aware that it is simply impossible to keep information private. Social media has meant that information, whether good or bad, can be shared among millions of interested parties. Companies can no longer control what people know about their business practices. By acting like McDonald's, and sharing all information between all customers openly, companies can gain a reputation for honesty and trustworthiness.

McDonald's took this policy of honesty to the highest level of the company. When one lady asked why McDonald's burgers looked different on purchase to the images the restaurant promoted, she received a personal video response from a company director, showing in detail the heavy styling and retouching done by food stylists before photos. Despite this admission, the honesty had a positive effect: customers appreciated being told the truth. In-touch and candid management helped build public esteem for the brand.

**Religiously and honestly answering customer questions improves a company's reputation and trustworthiness.**

> _"If you sell something, you make a customer today; if you help someone, you make a customer proclaim for life."_

### 8. Employees form a valuable, trusted part of the online presence of a company. 

Staff at Hilton hotels have their own Twitter account which they use to help people with queries and concerns. Because the employees' networks extend further than the company's own network, it is easier for those in need to find help. What is especially interesting is that this employee help is not limited to Hilton customers. The knowledge is imparted to anyone and on subjects unrelated to their business.

The replies come from regular Hilton employees who share their personal and local knowledge of their home city. For example, one user tweeted that their dog was ill and needed a vet in Memphis. After seeing the tweet, a Hilton employee recommended a local vet and gave their address. The owner took the dog there and later tweeted his gratitude and amazement that a company like Hilton would reach out beyond their immediate business goals.

Having employees act in this way is very beneficial for the brand. They provide information to potential customers that is tailored and personal to them — something missing from company press releases and official communications. Furthermore, because individual employees will also be better able to maintain contact with others online over a number of messages, they can get a better understanding of someone's problem and therefore provide a more effective answer.

Services like this are highly regarded because the staff are friendly and open. Customers appreciate that these employees are being helpful rather than trying to sell them something. The trust generated helps them to build a relationship with the company, hopefully encouraging them to switch their business to them in the future.

**Employees form a valuable, trusted part of the online presence of a company.**

### 9. To best serve customers, companies must adapt quickly to new opportunities unleashed by changing technologies. 

Until the 1990s, we used the hundred-year-old Yellow Pages to find local businesses, and it was therefore essential for marketing. Yet suddenly this once useful product became obsolete as the web and mobiles replaced it, making it a victim of technological advancement.

Today, with constantly changing technology, companies need to be flexible in engaging with customers, otherwise they could end up like Yellow Pages. There are now many ways to build a relationship with the customer, and firms need to find the optimal route to making their existing services as useful as possible. It could be, for example, an app for on-the-go smartphone owners or a detailed blog for those who need more comprehensive answers.

While companies will have to adapt their existing services to fit this changing environment, they must also be aware of new possibilities that spring up. New technologies create new opportunities and new ways of interacting with the customer. Quite often entire new markets are created by technological change.

Sometimes totally novel ways to be useful crop up as a result of this. For example, while watching a long film Dan Florio needed to use the toilet. This gave him the idea for an app which told users the "safe" times to use the bathroom in films without missing crucial dialogue. He devised RunPee using new technologies to realize an original idea.

But, just as importantly, new technologies can unlock existing content that was previously inaccessible to most people. For example, Phoenix Children's Hospital designed an app which condensed confusing medical jargon into information that every parent could easily understand. In doing this they made old content useful.

**To best serve customers, companies must adapt quickly to new opportunities unleashed by changing technologies.**

### 10. Final Summary 

The main message of this book is:

**To stand out from competitors, companies should sacrifice immediate profits and instead help customers. By allowing customers to serve themselves without company promotions or interference, by answering all their questions and by being relevant, firms will be successful in the long-term. This will build trust in the brand and is especially effective on social media, where it influences many purchasing decisions.**

### 11. Actionable ideas 

Actionable ideas from this book in blinks:

**Truth speaks volumes**

The internet is awash with agendas, biases, opinions and adverts, but somewhere among them are facts. If someone can't find the answer from you, they will go elsewhere. So answer all questions and remember that putting out your own factual and comprehensive information first makes you a trusted source. People will remember where they got that good advice or help and come back for more.

**Promoting your advice on social media helps maximize its effect**

If you want to reach the largest possible audience to show you are helpful and useful, engagement with people on social media is paramount. Demonstrate your knowledge and assist those in need, but advertise only your usefulness, which needs no fee. Avoid using "loudspeakers" and posting too many messages. By acting like a friend first, you will be afforded the attention of a friend, and good feedback will spread far and wide.

**Note down ideas**

If you ever want to develop an app, blog or any other way of providing value to people, remember that simplicity is crucial. Inspiration for good ideas can come from anywhere and at any time, even when standing in a queue or watching a film. It doesn't even need to start from scratch: if you find some valuable information that is concealed behind complex or difficult-to-understand writing or functionality, develop it into something more manageable and accessible.
---

### Jay Baer

Jay Baer is a digital marketing consultant, blogger, podcaster, speaker and founder of five companies. He has consulted for 29 companies in the Fortune 500 and his Convince & Convert blog is ranked world's number one by the Content Marketing Institute. His extensive experience with web companies led him to co-write books that help companies stand out from competitors through social media intelligence and responsiveness.

