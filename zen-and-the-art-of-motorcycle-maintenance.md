---
id: 565425406362640007610000
slug: zen-and-the-art-of-motorcycle-maintenance-en
published_date: 2015-11-26T00:00:00.000+00:00
author: Robert Pirsig
title: Zen and the Art of Motorcycle Maintenance
subtitle: An Inquiry into Values
main_color: EE7F55
text_color: AB5B3D
---

# Zen and the Art of Motorcycle Maintenance

_An Inquiry into Values_

**Robert Pirsig**

_Zen and the Art of Motorcycle Maintenance_ (1974), using the allegory of a motorcycle road trip, guides you through one man's questionings of the philosophical and metaphysical order of the world.

---
### 1. What’s in it for me? Balance your classical and romantic mind and achieve harmony in life and work. 

What does Zen, a holistic, meditative and spiritual practice, have to do with tinkering with the gears and shafts of a greasy motorcycle? A lot, it turns out, if you're trying to live a balanced life.

If you are "classically" minded, you tend toward science, approaching problems rationally and working to create order in a chaotic world. If you are more of a "romantic," you embrace the chaos, understanding the world through emotion and exploring the whole, not the details.

Yet few people can perfectly balance these two mind-sets within themselves; in fact, many of the world's problems and conflicts stem from exactly this gap between the classical and the romantic modes of thought.   

So how do we overcome this gap, and live a balanced and harmonious life? Well, we take a road trip, of course — one that can teach us how to travel that magic middle road.

In these blinks, you'll learn

  * how exactly philosophy relates to motorcycle maintenance;

  * why a romantic thinker won't fix a broken motor himself; and

  * why quality is the perfect balance of rational and spiritual.

### 2. Western thought is split in half, and one half is the rational, unemotional “classical” mode. 

The narrator's journey begins at the start of a motorcycle road trip he's planned with his son, Chris, and a married couple, John and Sylvia Sutherland. 

On a philosophical level, the narrator represents the _classical_ mode of thinking, while the Sutherlands represent the _romantic_. 

Using the metaphor of motorcycle maintenance, the _classical mode of thought_ finds its expression in the rational knowledge and expertise of an engineer or mechanic.

A classical thinker like a mechanic understands all the technical details that make a machine function, how they all fit together and, importantly, how to find what's wrong and fix it if the machine malfunctions.

When presented with an engine, for example, a classical mind is fascinated by the rich underlying symbols and functions of form that make a machine work — the gears, the belts, the pistons and all the complicated interactions that make a machine what it is. 

Looking beyond motorcycle maintenance, other examples of classical thinking include things such as the scientific method, logic and mathematics.

These fields are underpinned by highly systematic, reliable and rational systems. They abide by an established set of rules that have been tested and verified. Each new innovation within a system is built upon pre-existing norms, which are themselves built upon the same standards and rules — thus making the classical mode of thought predictable, straightforward and unemotional. 

Ultimately, the classical mode aims to bring control and order to the chaos of the world.

### 3. The opposite of a classical mind is the romantic: thought driven by emotional and creative urges. 

In contrast to the narrator's "classical" mind, his road trip partners, John and Sylvia Sutherland, represent the "romantic" mode of thinking.

The Sutherlands refuse to learn how to fix their motorcycle on their own, even though it would be much cheaper to do so — and could make them more self-reliant riders, knowing the basics of how the machine functions.

For example, when the couple's motorcycle begins to have problems, the narrator suggests a rough, do-it-yourself solution — using a soda can to replace a malfunctioning part — an idea that horrifies John, as it would compromise the sleek, romantic aesthetic of his expensive BMW motorcycle. He refuses his friend's advice and insists on taking the bike in for repairs. 

As a classical thinker, the narrator doesn't understand why John would rather pay, both in terms of money and time, for something that could be easily fixed.

The narrator soon realizes, however, that the reason why the Sutherlands refuse to learn or understand the workings of their bike is that they resent the creeping power of technology in their lives. Their refusal to engage with the technology is simply their way of fighting it. 

In general, romantics are driven by the emotional, inspirational, creative, imaginative and intuitive modes of life. The strengths of the classical mode are seen as weaknesses by romantic-minded people. They see the human experience as neither predictable nor controllable; instead, life is full of chaos and emotion, the very forces that classical thinkers seem to ignore or attempt to control.

The narrator is baffled by his friends' admiration of their motorcycle as a beautiful object, while rejecting its efficiency as a powerful machine. Yet romantics often value aesthetics over practical application. To the Sutherlands, understanding their machine would simply undermine its beauty!

### 4. The dual nature of Phaedrus, a “madness” now cured, haunts the narrator’s classical mind. 

During the road trip, the narrator begins to remember and reveal parts of his past identity, which he calls Phaedrus, a "madness" that was cured after undergoing electroshock therapy.

Phaedrus was a philosophy student and English professor who struggled with the tension between the classical and romantic modes of thought.

He had initially studied science, but quickly became disillusioned with its focus on rationality and its veneer of self-certainty. 

Phaedrus realized that behind each explanation there can always be an infinite number of other possible explanations. Thus he began to search for other ways of understanding the world.

Yet as he started challenging and questioning existing systems of thought, he also began to exhibit increasingly antisocial behavior and even signs of mental illness.

Phaedrus was then placed in a mental health institution and prescribed electroshock therapy to "cure" him of his illness. 

When he awoke many months later, the narrator remembered little of Phaedrus and his thoughts. He left his teaching position at the university, and he, his wife and son moved away.

According to the narrator, while some people might judge an experience such as Phaedrus's mental "break" as an illness that needs to be cured, other cultures might see it differently, as a profound moment or period of enlightenment. 

The narrator is torn between fighting Phaedrus's re-emerging memories and embracing them. The contrast between his views on life and Phaedrus's perspective creates a tension that builds until the end of the story. 

While the narrator seems bound to his classical mind, it soon becomes clear that Phaedrus had the ability to combine both classical and romantic ways of thinking.

### 5. “Quality” is a theoretical solution to reconcile the divide between the classical and romantic. 

While studying philosophy, Phaedrus began to develop his own philosophy, which both caused and would ultimately cure his "madness."

Phaedrus believed that the dichotomy of classical versus romantic, so central to Western thought, is responsible for the dissatisfaction, confusion and lack of wisdom in modern society.

When John Sutherland couldn't get the engine on his motorcycle to start, it upset him profoundly as it was an intrusion on his reality. It reminded him of the ubiquitous presence of technology, a force from which his romantic mind had tried so hard to escape. 

Phaedrus believed that if we are to live in a way that promotes well-being and wisdom, we must find a way to mollify the antagonism between the classic and the romantic with the concept of quality.

Quality is a philosophical approach that includes both the classical and romantic modes of thought, by integrating the romantic side into the canon of rationalism.

In his musings, Phaedrus describes the ways in which people consciously and unconsciously select from millions of possible stimuli the specific things upon which we focus attention. 

Those classically minded tend to acknowledge their perceptions of the world and then classify and divide them based on individual characteristics — thus creating order out of chaos. 

Romantics in contrast tend to admire and exalt the chaos and richness of life's experiences. 

Quality rejects neither approach outright but instead incorporates both by reflecting upon the vastness of the original pool of stimuli, from which we assemble our versions of "reality" and "truth."

### 6. Living according to the principle of quality is not an easy task; both grief and happiness are the result. 

Although the philosophy of quality is intended to reconcile the classical and the romantic modes of thought, this was unquestionably a struggle for the narrator. 

Despite Phaedrus's criticisms of rationality, set definitions and blind fanaticism, he nonetheless employs all these elements in his criticisms of himself. 

For instance, when the university where he was teaching came under the influence of right-wing state politics, Phaedrus felt that academic integrity and freedom were being compromised and pushed to have the university's accreditation revoked in protest. 

In justifying his actions, Phaedrus compares a university to a church. He says that the people running an organization like a church must abide by the standards that have been established by the "state of mind" of that organization. 

Essentially, a church, like a university, is not the property it sits on or the jobs it provides, but instead the spirit and energy of the ideas behind it. 

In arriving at this epiphany, Phaedrus berates himself for having so long pursued only the rational, despite knowing that rational explanations cannot provide the only path to understanding. He realized then that he had been blind to the "romantic," or the spiritual, side of life.

The attempt to reconcile classical and romantic thought — or striving for "quality" and balance in one's life — led to personal strife and eventually "madness" for Phaedrus. 

This collapse caused pain for his family as well. The narrator worries throughout the road trip that Chris too is showing signs of compromised mental health — complaining about psychosomatic stomach aches and exhibiting dramatic mood swings — and the narrator blames himself. 

Although Phaedrus's crisis forced the narrator to abandon teaching and move his family to another town, he explains that his son still misses Phaedrus and all that he represented.

Even though Phaedrus was the cause of both grief and happiness, at the end of the story, the narrator is able to embrace his old identity, and father and son ride away on their motorcycle, happy.

### 7. Final summary 

The key message in this book:

**While the divide between science and the humanities may seem vast, that doesn't mean the two aren't reconcilable. In fact, bridging this divide is absolutely necessary if we want to better understand the complexities of the human condition.**

Actionable advice:

**Expand your horizons.**

If you are a "humanities person" who doesn't know the first thing about how basic things work — like your sink's plumbing or your car's motor — push yourself to learn about the mechanics behind everyday technologies. If you're more scientifically minded, however, and less engaged with the humanities, consider reading a poem or two or keeping a journal of self-reflection. 

**Suggested further reading:** ** _The Art of War_** **by Sun Tzu**

Thousands of years old, _The Art of War_ is a Chinese military treatise that is considered the definitive work of military tactics and strategy. It has greatly influenced military tactics as well as business and legal strategy in both the East and West. Leaders such as general Douglas MacArthur and Mao Zedong have drawn inspiration from it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Robert Pirsig

Robert Pirsig is an American writer and philosopher, called "probably the most-widely read philosopher alive" by British newspaper _The Guardian_. His seminal work, _Zen and the Art of Motorcycle Maintenance_, is based largely on real events in Pirsig's life.

