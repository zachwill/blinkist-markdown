---
id: 5984e667b238e1000629007d
slug: zen-mind-beginners-mind-en
published_date: 2017-08-07T00:00:00.000+00:00
author: Shunryu Suzuki
title: Zen Mind, Beginner's Mind
subtitle: Informal Talks on Zen Meditation and Practice
main_color: E3312D
text_color: C92C28
---

# Zen Mind, Beginner's Mind

_Informal Talks on Zen Meditation and Practice_

**Shunryu Suzuki**

_Zen Mind, Beginner's Mind_ (1970) is a starter's guide to Zen Buddhism. These blinks explain how Zen is not only a system of meditation, but also a philosophy of life. They describe how to sit, breathe and observe while maintaining a vital connection to the present moment.

---
### 1. What’s in it for me? Abandon ambition. 

Do you feel agitated, stressed, or generally anxious in life? In the modern world, it's easy to get trapped in ruts that feel impossible to escape. We face a constant slew of work emails and obligations, social events and relationships, fitness and weight goals. We juggle all of these and try to be perfect high achievers.

But what if it doesn't have to be this way? What if society's priorities are all wrong, and the goal of achieving a certain social status, job title or income bracket is misguided? What if you could bring your focus back to the activities that make up your day and find peace and contentment from them, with no motive beyond that?

In these blinks, you'll learn

  * the wisdom of nonduality;

  * the idea behind pure activity; and

  * why a bad horse might be better than a good horse.

### 2. The Zen meditation posture is a practice unto itself but also has a deeper symbolic meaning. 

Lots of modern health gurus promote a particular body posture as a way to feel more confident and happy. Interestingly enough, so does the ancient practice of Zen Buddhism.

The posture Zen practitioners adopt when meditating promotes spirituality in and of itself. Not just that, but taking a certain posture actually constitutes the entire practice, as it enables your mind to automatically tune into the spiritual realm.

As a result, the sole aim of the practice is to sit in this position. So, what exactly is it?

Sit cross-legged, preferably in the lotus position, with your right foot on your left thigh and your left foot on your right thigh. Your spine should be straight, with your chin pressed gently down.

The center of your body, just below your belly button, should also be aiming down, directly toward the floor. This last shift will help with stability.

But this isn't a purely pragmatic technique. The lotus meditation posture also has a symbolic meaning that relates to the relationship between life and death.

More specifically, it's a symbolic expression of _non-duality_, the notion that all things and beings on earth have the same essence. While people tend to think of themselves as having two legs, those two legs "become one" in lotus, when it's no longer as apparent which leg is right and which is left.

This concept of nonduality is essential as it applies to every aspect of the Zen worldview. Therefore, "life" and "death" don't exist. Rather, life ends while remaining eternal. You die, yet you do not die. The body and mind dissolve but remain.

Such seeming contradictions are unified by a single, harmonious idea, which is the essence of Zen.

> _"To take this posture is itself the right state of mind."_

### 3. The Zen practice of breathing makes us aware of our true nature while dissolving time and space. 

Most people don't notice their hearts beating, yet it's tremendously beneficial to observe such basic life processes. That's where Zen breathing cultivates an awareness of our true nature.

This practice focuses simply on bringing attention to the breath. It involves following the air as it is inhaled into our bodies and then exhaled back into the world. By noticing this process, we see that the world is one unified whole, free of boundaries.

The throat is a door that can swing open, allowing air to pass through. As a result, the inner world of the body and the outer world are one, connected by the flow of breath. This practice teaches us to let go of duality and the idea that there is an "I" and an "other." In Zen, all that exists is the movement of the breath, which comprises our _Buddha_ or _true_ nature.

This is already profound, but Zen breathing goes further, dissolving the feeling of time and space. After all, when the world disappears, along with _ego_ — the ideas and memories we hold of ourselves — time and space cease to exist. There's no longer a specific hour on a clock, and there's no specific room in which we're sitting.

While we might believe there is a specific errand we must do this afternoon, the very idea of "this afternoon" is merely an arbitrary concept. In fact, we will run this errand after doing or not doing some other things that simply happen one after the other. In this way, just like our breath, the minutes slip by without any differentiation between them.

All we experience is the _in breath_ and the _out breath_, over and over.

> _"Whatever it is, we should do it, even if it is not doing something. We should live in this moment."_

### 4. In life as well as meditation, it’s better to observe than to control. 

Western society is dominated by control freaks, but control is overrated. Most people experience their best ideas when relaxed into a state of flow.

Instead of attempting to micromanage the world, step back and observe what happens without your interference. After all, disorder is beyond human control; it cannot be grasped.

Not just that, but life and the world are inherently disorderly and random, which makes any attempt at control futile. We might try to control other people, forcing them to do what we want, but attempts fail practically every time — all while wasting a great deal of effort.

In reality, the best way to get people to behave reasonably is to let them be naughty, crazy or free. Instead of attempting to guide their behavior to where you'd like it to be, simply watch and intervene only when they are a threat to themselves or others.

It's like raising sheep or cattle. If you give your herd a massive field to live in, they're much more likely to exist in peace and contentment. If you confine them to a packed pen, they'll be tempted to jump the fence.

So, in life, control holds us back, and the same is true in meditation. Oftentimes, when we meditate, we attempt to control our thoughts and prevent them from existing, which doesn't work at all.

Instead, you should wisely allow thoughts to come and go, observing them as they do so. Remember, your efforts to deter thought are not appropriate for Zen meditation. The only proper effort here is to return the mind and concentration to your breathing.

You might think observing is the easiest thing in the world, but next up we'll learn how tricky meditation practice can be.

### 5. The adversity you encounter in meditation feeds your practice. 

While meditating, it's not uncommon for people to grow tired, emotional and discouraged. But actually, all of these responses are good.

That's because encountering adversity in meditation helps you grow. Just think of the mind as if it were a garden full of both flowers and weeds. The weeds naturally appear unsightly, but if you pull them up and bury them near the flowers, the soil will be enriched.

Or, to put it another way, trying to wake up every day at six in the morning to meditate won't be easy. You'll struggle to pull yourself out of bed, have a hard time getting into the proper position and then will have yet a harder time sitting with a straight spine.

However, all of these struggles are merely thoughts in your mind. Like waves in the sea, they arise and recede over and over. Every time you overcome such a wave, it feeds your practice and, over time, the intensity of the waves diminishes. By using this adversity to nourish your practice, you can make progress quite quickly.

That being said, meditating does require some effort. The human mind is incredibly active, and it's essential that you make the correct effort to calm it. As you already know, this means simply concentrating on your breath rather than on attempting to calm your mind.

You should prepare yourself to continue focusing on your breath forever, without the expectation of sudden relief. As time passes, you will find your effort becoming more precise and less strained.

This is just a taste of how Zen's approach to effort and success differs from the conventional Western conceptions of these words. In the next blink, you'll learn more about this difference.

### 6. In Zen practice, excellence is not the aim, and the worst students are often the best. 

The Western world values people who succeed, especially if they do so effortlessly, like musical and mathematical geniuses. But Zen has a different concept of success.

In Zen, excellence is not the goal, but rather patient perseverance. Just take an example from the Zen scriptures. The Samyuktagama Sutra speaks of very good horses and bad horses. The good horses obey the will of their riders with almost telepathic obedience, while the bad horses must be whipped into following orders.

Most people automatically want to be the good horse, but in Zen, this is not the aim. The intention of Zen is simply to practice without worrying about how difficult or effortless that practice is.

As a result, in Zen, the worst students are often the best. Since these students must overcome much greater adversity, they often undergo intensive practice, cultivating skilled, disciplined minds.

For instance, calligraphers with tremendous natural talent start off as great successes. But later, they may reach a point where their progress plateaus. To advance from this place they would need to invest a great deal of effort, but since they have not practiced doing so, they often just give up.

On the other hand, calligraphers who struggle initially learn to deal with and overcome such difficulty. When they encounter challenges, they are not so easily dissuaded. The same goes for Zen. This practice teaches that failure can be success and vice-versa.

But that's not the only lesson Zen has for Western society. We'll explore further teachings in the next blink.

### 7. Practicing Zen isn’t about excitement or achievement. 

Nowadays, most people are more or less addicted to information and entertainment that makes them feel some sense of excitement. The practice of Zen offers the opposite of such a lifestyle.

It probably doesn't come as a shock at this point, but practicing Zen isn't about excitement. It's not about big parties, gallery openings, blockbuster movies or dinner dates. Though it doesn't entirely exclude such activities, Zen is in many ways opposed to them. It's about bringing focus to our daily routines and the actions we normally perform on autopilot, like eating, cleaning, working and talking.

In so doing, Zen endeavors to maintain a feeling of quiet and happiness in the mind, rather than one of excitement and stimulation. This can be tricky in the modern world, where professional and social lives often demand a great deal of engagement and responsibility.

But we don't have to give in to this excitement. Rather, we can take part in every aspect of life while maintaining a calm mind.

And simply getting excited about Zen and nothing else isn't a solution either. This is a situation many enthusiastic young Zen students find themselves in, making them ready to drop everything and spend the rest of their lives meditating on a mountain. But a much better — and more challenging — path is to remain rooted in a normal life, faithfully practicing a daily hour of meditation.

The practice of Zen isn't geared toward reaching a result. This is hard for us to grasp. When we engage in an action, the goal is to achieve something else, like fame, recognition or money.

But for Zen, the intention is to do things without seeking any extra achievement. The activity in question can be absolutely anything, from cleaning to cooking, making art or meditating. As long as the aim is to get rid of everything that isn't purely the action in which you are engaged.

If you have practiced meditation and feel proud of doing so, you should let go of this pride. Simultaneously, any disappointment should be set aside. Only the activity itself is of importance.

### 8. Zen entails discovering pure activity, which is, at its core, an act of giving. 

Have you ever taken a long walk to relax, only to find that you were lost in anxious thoughts the whole time?

It's just one example of how frustrating it can be when thinking prevents you from living. Zen is a great tool for overcoming this dilemma. That's because Zen is a practice of pure activity.

In other words, the goal of Zen is to purely engage in an activity without attaching thoughts to it. This is key since, when people become attached to the product their activity will produce — for example, a book they've written — they begin to judge it. They think it was good, or bad, and feel the emotions associated with those thoughts.

These thoughts detract from the purity of any given activity. Each time you worry about what other people might think of your work, or whether the task you're undertaking will be successful, you drift away from the purity of what you're actually doing.

For this reason, the aim of Zen is to be entirely absorbed by the thing you're doing in the moment. As you complete this activity, and begin the next, not even a trace of the previous undertaking should remain in your mind.

Zen acknowledges that true activity is a gesture of giving. After all, we are not separate from the divine, and whatever we believe ourselves to create is actually the product of a larger divine consciousness. By realizing this, we can come to understand how little it matters that our creations are recognized, applauded, or even have material value.

When we engage in an activity without such preoccupations, all our activity becomes an act of generosity. It's just another of the many benefits of Zen. However, there's a catch: if you engage in this practice to achieve such a sense of generosity or any of the other things Zen can offer, the practice won't work.

Therein lies the mystery of Zen; it eludes simplicity and cannot be encapsulated into a simple statement of right or wrong.

### 9. Final summary 

The key message in this book:

**Zen meditation isn't about achieving something — not even happiness. Rather, it's simply bringing awareness to your breath and gradually learning to be fully present for every daily activity without any distractions.**

Actionable advice:

**Let go of your notions of good and bad.**

People tend to believe that there are good things and bad things, but this conception is very limiting. Whether something is good or bad isn't relevant. What _is_ relevant is being able to do an activity without passing a value judgment on it. This is the simple pursuit of peace.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Buddhism — Plain and Simple_** **by Steve Hagen**

_Buddhism Plain and Simple_ (2013) is your no-nonsense guide to essential Buddhist practices. From building awareness to living in the present moment, Buddhism's most important teachings are explained in a clear and accessible way, and are linked to aspects of everyday life where we need them the most.
---

### Shunryu Suzuki

Shunryu Suzuki was a Japanese monk who came to the United States in 1954. In addition to founding the San Francisco Zen Center, he taught countless Zen students and wrote several books on the subject, including _Branching Streams Flow in the Darkness_.

