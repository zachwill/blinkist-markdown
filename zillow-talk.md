---
id: 555b367661353300077c0000
slug: zillow-talk-en
published_date: 2015-05-20T00:00:00.000+00:00
author: Spencer Rascoff and Stan Humphries
title: Zillow Talk
subtitle: The New Rules of Real Estate
main_color: 4D5863
text_color: 4D5863
---

# Zillow Talk

_The New Rules of Real Estate_

**Spencer Rascoff and Stan Humphries**

_Zillow Talk: The New Rules of Real Estate_ (2015) gives the reader all the tools they need to buy, sell or rent a home. From the conundrum of whether to rent or buy, to when to sell and how to boost the value of your property, these blinks shed light on the perennially important dilemmas of real estate — the biggest investment of your life.

---
### 1. What’s in it for me? Discover all you need to know about buying or renting a house. 

You've just had your first child. Your two-room apartment is overflowing with baby stuff and suddenly feels a little too small. Maybe it's time to buy a house? However, as we all know, buying a house is a huge investment, and there are many questions that need to be answered before you take the step: Can you afford it? What's the best area to buy in? Should you give it a luxury makeover?

You probably have a lot of preconceived ideas and assumptions about buying a house. You might think that just because you're single and living in the city, buying a house is not an option. You might think it doesn't matter what time of year you buy a house. You might even think that buying a house is always a good investment. As these blinks will show, all of these assumptions are wrong.

In these blinks you'll learn

  * where you should buy;

  * why that new fancy bathtub you just installed won't make your house more valuable; and

  * how a single word can have a huge impact on a house sale.

### 2. Determine whether buying is right for you and choose an up-and-coming neighborhood to maximize your investment. 

Most of us associate homeownership with a family. But buying a home can be as advantageous for a single working woman as it can be for a family of four.

How can you know if buying is right for _you_? It could be because buying saves you money.

Zillow developed a tool called the _break even horizon_, which can help you determine whether it makes more financial sense to buy or rent.

Here's how it works.

The break even horizon compares costs over time to determine how long it will take for the home you purchase to cost less than it would to rent the same property. It does this by calculating inflation and tax rates as well as property value.

If you use Zillow's break even horizon tool and find out that you could actually save money by buying a house, you're ready for the next step: deciding _where_ to buy.

When choosing a neighborhood, it's important to consider how it will transform in the years to come. After all, buying property is a huge investment and real estate prices are directly linked to location.

So, while it may be tempting to buy the best property in the most desirable area, a wiser financial decision is to buy property in up-and-coming neighborhoods.

Luckily, by following market trends you can determine how a neighborhood will change over time.

For instance, a part of the city with bad roads or little commercial activity can become a booming center of commerce in just a few years.

How?

Say you knew that a developer planned to build a shopping mall in the area by next year. A major commercial attraction like that would likely draw a new group of people to the area and start transforming the run-down neighborhood into a hip and attractive one.

As the atmosphere of the neighborhood improves so will the housing prices, thereby making your investment a sound one.

### 3. Instead of buying the worst house in the best neighborhood, buy the worst house in the hottest neighborhood. 

Has anyone ever told you that buying the most run-down house in an expensive neighborhood is a good investment? Well, they're wrong and you'd be surprised to hear just how disastrous that decision can be.

Think of it this way, when you buy the worst house in a wealthy neighborhood you are also buying the house with the least potential to rise in value.

You might think that the surrounding properties will increase the value of your own. But in fact, being the cheapest house in the area will only arouse the suspicions of prospective buyers.

Why?

They'll wonder why the house is so much cheaper than those around it and assume something is amiss.

Take the Marcus family. They bought a small bungalow in the lovely Fort Worth, Texas suburb of Eagle Ranch that was valued in the bottom ten percent for the area. While every other house increased in value, the Marcuses' house lagged behind by four percentage points.

On the other hand, buying the worst house in the _hottest_ neighborhood can be a great investment and the key is to find gentrifying neighborhoods.

What are they?

A gentrifying neighborhood is one that used to be run down but is turning around as young people and artists move in. As the cultural climate changes, more wealthy people are attracted to these neighborhoods and property values skyrocket as renovations take place.

For instance, at the turn of the twentieth century, Manchester was home to many wealthy steel barons but fell into disrepair as they left for the suburbs. However, since 2008 the inner city has been experiencing a revival as young people move into the cheap properties, thereby sparking gentrification that sends property values soaring.

So the next time you're looking to buy property, remember that the future of the location is key when you're picking a prime area for investment.

### 4. Before buying a home, consider whether owning is right for you. 

Do you remember in 2002 when President George W. Bush took measures to increase homeownership among low-income Americans? He declared that owning a home was a crucial part of the American Dream and should be accessible to everyone. Unfortunately, for some this dream turned into a nightmare.

That's because home ownership isn't for everyone, and here's why.

For starters, government incentives for banks to issue mortgages to low-income families end up hurting those they are meant to help.

Take the example of Atlanta police officer Darrin West. West bought his $130,000 townhouse using a $20,000 government-backed loan as down payment. In 2008, when the housing bubble burst, Mr. West and countless other Americans with government-backed mortgages could no longer afford their payments. They defaulted on their loans and their homes now belong to banks.

Another reason homeownership isn't for everyone is that it can be much more difficult to handle unforeseen circumstances when you own a home.

Consider this, when you own a home somewhere you have an incentive to stay there. In order to do so you've got to keep up with your mortgage payments, often for decades. What happens if all of a sudden you have a major unforeseen expense or need to move?

Say one of your children fell ill and you got slammed with a $10,000 bill. How could you pay it when all of your assets are tied up in your property?

Or what if your company changes locations and you have to move to keep your job? You might have to choose between your job and your home.

For these reasons renting can be a good choice for those who need the flexibility to move easily when finances demand it.

Don't assume that owning a home is necessary to leading a happy life. For some people it's not even desirable.

### 5. Don’t assume that fancy renovations will increase the value of your property. 

Most of us take it for granted that high-end remodeling will boost the value of our homes. Making things fancy increases their value, right?

The truth is, high-end improvements return less on their investment than mid-range remodelling.

Why?

Because of the law of diminishing returns: at a certain point your investment returns less.

While upscale remodelling is about style and aesthetic, mid-range options tend to be more functional. And functionality sells.

Often when prospective buyers view a property the first question they ask is "does it have all the essentials?"

Therefore, a pragmatically remodeled house is more marketable than one with luxury facilities.

Consider a bathroom that's in terrible shape, the mirror is cracked, the sink drips and the toilet won't flush. A mid-range renovation would significantly increase the desirability of the space by making it more usable. But going the whole nine yards with the renovation wouldn't change the functionality of the space and therefore wouldn't make it much more valuable.

So basic renovations are a better investment than luxury ones, but should you even remodel at all?

Before you decide it's important to remember that the value of your renovations will decrease over time.

For instance, some facilities decrease in value as technical progress is made or as new things come into style.

If you do decide to remodel you should factor in the value lost to depreciation.

Say, for example, you do a total remodel of your home and it increases the property value by $24,310. In one year's time that increase would drop to only $22,000, meaning that your investment has depreciated by $2,310 in just one year!

So the next time you remodel make sure to go with the mid-range option to ensure the maximum return from your investment.

### 6. Get the most for your property by listing it at the right time and describing it in detail. 

Selling your house isn't as simple as listing it and waiting for the offers to come rushing in. Selling property requires strategy and the key is to know _when_ to sell.

In fact, certain seasons are better for selling real estate than others.

In many places summer is the best time to sell your house. This makes sense, because who wants to move in the cold of winter?

But the weather isn't the only reason summer is a good time to sell.

Families with children are more likely to buy a new home during the summer because they won't want to have to take their kids out of school when they move.

So don't lose track of time, take advantage of the prime selling season by listing your property early in the year.

Just consider the fact that in 2011 and 2012 homes put on the market at the end of March sold faster and for over two percent more than the average for houses listed during the rest of the year.

But listing your house at the right time won't ensure you get a good price for it. The way you _describe_ your property is also essential.

Consider the description "modern". You might think that a modern home is one with all the newest improvements, but the word actually refers to houses built in the 1950s and 1960s, a drawback for many buyers.

Other adjectives like "cosy", "cute", or "charming" have different meanings for different people — in short, they're not concrete enough.

No matter how you describe your house, keep in mind that homes listed with longer descriptions tend to fetch more because buyers are more likely to view houses that are described in detail.

So the next time you list your home make sure to do it in the prime season and don't spare any details when describing it.

### 7. Final summary 

The key message in this book:

**If you decide to invest in a home, you can boost its value by scouting out property in up-and-coming neighborhoods, choosing mid-range options when you remodel, and, when you're selling, list it for sale at the right time of the year to get the best price.**

Actionable advice:

**Consider the school district.**

One factor which is often overlooked when buying a home is whether the property is located in an area with good schools. Even if you don't have kids, make sure you buy a property close to a popular school. This will make it an attractive option for families, increasing its value.

**Suggested** **further** **reading:** ** _The Education of a Value Investor_** **by Guy Spier**

In his bestselling book, Guy Spier recounts his transformation from greedy hedge-fund manager on Wall Street to a successful _value investor_. Sharing the incredible story of his career and the wisdom he acquired along the way, Spier has some surprising insights concerning, what he sees as a false choice between leading an ethical life and a financially successful one. With great admiration, Spier also names the people who were most influential to his professional life, explaining the specific effect each of them had on his mindset and career.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Spencer Rascoff and Stan Humphries

Spencer Rascoff co-founded the discount travel website Hotwire. After selling his start-up to Expedia he helped start the real-estate search engine Zillow in 2005. Stan Humphries is chief economist at Zillow and a frequent commentator on housing trends for CNBC, Bloomberg TV and Fox Business News.

