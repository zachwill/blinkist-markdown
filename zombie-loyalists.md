---
id: 56c06a5a4f33cc0007000079
slug: zombie-loyalists-en
published_date: 2016-02-19T00:00:00.000+00:00
author: Peter Shankman
title: Zombie Loyalists
subtitle: Using Great Customer Service To Create Rabid Fans
main_color: 7A7040
text_color: 7A7040
---

# Zombie Loyalists

_Using Great Customer Service To Create Rabid Fans_

**Peter Shankman**

_Zombie Loyalists_ (2015) gives you the inside scoop on the customers that every company dreams of attracting. These blinks, illustrated with insightful stories from a wide range of businesses, provide you with simple steps that your brand can take to establish, grow and maintain a faithful, enthusiastic customer base.

---
### 1. What’s in it for me? Cultivate unwaveringly loyal customers. 

Customer service? Ah, yes: it's pretty important. In fact, it's downright essential, right? One thing's for sure: if you tell your customers that they're wasting your time or give them the impression that all you want is their money, you can be pretty sure that they won't be your customers for long.

So, maybe it actually is worth spending a little time on getting (more) acquainted with the concept of customer service. After all, it's one of those key components that'll keep your customers from going over to the competition.

These blinks aren't only about how to provide solid customer service, however. They'll actually show you how to turn your customers into loyal fans, people who'll grow your company for you by spreading word of its awesomeness!

In these blinks, you'll learn

  * why one San Franciscan chose a particular bodega;

  * how to make your customers feel valued; and

  * what to do if you let a customer down.

### 2. Nothing should stand between your company and loyal customers. 

Say hello to some zombies you'd like to meet — Zombie Loyalists. While regular zombies will do anything to get their next feed, Zombie Loyalists will do anything to show their support for a company. They'll tell their friends about your brand, and they'd never dream of buying from one of your competitors. These customers are programmed to love you.

But how do you get them?

Breeding Zombie Loyalists comes down to one thing: _great customer service_. No matter how much prestige your brand has or how fresh your products are, people won't become passionate customers unless your company provides them with fantastic experiences. And that's not really so difficult to do. Nonetheless, most companies have a few things hindering their customer service, such as convoluted communications processes or a less-than-welcoming company culture.   

  

If you want your employees to give your customers something special, start by boosting internal communication. No customer wants to spend hours in line or on the phone waiting for a solution to some problem. Customer support must be made efficient, by ensuring all departments can connect and collaborate with each other. 

The next step is to nudge your employees in the right direction. Let them know that their suggestions for improving customer experience are welcome. For instance, the author once worked in a yoghurt shop in New York. One day he recognized the dirty brass poles holding the awning above the entrance and went out to clean them because it would attract more customers. The owner asked him why he wasn't behind the counter and told him not to think too much. 

You'd be surprised at how many workers don't point out problems they've noticed, simply because they feel it's not their responsibility. But by encouraging your employees to take initiative and test out different solutions, they'll be more than happy to go out of their way to make customer experience that much better.

> _"If your company puts rules before people and inflexibility before logic, you have a problem."_

### 3. Infect your very first Zombie Loyalist with little acts of kindness. 

Every army started with just one soldier. This will be true of your Zombie Loyalist army, too. Your very first supporter will make the rest of your journey possible. This doesn't mean that finding your first Zombie Loyalist takes lots of time and effort, though. On the contrary, there are a few simple strategies you can use to transform the average customer into your number-one fan!

The first thing you can do is to leverage the power of surprise. Get your employees to perform some random acts of kindness and watch the effect it has on your customers. It's this kind of human attention that will compel customers to come _back_ to your store!

One shopper in San Francisco told the author how she always went to one particular bodega. Why? Well, one day she was wearing headphones and the guy behind the counter asked her what she was listening to. She told him it was the artist T.I., and so, for a week, he sang "You can have whatever you like!" every time she walked in. 

Of course, it's not just employees that get to have all the fun! As the owner, it's vital that you give customers these special moments, too. This will set you apart from other company leaders and bring you that much closer to your customers. 

This is what the owner of The Red Stag, in Halifax, uses his Twitter account for. He tweets before driving to work, offering to pick up coffee for the team, or when he accidentally makes an extra burger, offering it to anyone who missed lunch. This is a fun, friendly way of creating new opportunities to get to know customers one-on-one, and they love him for it!

When starting up your army of Zombie Loyalists, it's the little things that count. But when you really want to grow your army, it helps to go that extra mile.

### 4. Create an army of Zombie Loyalists with thoughtful favors and genuine care. 

With your first Zombies on board, it's time to up the ante. Zombie Loyalists love nothing more than infecting other customers! All you need to do is give them a reason to do it. So now that your employees are well versed in providing small surprises, start rewarding a few customers with special favors. 

Our society has known a lot of terrible customer service in its time, so a little extra thought goes a long way. One couple, for instance, booked a couple's massage at a spa in Maryland and were asked by the receptionist if it was for a special occasion. They told him it was their anniversary, and didn't give it a second thought. 

But when they arrived at the spa, the couple was greeted with a chocolate cake that had _Happy Anniversary_ written across it. The couple became Zombie Loyalists from that point on, as did many of their friends after hearing the story. 

As well as doing favors, you should show customers that their problems matter to you. Most people who're urgently seeking a solution to some problem are already up to their ears in stress. The last thing they need is someone who won't give them the time of day. If your employees try their best to provide simple and speedy solutions, you'll be rewarded with genuinely grateful customers. 

Take the story of Harlan, for example. He was invited to an awards gala and so had packed a blue pinstripe to wear. To his horror, he realized right before the event that he was required to wear black. Harlan desperately started calling tuxedo shops. 

One of them was about to close, but, having listened to Harlan's problem, they invited him to drop by. When Harlan arrived, they had a tailor ready for him. Just 30 minutes later, he had the perfect black suit. He became a Zombie Loyalist and told his story to the 1,000 guests at the event during his speech! Word-of-mouth advertising doesn't get much better than that.

> _"Building armies of Zombie Loyalists is not only doable but easier than you think."_

### 5. Social media is the perfect tool for growing your Zombie Loyalist crowd. 

What would you trust more — a glossy poster announcing that a new cocktail bar is the best in town, or the story your friend tells you about the great talk he had with the bartender over an amazing Tequila Sunrise. It goes without saying that customers trust their network more than they trust a brand's marketing. So how can you get your customers to spread the word?

It all starts with good manners. Saying _thank you_ to existing customers is a surefire way of encouraging them to recommend you. Many companies fear social media today, believing it to be the breeding ground of bad reviews and copious complaints. 

But don't forget that platforms like Facebook, Twitter and Instagram are also for sharing positive experiences! If your customers give you a shout-out online, show them that you appreciate it. Next time someone tweets that they had a great meal at your restaurant, for example, be sure to respond with a positive response of your own — maybe even offer the customer and their friends a free drink! 

Showing your appreciation turns your brand into a place where customers feel they belong. You're likely to start seeing them around more and more, and probably with friends, too! 

You can also get your customers sharing their experiences online by giving them _proof_ to pad out their posts. Sharing a happy status update is one thing; having a photo to go with it is even better! Try adding a small note or gift to your customer's bill — anything that they might photograph and share on social media.

For example, one man took his highly pregnant wife out for dinner at the local Red Robin. When they got the bill, there was a small note wishing them luck with the birth. The family was so charmed that they posted the bill on Reddit and it ended up on the consumer website Consumerist.

> _"Remember, everything is more believable when it comes from a trusted source."_

### 6. Show Zombie Loyalists you can learn from your mistakes and they’ll be forgiving. 

Regular zombies are brainless; Zombie Loyalists aren't. If your company stops caring for them, they won't hang around just for the heck of it! And remember: though gaining Zombie Loyalists will lead your company to great things, losing them will drag you down. 

This doesn't mean that you should fear or — even worse — cover up mistakes. As long as you acknowledge that you messed up and are trying to improve, your Zombie Loyalist will forgive you. 

Say you take the bus to work every day. Over the past several months, you've noticed service has declined. The bus is often late, and sometimes doesn't arrive at all. One day, you see the district manager on the bus, and so you let him know about the problems. What happens? He makes a note of it and, two days later, your bus shows up on time with a new driver to welcome you. Now that's how to handle a mistake!

But if, for whatever reason, your company fails to show improvement the first time around, don't fret! If you do your best to make up for it, you might have a second chance. Take the story of Zombie Loyalist Dennis, for example. He was a regular patron of the restaurant O'Hara's. But, after informing the managers of problems several times to no avail, he told the staff he wouldn't be coming back. 

Three months later, he got a message telling him that there had been changes and that he was invited to drop by once more. Dennis went, and the owner thanked him personally. Everything was perfect, and Dennis became a loyal customer again.

> _**"** A scorned Zombie Loyalist has the power to take all of his or her infected zombies somewhere else."_

### 7. Final summary 

The key message in this book:

**It's not too hard to create loyal fans. Start with some small acts of kindness and some favors to get your customers spreading the word. Encourage your employees to make an extra effort and create unique, surprising and special customer experiences. Finally, treat your customers with the honesty and respect they deserve. Soon, you'll have some Zombie Loyalists who'll bring nothing but good for your brand!**

Actionable advice:

**Track service quality.**

It's fine to show disappointed Zombie Loyalists that you can do better, but ideally you'll have already shown them that you consistently do a top-notch job. To ensure this, be sure to monitor and track the quality of your service; let managers test it once a month and ask customers to give feedback. Take all this information seriously and get to work on any problems that have emerged! 

**Suggested further reading:** ** _Hug Your Customers_** **by Jack Mitchell**

**_Hug Your Customers_ (2003) is based on the author's five decades of experience in crafting the perfect customer-centered business. "Hugging" your customers is about catering to their every need and organizing your entire company around them. Establishing a hugging culture is the most effective way to achieve financial success and keep your customers happy.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Peter Shankman

Peter Shankman is an American entrepreneur and consultant for customer service, marketing, PR and social media. He is the author of four books, including _Can We Do That?!: Outrageous PR Stunts That Work — And Why Your Company Needs Them_.

