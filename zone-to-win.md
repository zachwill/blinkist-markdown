---
id: 580bb680d4d2ce0003dd24fe
slug: zone-to-win-en
published_date: 2016-10-28T00:00:00.000+00:00
author: Geoffrey A. Moore
title: Zone To Win
subtitle: Organizing to Compete in an Age of Disruption
main_color: D04B2C
text_color: D04B2C
---

# Zone To Win

_Organizing to Compete in an Age of Disruption_

**Geoffrey A. Moore**

_Zone to Win_ (2015) is the ultimate guide to organizing your company to compete effectively in an age of disruption. These blinks will show you how to keep up with constant market innovation while creating lucrative disruptions of your own.

---
### 1. What’s in it for me? Discover how the four zones of management can help you battle disruption. 

Not so long ago, the idea of a handheld device with the power of a supercomputer seemed the stuff of science fiction. But in 2007, Apple released the iPhone and the market for mobile devices was disrupted in a major fashion. Leaders like Nokia crumbled; others like Samsung soldiered on.

So in an age of constant innovation and market disruption, what's the secret formula to keep your company ahead of the pack?

These blinks outline a revolutionary system called four zones of management. When you implement this system, you'll not only successfully weather competitive disruptions but also learn to disrupt your market and dominate your rivals.

In these blinks, you'll learn

  * how to keep a product relevant after a market disruption;

  * when it's good to focus on innovation (and when it's not); and

  * how Microsoft got innovative to stay afloat in the mobile market.

### 2. Companies need to innovate to keep growing, yet such a disruption necessarily has a ripple effect. 

Any surfer knows that you've got to catch a wave at just the right moment. The same holds for companies. To grow, they need to catch the next wave of innovation before a competitor does.

In fact, the best way for a company to grow rapidly is to introduce a new product or service that turns an industry on its head: consider the innovations of online marketing, cloud computing or electric vehicles.

Being an innovative market leader can be extremely profitable, often yielding 20 percent revenue growth over the first five to seven years after a new product or service is introduced.

On the other hand, if you miss the wave, there's no way to catch it later. You're best off searching for another swell later on!

Companies that experience continuous growth are experts at catching innovation waves. For instance, Apple caught not one but three waves in the last ten years: digital music, smartphones and tablets. In doing so, the company completely transformed the market for mobile technology.

Disruptive innovations, however, can also throw whole industries off balance, as a disruption in one field can have far-reaching effects in others.

With the release of the iPhone, not only was the market for mobile phones turned upside down but also the commercial airline sector had to adjust to the new reality. Customers suddenly wanted to use their smartphones to book trips, download boarding passes and track flights.

Thus the airlines that could quickly build a strong mobile app had a huge advantage in the market. But this particular disruption didn't just require a simple infrastructure upgrade. The introduction of smartphones forced airlines to reconsider all their processes, from check-in to lounge services to boarding — requiring substantial investments that didn't immediately result in additional profits.

So disruptive changes certainly can be difficult for companies to handle. This is why it's essential to know how to manage disruption when it comes knocking at your company's door.

> _"What industry can possibly be exempt from massive reengineering now that every person is carrying a supercomputer?"_

### 3. Established companies can compete with disruptive start-ups by creating four zones of management. 

If you're dating in your 30s, you probably know it's not easy to compete with 20-somethings in the "dating market." Doing so requires fundamental changes to your dating strategy.

In the same vein, while established companies certainly _can_ compete with lean, disruptive start-ups, they first have to restructure their business models.

Doing so is a question of survival, as a savvy start-up can easily upend an existing enterprise.

Young companies are focused on a single goal — building a new product — while established companies have to balance maintaining a current business model and handling market disruptions simultaneously.

The field of journalism, for instance, has faced major disruptions with the advent of digital technology. Over the past 15 years, printed media outlets have had to grapple with the power of the internet and the rising use of smartphones. As a result, subscription numbers have plummeted, and many journalists have lost their jobs.

To compete effectively in a disruptive world, established companies need to thoughtfully restructure operations into _four zones_.

The first is the _performance zone_. Here is where employees who sell an existing product are located. In the automotive industry, for example, this zone would be populated by people who sell cars or who generate direct income for the company.

The second is the _productivity zone._ This zone encompasses all activity that doesn't directly generate revenue but provides necessary support for revenue generation. Producing cars, marketing the company brand, providing customer service and managing human resources are all found here.

The third is the _incubation zone._ This group is responsible for seeking out innovative solutions to boost the growth of the company. In the automotive industry, this zone might focus on developing new types of energy-efficient engines, for example.

And finally, the last zone is the _transformation zone._ This zone focuses on finding ways for the company to adapt to competitors' disruptive innovations. If a competitor is developing a highly efficient electric engine, for instance, this zone would form a competitive response.

Now you know the general framework for the four zones of management. So let's take a closer look at how each one works.

> _"A company like Kodak finds itself fighting not Fuji so much as digital photography."_

### 4. The performance zone should take a backseat to the incubation zone, except in times of disruption. 

As the adage goes, sometimes you have to take a step backward to move forward. Companies eager to grow and succeed need to accept the minor setbacks that may eventually lead to innovative solutions.

In fact, a company's revenue-generating activities — located in the performance zone — are often put under stress by the firm's efforts at innovation.

Maintaining two competing priorities can be distracting. On the one hand, a company wants to create new products, while on the other, it wants to maintain revenue and shareholder confidence.

At the end of the day, however, a company has to choose a priority and stick with it — and that choice should always be innovation. If an innovation disrupts a market segment successfully, the resulting revenue will easily compensate for any dip in performance during the years when the product was in development.

The situation changes when you're facing a period of disruption caused by _another_ company. Here you'll need to sacrifice innovation to keep your established customers loyal.

Companies can find themselves with new technology still in the pipeline when a competitor launches an innovative, disruptive product. In situations like this, instead of sticking with development, the smart company will sacrifice its incubating projects to remain competitive.

Let's say you own a car company, and your competitor launches an electric vehicle — an idea that your company also has been working on for some time.

While you've spent lots of money developing your project, you need to focus now on your existing business to protect market share.

But as you'll learn later, your transformation-zone team will help you adapt to the disruption, once your revenue stream is secured.

### 5. The productivity zone makes sure systems are efficient and programs effective amid a disruption. 

Imagine you're a talented home baker who wants to start a cake business. How do you go from baking one cake per day to baking and selling thousands of cakes per month?

The _productivity zone_ is where these ideas are born. This area focuses on driving efficiency and effectiveness to grow your business for success. Here's how it works.

A company starts by developing systems to improve its overall efficiency. In general, systems are standard procedures within a business that are fixed and universally applicable.

For instance, most companies have a set system when a new employee is hired. That individual is introduced to the company through a standard process, including things like signing contracts, meeting with human relations, reviewing company best practices and so on.

Companies use programs to improve effectiveness. In contrast to systems, programs are temporary and targeted. For example, when a new product is released, a group meets and determines how to market the product. Once a solution is reached, a program ends.

One such program that's beneficial for both efficiency and effectiveness is called the _end of life program._ This program handles situations in which companies reallocate resources from older revenue sources to newer, more profitable ventures. To make a switch, a company can periodically introduce an end of life program.

This is often triggered amid a disruption, in which certain products become uncompetitive. One good example of this is how the music industry gradually phased out CD production in the switch to digital music.

Amid a disruption, an end of life program also works to reallocate talent to new tasks, cut the older product from sales and, unfortunately, handle layoffs and shutdown expenses.

> _"Expiring products are like zombies. They need a special touch."_

### 6. The incubation zone helps you select disruptive products and bring them to market. 

The first rule of incubation is to ensure a future product meets certain standards that will guarantee a successful market disruption.

First off, your innovative product should represent a new product or business opportunity for your company. Enhancing or building on an existing product isn't enough to qualify it for incubation.

Your product should also aim to produce at least 10 percent more revenue than the existing product you plan to disrupt. For example, when Apple launched its iPhone, the company's goal would have been to earn at least 10 percent more revenue than the market leader at the time, Blackberry.

And finally, your product should have the potential to increase _total_ company revenue by at least 10 percent. If you find your projections fall short of that, your innovation isn't worth the risk.

By following these standards, your company can lower any given product's chances of market failure. More importantly, these guidelines will help you prove to skeptical shareholders that their money is being invested in innovative and worthwhile ventures.

So let's say your product meets all the above criteria. It then can move from incubation to development, and once developers have created and tested the product, management can build a release strategy.

Regarding your product release, your first step is to win over a brand ambassador for your innovation. Ideally, this individual is an intelligent, charismatic person who understands your product and will advocate its use to other influential early adopters.

Entrepreneur Sean Parker in 2004 was the first person to invest in Facebook, for instance. He also served as an ambassador for the social media platform, using his contacts to help transform the start-up into a multibillion-dollar company.

Once you've secured an ambassador, you need a strategy geared toward winning a dominant share of your expected market. To do so, you'll need to gain the trust of product advocates who will support your product as long as it fits their business needs.

### 7. The transformation zone comes alive during market disruptions, the ultimate test for a CEO. 

Military generals need to maintain constant discipline over troops, a control that is put to the test on the battlefield. Your company's transformation zone is only activated in similarly militant situations, such as when a competitor disrupts your market position with a new product or service.

Your transformation zone is your tool as a leader to deal with a crisis. After all, a market disruption is the true test of a company CEO.

During normal operations, a CEO handles day-to-day management; during a disruption, the leader's role transforms. Daily issues are passed off to the executive team, while the CEO focuses on dealing with the crisis.

So how does a CEO proceed? To handle a market disruption, a business leader must _neutralize, optimize_ and _differentiate_.

Let's start by examining how to neutralize a disruption. A CEO needs to act quickly, adopting the best features of the disruptive technology and adding them to the company's competing product.

When mobile ridesharing app Uber launched in San Francisco, it disrupted the operations of the established taxi industry. To neutralize the threat posed by Uber, several companies developed an app, enabling customers to use their smartphones to order and pay for a taxi, just like Uber.

Once you've neutralized the disruption, a CEO's next goal is to optimize the company's product. San Francisco cab companies might have, for example, implemented additional features to their app such as a way to rate the quality of drivers, which Uber also offers.

And finally, a CEO has to consider how to create a product or service that's truly competitive and disruptive on its own. Crucially, the product needs to be entirely different than that of your competitor!

Naturally, this goal leads a CEO and her team back to the incubation zone.

### 8. Put your four zones of management into place with forethought and careful planning. 

Are you the type of person who sorts documents, makes lists and generally keeps things under control? If so, as a CEO you'll excel at this next step, in which you put all you've learned in these blinks into action. 

First, divide your company into four zones. Every organization, initiative and company employee should be assigned to a specific zone and function, according to the rules of each zone.

But just because you work in a particular zone doesn't mean you can't assign your staff to work or be part of other zones. For instance, if you're in productivity, staff members don't necessarily need to commit all their time to productivity goals. You might consider delegating some incubator tasks within your department, too. 

Once you've created your zones, activate the ones dedicated to performance and productivity. Begin with an annual planning process and a corresponding budgeting session. Determine which programs and sales goals you want to see realized this year and ensure you have the money to do so.

Your incubation zone requires a bit more flexibility. It's impossible to plan exactly when projects will need to be fed into your company's incubator. The correct timing is a matter of feel and depends on the company's needs and market position at any given time. 

However, you _should_ decide how much money you can spend on future incubation projects and determine the members of the zone's board of governance. The CEO and the executive board should make these decisions.

Finally, determine whether a transformation zone is necessary. Decide whether it needs to be inactive, reactive or proactive this year. Once you've determined the zone's status, apportion funds and resources accordingly.

### 9. Technology giant Microsoft uses the four zones of management to combat disruption successfully. 

The experience of Microsoft is one poignant example of how a disruption can hobble even a technology giant. The company's Windows operating system has experienced serious headwinds in the face of competition from mobile technology.

The boom in smartphones has benefited Microsoft's biggest competitors, Apple and Google, both of which have developed mobile operating systems that together dominate most handheld devices on the market today.

What's more, as the popularity of smartphones has increased, developers have focused more energy toward creating products for iOS or Android, ignoring Windows platforms.

After years of dominating the market for operating systems, Microsoft Windows has fallen hard.

But this disruption hasn't crushed Microsoft completely. Instead, the company channeled energy into its transformation zone and is meeting the challenge with innovative strategies. 

CEO Satya Nadella was appointed in 2014 to head an effort to regain customers in the face of increased competition in the mobile market. Microsoft decided to make Office available as a free download for smartphones that ran iOS or Android.

This decision helped Microsoft retain loyal customers, those who already use Office on a home computer but who could now access the program in a mobile format.

Microsoft at the same time is developing new products through its incubation zone to remain competitive. For example, its search engine Bing has made serious inroads as a Google competitor.

Bing is focused on incorporating innovative, disruptive features, such as its ability to learn from daily search queries. The search engine is also working on incorporating algorithms that could, eventually, help urban planners create "smart" roads, buildings or even cities.

### 10. Final summary 

The key message in this book:

**Disruptive technology doesn't necessarily mean the end of existing businesses. By incorporating disruptive ideas and remaining open to development, established companies can still thrive.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Crossing the Chasm_** **by Geoffrey A. Moore**

_Crossing the Chasm_ examines the market dynamics faced by innovative new products, particularly the daunting chasm that lies between early to mainstream markets.

The book provides tangible advice on how to make this difficult transition and offers real-world examples of companies that have struggled in the chasm.
---

### Geoffrey A. Moore

Geoffrey A. Moore holds a doctorate in literature from the University of Washington and works as a consultant for technology companies such as Salesforce and Microsoft. His first book, _Crossing the Chasm_, sold over one million copies.

