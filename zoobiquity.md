---
id: 5406ed9b3933310008c70000
slug: zoobiquity-en
published_date: 2014-09-02T00:00:00.000+00:00
author: Barbara Natterson-Horowitz and Kathryn Bowers
title: Zoobiquity
subtitle: The Astonishing Connection Between Human and Animal Health
main_color: 1C6E33
text_color: 1C6E33
---

# Zoobiquity

_The Astonishing Connection Between Human and Animal Health_

**Barbara Natterson-Horowitz and Kathryn Bowers**

_Zoobiquity_ describes the intimate similarities between humans and other animals by examining topics such as sexuality, health and psychological development. It illustrates how we, as humans, could have much to gain by increasing our understanding of the animals we share our planet with.

---
### 1. What’s in it for me? Learn how much you have in common with animals. 

As humans, we tend to think of ourselves as somewhat separate from the animal kingdom. On the surface, that may seem logical because our intelligence level is so much higher, but reach deeper and you'll discover we have a great deal in common with our fellow animals. When we understand the qualities we share with animals, we understand more about ourselves.

You probably already know that we have many things in common with our closest animal relative, the ape, but our connections to the animal world extend much further than you'd imagine. In these blinks, you'll learn just how far those connections can go. You'll also learn about the great advances that can be made if we see more collaboration between veterinarians and physicians.

You'll also learn

  * what sea otters can teach us about teenage drunk driving;

  * the similarities between obesity in humans and weight gain in livestock;

  * how water buffalos follow the same drug-addiction patterns as humans;

  * why ring-tailed lemurs can suffer from erectile dysfunction, just like human males; and

  * how joint research between doctors and veterinarians could lead to cures for cancer, for sexually transmitted diseases and for viral epidemics.

### 2. Humans and animals are so genetically similar that it makes sense for their doctors to collaborate. 

Have you ever been to a zoo and found yourself taken aback by the "humanness" of the behavior of some monkeys or other animals? That closeness isn't just a coincidence, or your imagination. It's genetic.

Human beings and animals have many fundamental similarities, including their genes. In fact, the human genome is 98.6 percent identical to the chimpanzee's.

Biologists call the genetic similarities we share with other animals _deep_ _homology_. Deep homology doesn't just refer to species that are closely related, like wolves and dogs, or humans and chimps. It also refers to animals that are more distantly related. For example, it explains the connection between light-responsive vision in a hawk and photosensitivity in green algae.

Because of the intimate similarities between humans and animals, it's logical that veterinary and medical researchers should collaborate. In fact, this used to be the case: two centuries ago, most physicians also tended to their town's animals. This changed in the twentieth century, when people began to give physicians a higher social status, and they began to have more prestige and higher salaries. Unfortunately, these benefits weren't extended to veterinarians.

The dividing line between doctors and vets still exists, although it is slowly being weakened. In 2007, for instance, the head of the American Medical Association and the head of the American Veterinary Medical Association arranged a meeting, hoping to foster collaboration between physicians and veterinarians.

If collaborative efforts like this continue, we might gain a better understanding of both human and animal biology. Our genetic similarities run deep — but how similar is our behavior, or the way diseases affect our bodies? The following blinks take a closer look at some of the astonishing similarities we share with the animals in our world.

> _"Seeing too much of ourselves in other animals might not be the problem ... underappreciating our own animal natures may be."_

### 3. Fainting might be a protection mechanism for both humans and animals. 

Have you ever felt dizzy after standing up too quickly? Was it ever so bad that you fainted, or felt you were going to faint?

Fainting is actually quite normal for humans. In fact, one in three adults have _already_ fainted in their lifetime. Fainting is usually caused by a temporary shortage of blood and oxygen in the brain. There are two reasons why this might happen.

First, if you stand up too quickly, your blood has to be pumped into your brain against gravity. Sometimes that process doesn't happen fast enough, which causes dizziness that might lead to fainting.

The second potential cause of fainting is emotional. When you have a strong emotional reaction to something, your heart rate increases. Your body then responds by trying to lower it. If that drop is too sudden, however, you might have a temporary loss of blood flow to the brain, which might make you faint.

But what is the _purpose_ of emotional fainting, exactly? The animal kingdom provides some answers.

Fainting isn't just found in humans — some animals also faint when they face serious danger, and it can serve as a vital protection mechanism.

Ducks, for example, sometimes faint when they see a fox. This makes the fox less likely to attack them, as it causes the duck to appear dead, and foxes are more interested in live ducks.

Also, when animals faint, it usually lowers their heart rate. This can also protect them. Sharks, for instance, can detect electrical impulses in the beating hearts of nearby fish. If the fish faint, they might be able to escape a shark attack.

The animal world can help us understand fainting in humans. It seems likely that fainting evolved in humans for the same reason it evolved in animals — it may have helped our ancestors survive.

### 4. Both humans and animals can suffer dangerous heart problems when under emotional strain. 

You've probably noticed that your heartbeat changes when you experience intense emotions. In extreme cases, emotions can cause serious damage to your heart, or even death.

When a person dies because their heart unexpectedly stops beating, it's called _sudden_ _cardiac_ _death_. Usually, it follows a specific pattern: a person's heartbeat either quickly speeds up, or starts beating irregularly. In either case, there's a danger that the heart might stop beating entirely, unless doctors can restore it to it's regular rhythm.

Intense emotions like fear or excitement can cause sudden cardiac death. In the United States, patients with implanted electrical devices that monitored their heart rates showed a 200 percent leap in life-endangering arrhythmias in the aftermath of the 9/11 attacks.

Sudden cardiac death can also be brought on by stress. Sometimes people suffer from it after they lose their partner or a close family member, for example.

Like humans, animals can also experience sudden cardiac death, although in the animal kingdom it's called _capture_ _myopathy_.

Capture myopathy occurs when animals die suddenly after being relocated or captured by humans. Deer, elk and reindeer that are hunted or relocated in North America have a 20 percent death rate from the condition.

Capture myopathy doesn't have to happen in the heat of the moment: In one zoo in Vancouver, for example, four zebras died from capture myopathy after being kept in the same enclosure as two buffalo, because they were so frightened of them.

Capture myopathy and sudden cardiac death have a great deal in common. Both are caused by intense emotions, or prolonged stress from something that's outside the animal or human's control.

### 5. Intestinal microbes can make both humans and animals obese. 

Obesity has become an increasingly prevalent issue in modern society. But why do some people seem more susceptible to it than others?

Well, one possible explanation comes from our intestines. Our intestines contain a wide variety of different microbes, such as bacteria, viruses, fungi or worms. Collectively, these microbes are known as _gut_ _flora_.

We don't just have microbes in our intestines — in fact, our bodies contain so many microbes that researchers estimate that only one out of ten cells in our body is actually a human cell! Though that might sound unpleasant, these microbes are instrumental to our survival. One of their important roles is to help break down our food, which is why they can have an effect on our weight.

At the beginning of this century, geneticists at Washington University found that the proportions of certain intestinal microbes are different in obese people. Obese people have a higher proportion of _firmicutes_, while lean people have more _bacteroidetes_. Firmicutes extract calories more effectively. So if a person has more bacteroidetes, they won't extract as many nutrients from their food, and this partly explains why some people are able to eat more without gaining weight.

This link between intestinal microbes and weight gain can also be observed in animals, especially domestic livestock like chicken or cows.

Nowadays, livestock are usually given a lot of antibiotics. This not only makes them healthier, it also makes them gain weight. Why? Well, the antibiotics change the gut flora of the animals, causing an increase of the microbes that extract more calories from their food as it's being digested. That means they can gain more weight than an animal not dosed with antibiotics by consuming the same amount of food.

Currently, the intestines are one of the least researched areas of the human body. Exploring topics such as the relationship between microbes and obesity, in both humans and animals, may lead to important discoveries.

### 6. Grooming is vital for the health of animals and humans, but it can be taken too far. 

You've probably seen a cat busily cleaning and grooming itself. Grooming is important in the lives of felines and many other animals. Cats can spend up to a third of their waking hours cleaning themselves.

Grooming is sometimes a social activity, too. Some groups of apes scratch each other's backs and remove bugs from each other's fur. This is not only more efficient than trying to do it alone, but also strengthens social bonds.

Grooming also plays an important role for humans. Some of our grooming procedures are practical, like washing our hands, and some are more for pleasure, like having a professional manicure.

Leisurely grooming procedures like manicures aren't just about keeping clean — they can also calm us down. Grooming alters the neurochemistry in our brains by causing more opiates to be released into our bloodstreams, which leads to a decrease of blood pressure.

However, though it has great benefits, _over-grooming_ is dangerous, and might even explain some dysfunctional behavior in both humans and animals.

Humans, for instance, often feel the urge to squeeze their pimples. This is unhealthy, but the process also releases opiates in the brain, causing relief. This release-and-relief pattern is often described by people who cut themselves, so self-harm can be considered similar to grooming, as the underlying cycle is the same.

Animals sometimes do this too. Some dogs lick their wounds too much, which can slow down the healing process. Some birds pull out their own feathers.

Self-injury behaviors can seem difficult to understand, but viewing them as over-grooming might help explain them in both humans and animals.

### 7. Both adolescent humans and animals learn and develop through risk taking and impulsive behavior. 

When you think back to your teenage years, what comes to your mind? Were you sometimes impulsive, or did you take unnecessary risks?

During adolescence, risk taking is very common — for both humans and animals.

Adolescent sea otters, for instance, often have little fear about venturing into dangerous areas. Scientists in San Francisco have named an area near the city the "Triangle of Death" because of its high density of riptides and great white sharks. Adults sea otters almost never go there, but adolescent ones often do so freely.

This sort of behavior seems reckless, but it actually serves specific purposes. In animals, risk taking can develop the ability to recognize predators.

Like the otters who approach sharks, young gazelles sometimes approach their predators in order to inspect them. One out of 417 times, the daring young gazelle is killed, whereas only one in 5,000 adult gazelles who try the same move are killed.

Impulsivity can also be important for determining the social hierarchy within groups. For instance, some monkeys, such as vervets, have to leave their group at a certain age and find a new one. The most impulsive vervets are usually the ones who end up at a higher level in their new groups.

The same dangerous, risk-seeking behavior can be observed in humans. In fact, the Center for Disease Control and Prevention reports that for every year between age 12 and 19, a teenager's chances of dying increase. About 35 percent of these fatalities are traffic accidents. This is probably due to the impulsiveness of teenagers: their driving skills aren't advanced enough to handle the risks they take behind the wheel. But for the successful, risk taking can lead to plenty of social kudos.

Another tool that adolescent humans use to secure their place in the social hierarchy is bullying. As with vervets, the most impulsive ones are usually the most successful.

### 8. Humans and animals have the same reproductive process, and the same associated problems. 

Humans and animals have a fair bit in common sexually. For one, most matings in the animal world require both a male and female, and they both are identified the same way: males contribute sperm to the mating process, and females contribute the eggs.

Long ago, sexuality was quite different for the earliest forms of life. The first single-celled organisms replicated simply by cloning themselves: they didn't need a partner, and they didn't share any of their genetic material. The offspring they produced were exact copies of themselves.

However, they slowly evolved the ability to mix their DNA, as this was a great evolutionary advantage — they could gain advantageous characteristics or lose problematic ones. Over millions of years, animals evolved to have two different sexes, which play different but equally vital roles in the reproductive process.

This is why many animal species (including humans) have two sexes and each possesses either a penis or a vagina. Penises and vaginas are very useful for mating. Penises are effective because they can ejaculate semen directly next to the egg, without losing sperm. Vaginas are effective because they enable more control over the female's mating patterns by creating a more welcoming — or hostile — environment for sperm.

There are also similarities in the sexual problems that animals and humans experience. About one in ten human males suffers from _erectile_ _dysfunction_, when penises don't get hard enough for penetration. Although it's a physical problem, it often has psychological causes, like stress.

A similar phenomenon occurs in ring-tailed lemurs. They only mate once a year, and during that time there's intense competition, which is sometimes violent. This can cause stress in lower-ranked males, making them so nervous that they aren't able to mate, because they're so afraid that rival males may attack.

If stress is too high for both human and lemur males, it can ultimately prevent them from procreating. Stress affects their bodies in the same way, and results in the same consequences.

### 9. Treatments that battle cancer in animals might help fight the disease in humans too. 

Cancer is one of the most frightening diseases that exists in our world. Everyone has heard of cancer, but what is it exactly?

Well, cancer is the result of cells mutating and clustering together. Mutations sometimes occur when old cells are replaced with new ones. Usually when cells replicate, their DNA is copied exactly, but sometimes mistakes occur. Chemicals might be left out, or arrive in the wrong place.

If a mistake like this occurs, the DNA can use a suicide mechanism to destroy itself. However, sometimes the suicide mechanisms themselves suffer from mutations, which leaves the mutated cell unable to destroy itself.

When the cells containing the mutation come together, they form a cluster, also known as a _tumor_. These cells may then begin to travel to other parts of the body and replicate there, in a process known as _metastasis._ This is how cancer can spread and become even more dangerous.

Animals also suffer from cancer, through the same process.

For example, _malignant_ _melanoma_, a form of skin cancer, affects humans and dogs in virtually the same way. Because of this, a veterinarian and a physician worked together on a project aiming to treat melanoma in dogs. They injected dogs that suffered from melanoma with human DNA that had been shown to fight cancer. Their project was successful: the human DNA caused the dogs' tumors to become smaller.

In 2009, a vaccine was released that treats this type of cancer in dogs. Although a vaccine for humans still hasn't been developed, this research may lead to one.

Currently, relatively little is known about cancer, and that's what makes it so horrifying. At the moment, we don't have a great deal of defenses or cures for it. However, if we can understand and develop treatments for cancer in animals, it may lead to important discoveries in treatment for humans.

### 10. Research into sexually transmitted diseases of animals might indicate new treatments for humans. 

Did you know that animals can also get sexually transmitted diseases (STDs)?

STDs can infect a wide range of species, and can even make them sterile. Koalas, for instance, have become seriously endangered by an STD that also infects humans: chlamydia. Without proper treatment, chlamydia can lead to infertility.

STDs can even affect insects. Two-dot ladybugs can be infected by a mite that renders them sterile.

Research in animal STDs may be very important for humans. Unfortunately, research into human STDs is somewhat stigmatized. An STD is often perceived to be the infected person's fault, as he or she could've taken precautions. This is one of the reasons doctors usually prefer to specialize in other topics considered to have a higher status, like brain or heart surgery.

The study of animal STDs can be more useful than the study of human infections because this stigma doesn't exist. Animals can't consciously protect themselves from STDs, so when — for example — researchers see that some koalas in a population where chlamydia is rife never get infected, they instantly know that these koalas were born with an immunity to the disease.

If scientists can understand what makes these koalas immune, they'll be able to try to replicate that immunity in other koalas. If they can develop a vaccine or cure for chlamydia, they might then be able to modify it for humans.

Thus, research into animals and STDs might enable us to battle STDs in humans. It's much easier to study natural immunities in animals because they have a greater chance to develop, given that animals can't take precautions to avoid STDs. A better understanding of this phenomenon might explain why some humans are immune to HIV, for example, and help us to defeat it.

### 11. Humans and animals can both become addicted to drugs. 

Why do humans have emotions?

Well, emotions are important survival mechanisms. And they can be influenced by drugs, sometimes with very serious consequences.

Negative emotions signal threats to us. So if something dangerous makes us feel anxiety or fear, we're likely to avoid it.

Positive emotions, on the other hand, are rewards for behaviors that help us survive. That's why we feel good when we eat food, or become closer with those in our social circle. These sensations are caused by brain chemicals, such as _oxytocin_. Oxytocin makes you feel secure and attached to others.

However, there are quicker and more powerful ways to feel these sensations: drugs.

When we take drugs, chemicals flood the receptors in our brain. Our brain thinks the inflated levels of chemicals are normal, and will compel us to seek that state again. This often leads to addiction.

Animals can also become addicted to drugs.

We see this in buffalo, for instance. In some regions in Asia, opium poppies grow near areas where water buffalo live. When the flowers are available, the buffalo stops to get a dose of opium from them every day. When the flowering season ends, they show symptoms of withdrawal, much like humans.

Some of the buffalo become addicted to the poppies more easily than others, and others seem to not get addicted at all. The same pattern exists in human drug users.

Also, in both animals and humans, an individual is more likely to become addicted to a drug the younger they are when they first encounter it.

These commonalities might help scientists to better understand the nature of addiction. Why are some people more susceptible to it? Why do some people seem nearly immune? Answering these and other drug-related questions might help us treat and prevent serious addiction problems.

### 12. Vets and doctors save lives and money when they compare notes. 

You've probably heard of AIDS, SARS and ebola. These horrifying diseases share a fundamental similarity: they can infect both humans and animals.

Diseases that can move from animals to humans (and vice versa) are called _zoonoses_.

For example, you may recall the H1N1 or "swine flu" epidemic that hit humans in 2009. It appeared suddenly, and spread very quickly. That virus formed when genetic material from the human flu virus combined with genetic material from pig flu viruses to make a new and particularly dangerous virus. It didn't stop there. It also acquired genetic material from the bird flu virus, suggesting that it mutated drastically.

This can happen when two different viruses find themselves together in a single living cell. When they trade sections of their genetic code, a new virus is born.

Zoonoses infect all living beings and can spread very quickly, so it's crucial that doctors and vets work together to fight them. This cooperation can save lives.

Collaborative research between physicians and veterinarians on zoonoses might also help _prevent_ global epidemics.

There's currently a huge discrepancy between how much money goes into _preventing_ diseases, and how much is spent on responding to them after they arrive. In the past 20 years, governments throughout the world have spent very little money on disease prevention, but over $200 billion on research for cures.

In the long run, focusing on prevention is much cheaper and also more effective, especially for diseases that affect both humans and animals. Tighter cooperation between physicians and veterinarians can help fight future epidemics, and also save us a great deal of money.

### 13. Final summary 

The key message in this book:

**Humans** **and** **animals** **have** **a** **great** **deal** **of** **intimate** **similarities.** **By** **learning** **more** **about** **the** **animal** **kingdom,** **we** **can** **increase** **our** **understanding** **of** **ourselves.** **And** **because** **of** **our** **deep-rooted** **similarities,** **more** **collaboration** **between** **physicians** **and** **veterinarians** **could** **yield** **revolutionary** **results,** **not** **only** **for** **animals** **but** **for** **us** **humans,** **too.**

**Suggested** **further** **reading:** **_Our_** **_Inner_** **_Ape_** **by** **Frans** **de** **Waal**

Human beings are just as closely related to the gentle bonobos as they are to the aggressive chimpanzees. Frans de Waal compares the lifestyle of these two species of apes, in whose groups opposing characteristics such as sympathy and violence, fairness and greed, and dominance and community spirit clash with one another. Their sexual behavior tells us that we need to rethink the origins of our morality.
---

### Barbara Natterson-Horowitz and Kathryn Bowers

Barbara Natterson-Horowitz is a professor of cardiology at University of California, Los Angeles. She's also a member of the medical advisory board of the Los Angeles Zoo. Kathryn Bowers is a writer and producer at CNN International, and she often gives lectures at UCLA on medical topics.

